cmake_minimum_required(VERSION 3.1)
project(imgui)

set(CMAKE_CXX_STANDARD 17)

add_library(imgui
        imgui/imconfig.h
        imgui/imgui.cpp imgui/imgui.h
        imgui/imgui_draw.cpp
        imgui/imgui_widgets.cpp
        imgui/imgui_demo.cpp
        )

target_include_directories(imgui PUBLIC ../imgui imgui)