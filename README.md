#[PREVIEW]

![An example screenshot](https://gitlab.com/CyanPhoton/cits3003_project/-/raw/master/preview.png)

#[USAGE INFO]

Right clicking in the view space will give you a tool selection window, with the hotkeys shown.
Otherwise is simple mouse scroll to zoom, middle mouse to pan camera and WASD to slide camera around.

Per the project spec we where to use a specific set of models and textures, which due to copyright are not avalaible to te public.
These would be placed inside a res/models-textures directory, and expects a minimum a "texture.bmp" (A white texture), "model0.x" (A 2d plane) and "model55.x" (A Sphere) and without these will simply crash a launch. This would not be hard to fix by just generating these models
at runtime (like I did with the gizmo models) but that wasn't part of the spec.

#[BUILDING / RUNNING]

NOTE: This can take a while to build since the libraries are build from source
NOTE: IT WILL NOT RUN IN A LINUX VM as it needs a proper OpenGL context.

When writing this application I did in in WINDOWS with g++ using MinGW-64, in CLion.
However since I used CMake and built all libraries from source, it should be able to be built on Win, Linux and Mac.

I have only tested building on Windows with MSVC and MinGW-64, and on Linux with g++. But expect it to work on Mac (and don't care if it doesn't).

I've only tested running it on Windows, but suspect it to run on Linux and Mac.

If you want to build on Windows then I suggest doing it via MSVC (see #[WINDOWS]), as setting up and using MinGW without something like CLion is a real pain (see #[CLION] if you have it).
If you really want to use MinGW and not use CLion (really unadvised) then I wish you luck.

If you want to build on Linux see #[LINUX]

#[WINDOWS]
I believe this to be as simple as, have MSVC installed (probably version 17 or greater is required), then run the BuildWin.bat script
and this should build it, which can take a while since it's building from source.

Then you just run SceneEditor.exe

#[LINUX]
Required dependencies to build from a fresh Linux Ubuntu 20.04 LTS install:
 cmake
 g++
 libx11-dev
 libxrandr-dev
 libxinerama-dev
 libxcursor-dev
 libxi-dev

after running sudo apt update

All in one command:
sudo apt install cmake g++ libx11-dev libxrandr-dev libxinerama-dev libxcursor-dev libxi-dev

Then, it should be as simple as running BuildLinux.sh (possibly with sudo unless you chmod the appropriate things, since a .zip destroys permissions I think)

Then just run SceneEditor

#[CLION] Note: Any platform

With CLion installed, and almost any toolchain setup, be it MSVC, MinGW (g++ or clang), Cygwin (g++ or clang)
you should be able to just open the project folder it will automatically find the CMake and handle it from there,
from which you just have run the SceneEditor target.
