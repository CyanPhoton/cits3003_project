#version 430 core

in layout(location = 0) vec3 vertexPos;

// Per-instance data
in layout(location = 3) mat4 modelMatrix;
in layout(location = 10) vec4 colour;

flat out layout(location = 0) vec4 flatColour;

uniform mat4 projViewMatrix;

void main() {
    gl_Position = projViewMatrix * modelMatrix * vec4(vertexPos, 1.0f);
    flatColour = colour;
}