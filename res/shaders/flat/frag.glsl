#version 430 core

flat in layout(location = 0) vec4 flatColour;

out layout(location = 0) vec4 outColour;

void main() {
    outColour = flatColour;
}