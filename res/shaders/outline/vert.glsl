#version 430 core

in layout(location = 0) vec3 vertexPos;
in layout(location = 1) vec3 normal;

// Per-instance data
in layout(location = 3) mat4 modelMatrix;
in layout(location = 7) mat3 normalMatrix;

uniform mat4 projViewMatrix;

uniform float normalDisplacement = 0.0f;

void main() {
    gl_Position = projViewMatrix * (modelMatrix * vec4(vertexPos, 1.0f) + vec4(normalize(normalMatrix * normal) * normalDisplacement, 0.0f));
}
