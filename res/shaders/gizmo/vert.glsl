#version 430 core

in layout(location = 0) vec3 vertexPos;

uniform mat4 modelMatrix;
uniform mat4 projViewMatrix;

void main() {
    gl_Position = projViewMatrix * modelMatrix * vec4(vertexPos, 1.0f);
}
