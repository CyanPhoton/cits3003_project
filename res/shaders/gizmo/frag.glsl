#version 430 core

out layout(location = 0) vec4 outColour;

uniform vec4 colour;

void main() {
    outColour = colour;
}