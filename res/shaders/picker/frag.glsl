#version 430 core

flat in layout(location = 0) uint id;

out layout(location = 0) uint idColour;

void main() {
    idColour = id;
}