#version 430 core

in layout(location = 0) vec3 vertexPos;
// Per-instance data
in layout(location = 1) mat4 modelMatrix;
in layout(location = 5) uint id;

flat out layout(location = 0) uint id_out;

uniform mat4 projViewMatrix;

void main() {
    gl_Position = projViewMatrix * modelMatrix * vec4(vertexPos, 1.0f);
    id_out = id;
}