#version 430 core

in layout(location = 0) vec3 vertexPos;
in layout(location = 1) vec3 normal;
in layout(location = 2) vec2 textCoords;

// Per-instance data
in layout(location = 3) mat4 modelMatrix;
in layout(location = 7) mat3 normalMatrix;
in layout(location = 10) vec4 materialProperties;
in layout(location = 11) vec4 tintColour_TexScale;

out VertexOut {
    vec4 worldPos;
    vec3 normal;
    vec2 textCoords;
} vertexOut;

out InstanceOut {
    flat mat4 modelMatrix;
    flat mat3 normalMatrix;
    flat vec4 materialProperties;
    flat vec4 tintColour_TexScale;
} instanceOut;

uniform mat4 projViewMatrix;

void main() {
    instanceOut.modelMatrix = modelMatrix;
    instanceOut.normalMatrix = normalMatrix;
    instanceOut.materialProperties = materialProperties;
    instanceOut.tintColour_TexScale = tintColour_TexScale;

    vertexOut.textCoords = textCoords;
    vertexOut.normal = normal;

    vertexOut.worldPos = modelMatrix * vec4(vertexPos, 1.0);

    gl_Position = projViewMatrix * vertexOut.worldPos;
}
