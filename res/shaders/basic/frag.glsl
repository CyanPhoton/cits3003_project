#version 430 core

in VertexOut {
    vec4 worldPos;
    vec3 normal;
    vec2 textCoords;
} fragIn;

in InstanceOut {
    flat mat4 modelMatrix;
    flat mat3 normalMatrix;
    flat vec4 materialProperties;
    flat vec4 tintColour_TexScale;
} instanceIn;

out layout(location = 0) vec4 outColour;

uniform vec3 cameraPos;

uniform layout(binding = 0) sampler2D diffuseTexture;

struct PointLightData {
    vec3 position;
    vec3 colour;
    vec3 attenuation;
};

struct DirectionalLightData {
    vec3 direction;
    vec3 colour;
};

struct ConeLightData {
    vec4 position_Softness;
    vec4 direction_FOV;
    vec3 colour;
    vec3 attenuation;
};

#if NUM_PL > 0
layout (std140, binding = 0) uniform PointLightArray {
    PointLightData pointLights[NUM_PL];
};
#endif

#if NUM_DL > 0
layout (std140, binding = 1) uniform DirectionalLightArray {
    DirectionalLightData directionalLights[NUM_DL];
};
#endif

#if NUM_CL > 0
layout (std140, binding = 2) uniform ConeLightArray {
    ConeLightData coneLights[NUM_CL];
};
#endif

const float globalAmbientFactor = 0.05f;

void main() {
    float diffuseFactor = instanceIn.materialProperties.x;
    float specularFactor = instanceIn.materialProperties.y;
    float ambientFactor = instanceIn.materialProperties.z;
    float shininessExponent = instanceIn.materialProperties.w;

    vec3 viewVec = normalize(fragIn.worldPos.xyz - cameraPos);
    vec3 normal = normalize(instanceIn.normalMatrix * fragIn.normal);

    vec3 diffuseLight = vec3(0.0f);
    vec3 specularLight = vec3(0.0f);

    #if NUM_PL > 0
    for (int i = 0; i < NUM_PL; i++) {
        vec3 incident = fragIn.worldPos.xyz - pointLights[i].position;

        float distanceSquared = dot(incident, incident);
        float distance = sqrt(distanceSquared);
        float attenuationFactor = pointLights[i].attenuation[0] * distanceSquared + pointLights[i].attenuation[1] * distance + pointLights[i].attenuation[2];

        incident /= distance;
//        vec3 reflection = reflect(incident, normal);
        vec3 halfWay = normalize((-incident) + (-viewVec));

        diffuseLight += pointLights[i].colour * max(diffuseFactor * dot(-incident, normal), ambientFactor) / attenuationFactor ;
//        specularLight += pointLights[i].colour * pow(max(dot(-viewVec, reflection), 0.0f), shininessExponent) / attenuationFactor;
        specularLight += pointLights[i].colour * pow(max(dot(normal, halfWay), 0.0f), shininessExponent) / attenuationFactor;
    }
    #endif

    #if NUM_DL > 0
    for (int i = 0; i < NUM_DL; i++) {
        vec3 incident = directionalLights[i].direction;
//        vec3 reflection = reflect(incident, normal);
        vec3 halfWay = normalize((-incident) + (-viewVec));

        diffuseLight += directionalLights[i].colour * max(diffuseFactor * dot(-incident, normal), ambientFactor);
//        specularLight += pointLights[i].colour * pow(max(dot(-viewVec, reflection), 0.0f), shininessExponent);
        specularLight += directionalLights[i].colour * pow(max(dot(normal, halfWay), 0.0f), shininessExponent);
    }
    #endif

    #if NUM_CL > 0
    for (int i = 0; i < NUM_CL; i++) {
        vec3 incident = fragIn.worldPos.xyz - coneLights[i].position_Softness.xyz;

        float distanceSquared = dot(incident, incident);
        float distance = sqrt(distanceSquared);
        float attenuationFactor = coneLights[i].attenuation[0] * distanceSquared + coneLights[i].attenuation[1] * distance + coneLights[i].attenuation[2];

        incident /= distance;
//        vec3 reflection = reflect(incident, normal);
        vec3 halfWay = normalize((-incident) + (-viewVec));

        float forwardFactor = dot(incident, coneLights[i].direction_FOV.xyz);
        forwardFactor = (forwardFactor - coneLights[i].direction_FOV.w) / coneLights[i].position_Softness.w;
        forwardFactor += 0.5f;
        forwardFactor = clamp(forwardFactor, 0.0f, 1.0f);

        diffuseLight += forwardFactor * coneLights[i].colour * max(diffuseFactor * dot(-incident, normal), ambientFactor) / attenuationFactor;
//        specularLight += forwardFactor * coneLights[i].colour * pow(max(dot(-viewVec, reflection), 0.0f), shininessExponent) / attenuationFactor;
        specularLight += forwardFactor * coneLights[i].colour * pow(max(dot(normal, halfWay), 0.0f), shininessExponent) / attenuationFactor;
    }
    #endif

    vec3 diffuseColour = instanceIn.tintColour_TexScale.rgb * texture(diffuseTexture, fragIn.textCoords * instanceIn.tintColour_TexScale.w).rgb;

    // Diffuse factor and per light ambient in diffuseLight, global ambient in max
    vec3 diffuse = diffuseColour * max(diffuseLight, globalAmbientFactor);
    vec3 specular = specularFactor * specularLight;

    vec3 totalColour = diffuse + specular;
    //totalColour = max(totalColour, vec3(0.1f) * diffuseColour);

    outColour = vec4(totalColour, 1.0);
}