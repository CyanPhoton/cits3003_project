#ifndef OPENGL_H
#define OPENGL_H

#include <glad/gl.h>

namespace OpenGL {
    // Supported as far back as at least gtx 460, so no issue there
    const int VERSION_MAJOR = 4;
    const int VERSION_MINOR = 5;

    void loadFunctions();

    void setupDebugCallback();
}

#endif //OPENGL_H
