#ifndef ID_VEC_H
#define ID_VEC_H

#include <vector>
#include <queue>

#include "Defs.h"

class ElementID {
    size_t id;

    template <class T>
    friend class IDVec;
};

template <class T>
class IDVec {
    std::vector<T> dataVec{};
    std::vector<size_t> idMap{};
    std::vector<size_t> revIdMap{};
    std::queue<size_t> recycledIds{};

public:
    IDVec() = default;

    ElementID addData(T value) {
        size_t id = 0;
        if (recycledIds.empty()) {
            idMap.push_back(0);
            id = idMap.size() - 1;
        } else {
            id = recycledIds.front();
            recycledIds.pop();
        }

        size_t dataIndex = dataVec.size();

        idMap[id] = dataIndex;
        dataVec.push_back(value);
        revIdMap.push_back(id);

        ElementID elementID{};
        elementID.id = id;

        return elementID;
    }

    T removeData(ElementID id) {
        size_t removeIndex = idMap[id.id];

        T data = dataVec[removeIndex];
        if (removeIndex != dataVec.size() - 1){
            dataVec[removeIndex] = dataVec[dataVec.size() - 1];
        }
        dataVec.pop_back();

        if (removeIndex != revIdMap.size() - 1){
            revIdMap[removeIndex] = revIdMap[revIdMap.size() - 1];
        }
        revIdMap.pop_back();

        if (removeIndex < revIdMap.size()) {
            idMap[revIdMap[removeIndex]] = removeIndex;
        }

        recycledIds.push(id.id);

        return data;
    }

    const T& element(ElementID id) const {
        return dataVec[idMap[id.id]];
    }

    T& element(ElementID id) {
        return dataVec[idMap[id.id]];
    }

    size_t size() const {
        return dataVec.size();
    }

    const T* data() const {
        return dataVec.data();
    }

    T* data() {
        return dataVec.data();
    }
};

#endif //ID_VEC_H


/*

 pub fn add_data(&mut self, data: T) -> DataID {
        let id = self.recycled_ids.pop_front().unwrap_or_else(|| {
            self.id_map.push(0);
            self.id_map.len() - 1
        });
        let data_index = self.data_vec.len();

        self.id_map[id] = data_index;
        self.data_vec.push(data);
        self.rev_id_map.push(id);

        DataID {
            id
        }
    }

    pub fn remove_data(&mut self, id: DataID) -> T {
        let remove_index = self.id_map[id.id];

        let data = self.data_vec.swap_remove(remove_index);
        self.rev_id_map.swap_remove(remove_index);

        if remove_index < self.rev_id_map.len() {
            self.id_map[self.rev_id_map[remove_index]] = remove_index;
        }

        self.recycled_ids.push_back(id.id);

        data
    }

    pub fn iter(&self) -> std::slice::Iter<T> {
        self.data_vec.iter()
    }

    pub fn iter_mut(&mut self) -> std::slice::IterMut<T> {
        self.data_vec.iter_mut()
    }

    pub fn as_slice(&self) -> &[T] {
        self.data_vec.as_slice()
    }

    pub fn as_data_ptr(&self) -> *const T {
        self.data_vec.as_ptr()
    }

    pub fn as_mut_data_ptr(&mut self) -> *mut T {
        self.data_vec.as_mut_ptr()
    }
}

impl<T: Sized> Index<&DataID> for IDVec<T> {
    type Output = T;

    fn index(&self, index: &DataID) -> &T {
        &self.data_vec[self.id_map[index.id]]
    }
}

impl<T: Sized> IndexMut<&DataID> for IDVec<T> {
    fn index_mut(&mut self, index: &DataID) -> &mut T {
        &mut self.data_vec[self.id_map[index.id]]
    }
}

 */