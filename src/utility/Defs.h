#ifndef DEFS_H
#define DEFS_H

#include <algorithm>

#define CLAMP(val, minV, maxV) std::max((minV), std::min((val), (maxV)))

#define PERIOD(val, period) if (val < 0) { int num = (int) (-val / (period)) + 1; val += num * (period); } else { val = std::fmod(val, (period)); }

typedef unsigned int uint;

#endif //DEFS_H
