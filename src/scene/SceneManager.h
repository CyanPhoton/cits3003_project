#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include <optional>
#include <map>

#include "../rendering/MasterRenderer.h"
#include "GizmoManager.h"

class SceneManager {
    struct SceneObject {
        // Visible
        std::string name;
        size_t modelIndex;
        size_t textureIndex;

        // Physical
        glm::vec3 position{0.0f};
        glm::vec3 eulerRotation{0.0f};
        glm::vec3 scale = glm::vec3{1.0f};

        // Material
        glm::vec3 tint{1.0f};
        float diffuseFactor = 1.0f;
        float specularFactor = 1.0f;
        float ambientFactor = 0.1f;
        float shininessExponent = 32.0f;

        float totalBrightnessFactor = 1.0f;
        float textureScale = 1.0f;

        // Hidden
        EntityID entityID;
        bool canChangeModel = true;
        bool canRemove = true;
        bool canMove = true;
        bool canRename = true;

        void updateData(MasterRenderer& masterRenderer) const;
    };

    size_t nextObjectNumber = 0;
    std::map<EntityID, SceneObject> sceneObjects{};

    struct SceneLight {
        std::string name;
        LightType type = LightType::PointLight;

        glm::vec3 colour{1.0f};

        float brightness = 1.0f;

        // Point || Cone
        glm::vec3 position{0.0f};

        enum AttenuationMode: int {
            Constant = 0,
            Linear = 1,
            InverseSquare = 2,
            Custom = 3
        } attenuationMode = Constant;
        glm::vec3 attenuation = {0.0f, 0.0f, 1.0f};
        glm::vec3 customAttenuation = {0.0f, 0.0f, 1.0f};

        // Dir || Cone
        //                      Yaw         Pitch
        glm::vec2 direction = {0.0f, -1.0f};

        // Cone
        float halfAngle = M_PI_4;
        float softness = 0.1f;

        // Hidden
        LightID lightID;
        EntityID entityID;

        LightVariant makeVariant() const;
        void updateData(MasterRenderer& masterRenderer) const;
    };

    size_t nextLightNumber = 0;
    std::map<EntityID, SceneLight> sceneLights{};

    struct TextureInfo {
        std::string name;
        std::string location;
        size_t usages = 0;
        std::optional<TextureID> loaded{};
        std::optional<std::string> essential{};

        TextureInfo(std::string name, std::string location) : name(std::move(name)), location(std::move(location)) {}
    };

    static const size_t DEFAULT_TEXTURE;
    size_t selectedTexture = -1;
    std::vector<TextureInfo> textureInfos;

    struct ModelInfo {
        std::string name;
        std::string location;
        size_t usages = 0;
        std::optional<ModelID> loaded{};
        std::optional<std::string> essential;

        ModelInfo(std::string name, std::string location) : name(std::move(name)), location(std::move(location)) {}
    };

    static const size_t DEFAULT_MODEL;
    static const size_t SPHERE_MODEL;
    static const size_t GROUND_MODEL;
    static const size_t LIGHT_MODEL;
    size_t selectedModel = -1;
    std::vector<ModelInfo> modelInfos;

    float baseSelectionThickness = 0.005f;
    glm::vec3 selectionColour = glm::vec3{1.0f};

    static const float FLOOR_SCALE;
    static const float SPHERE_SCALE;
    static const float OBJECT_SCALE;

    float sphereRadius;
    const float LUMINANCE = 1.0f;

    enum class SelectionType {
        Object,
        Light,
        None
    } currentSelectionType = SelectionType::None;
    std::optional<EntityID> selection = {};

    std::optional<EntityID> oldSelection = {};

    enum class ToolMode {
        None,
        Move,
        Scale,
        Rotate,
        Select
    } toolMode = ToolMode::None;

    bool scaleLink = true;

    MoveGizmo moveGizmo;
    ScaleGizmo scaleGizmo;
    RotateGizmo rotateGizmo;
    YawPitchGizmo yawPitchGizmo;

    float overallGizmoScale = 1.0f;
public:
    explicit SceneManager(MasterRenderer &masterRenderer);
    void toolUpdate(MasterRenderer &masterRenderer, const Window &window, const Camera &camera);
    void addImGui(MasterRenderer &masterRenderer);
    void addImGuiSection(MasterRenderer &masterRenderer);

private:
    void selectionImGui(MasterRenderer &masterRenderer);
    void objectSelectionImGui(MasterRenderer &masterRenderer);
    void lightSelectionImGui(MasterRenderer &masterRenderer);

    void objectLightsImGui(MasterRenderer &masterRenderer);

    void modelTexturesImGui(MasterRenderer& masterRenderer);

    SceneObject newSceneObject(MasterRenderer &masterRenderer, std::string name, size_t modelIndex, size_t textureIndex);
    SceneLight newSceneLight(MasterRenderer& masterRenderer, std::string name);
};

#endif //SCENE_MANAGER_H
