#include "MeshGenerator.h"

#include <iostream>
#include <iterator>

#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#include <math.h>
#endif

// By values needed since moving contents of vectors invalidates memory
Mesh MeshGenerator::mergeMeshs(std::vector<Mesh> meshes) {
    ModelData mergedModelData{};
    ModelMetaData mergedModelMetaData{};

    size_t totalVertices = 0;
    size_t totalIndices = 0;
    for (const auto& mesh: meshes) {
        totalVertices += mesh.modelData.vertices.size();
        totalIndices += mesh.modelData.indices.size();
    }

    mergedModelData.vertices.reserve(totalVertices);
    mergedModelData.indices.reserve(totalIndices);

    size_t vertexOffset = 0;
    size_t indexOffset = 0;
    for (auto mesh: meshes) {
        size_t vertexCount = mesh.modelData.vertices.size();
        size_t indexCount = mesh.modelData.indices.size();

        std::move(mesh.modelData.vertices.begin(), mesh.modelData.vertices.end(), std::inserter(mergedModelData.vertices, mergedModelData.vertices.begin() + vertexOffset));
        std::move(mesh.modelData.indices.begin(), mesh.modelData.indices.end(), std::inserter(mergedModelData.indices, mergedModelData.indices.begin() + indexOffset));

        for (int i = 0; i < indexCount; i++) {
            mergedModelData.indices[i+indexOffset] += vertexOffset;
        }

        vertexOffset += vertexCount;
        indexOffset += indexCount;
    }

    // Calc meta data
    for (const auto& mesh: meshes) {
        mergedModelMetaData.boundingAABB.min = glm::min(mergedModelMetaData.boundingAABB.min, mesh.modelMetaData.boundingAABB.min);
        mergedModelMetaData.boundingAABB.max = glm::max(mergedModelMetaData.boundingAABB.max, mesh.modelMetaData.boundingAABB.max);
    }

    return Mesh(mergedModelData, mergedModelMetaData);
}

Mesh MeshGenerator::genParallelogramNoNormNoTex(glm::vec3 position, glm::vec3 norm, glm::vec3 tangent, float sideLength, bool doubleSided) {
    ModelData modelData{};
    ModelMetaData modelMetaData{};

    norm = glm::normalize(norm);
    tangent = glm::normalize(tangent);

    if (fabs(glm::dot(norm, tangent)) > 0.01f) {
        std::cerr << "[WARNING]: genParallelogramNoNormNoTex called with non perpendicular norm and tangent, possible errors may occur" << std::endl;
        std::cerr << "           dot: " << glm::dot(norm, tangent) << std::endl;
    }

    glm::vec3 cotangent = glm::cross(norm, tangent);

    float halfSideLength = sideLength / 2.0f;

    // Defined in CCW fashion from perspective of front as defined by normal
    Vertex vertex1 = {
            position + tangent * halfSideLength + cotangent * halfSideLength,
            glm::vec3(0),
            glm::vec2(0)
    };
    Vertex vertex2 = {
            position - tangent * halfSideLength + cotangent * halfSideLength,
            glm::vec3(0),
            glm::vec2(0)
    };
    Vertex vertex3 = {
            position - tangent * halfSideLength - cotangent * halfSideLength,
            glm::vec3(0),
            glm::vec2(0)
    };
    Vertex vertex4 = {
            position + tangent * halfSideLength - cotangent * halfSideLength,
            glm::vec3(0),
            glm::vec2(0)
    };

    modelData.vertices.push_back(vertex1);
    modelData.vertices.push_back(vertex2);
    modelData.vertices.push_back(vertex3);
    modelData.vertices.push_back(vertex4);

    modelData.indices.push_back(0);
    modelData.indices.push_back(1);
    modelData.indices.push_back(2);

    modelData.indices.push_back(0);
    modelData.indices.push_back(2);
    modelData.indices.push_back(3);

    if (doubleSided) {
        modelData.indices.push_back(2);
        modelData.indices.push_back(1);
        modelData.indices.push_back(0);

        modelData.indices.push_back(3);
        modelData.indices.push_back(2);
        modelData.indices.push_back(0);
    }

    // Calc meta data
    for (const auto& vertex : modelData.vertices) {
        modelMetaData.boundingAABB.min = glm::min(modelMetaData.boundingAABB.min, vertex.position);
        modelMetaData.boundingAABB.max = glm::max(modelMetaData.boundingAABB.max, vertex.position);
    }

    return Mesh(modelData, modelMetaData);
}

Mesh MeshGenerator::genCylinderNoNormNoTex(glm::vec3 centre, glm::vec3 length, float radius, uint sides) {
    ModelData modelData{};
    ModelMetaData modelMetaData{};

    // Calc temp direction that is not parallel to length
    glm::vec3 tempVec;
    if (length.y == 0.0f && length.z == 0.0f) {
        tempVec = {0.0f, 1.0f, 0.0f};
    } else {
        tempVec = {1.0f, 0.0f, 0.0f};
    }

    // Calc directions
    glm::vec3 tangent = glm::normalize(glm::cross(tempVec, length));
    glm::vec3 cotangent = glm::cross(glm::normalize(length), tangent);

    // Calculate points of an aligned circle at origin
    std::vector<glm::vec3> circleEdges{};
    for (uint side = 0; side < sides; side++) {
        float theta = ((float) side / (float) sides) * 2.0f * (float) M_PI;
        glm::vec3 pos = cosf(theta) * tangent * radius + sinf(theta) * cotangent * radius;
        circleEdges.push_back(pos);
    }

    // Add front side circle vertices
    for (uint side = 0; side < sides; side++) {
        modelData.vertices.push_back(Vertex{
                centre + 0.5f * length + circleEdges[side],
                glm::vec3{0},
                glm::vec2{0},
        });
    }

    // Add back side circle vertices
    for (uint side = 0; side < sides; side++) {
        modelData.vertices.push_back(Vertex{
                centre - 0.5f * length + circleEdges[side],
                glm::vec3{0},
                glm::vec2{0},
        });
    }

    // Add front side circle centre
    size_t front_centre_index = modelData.vertices.size();
    modelData.vertices.push_back(Vertex{
            centre + 0.5f * length,
            glm::vec3{0},
            glm::vec2{0},
    });

    // Add back side circle centre
    size_t back_centre_index = modelData.vertices.size();
    modelData.vertices.push_back(Vertex{
            centre - 0.5f * length,
            glm::vec3{0},
            glm::vec2{0},
    });

    //Generate front triangles
    for (uint side = 0; side < sides; side++) {
        modelData.indices.push_back(front_centre_index);
        modelData.indices.push_back(side);
        modelData.indices.push_back((side + 1) % sides);
    }

    //Generate back triangles
    for (uint side = 0; side < sides; side++) {
        modelData.indices.push_back(back_centre_index);
        modelData.indices.push_back(sides + ((side + 1) % sides));
        modelData.indices.push_back(sides + side); // Note: reverse order of last two compared to front is correct
    }

    // Generate side rectangles
    for (uint side = 0; side < sides; side++) {
        size_t TL = side;
        size_t BL = side + sides;
        size_t BR = ((side + 1) % sides) + sides;
        size_t TR = ((side + 1) % sides);

        modelData.indices.push_back(TL);
        modelData.indices.push_back(BL);
        modelData.indices.push_back(TR);

        modelData.indices.push_back(TR);
        modelData.indices.push_back(BL);
        modelData.indices.push_back(BR);
    }

    // Calc meta data
    for (const auto& vertex : modelData.vertices) {
        modelMetaData.boundingAABB.min = glm::min(modelMetaData.boundingAABB.min, vertex.position);
        modelMetaData.boundingAABB.max = glm::max(modelMetaData.boundingAABB.max, vertex.position);
    }

    return Mesh(modelData, modelMetaData);
}

Mesh MeshGenerator::genOpenCylinderNoNormNoTex(glm::vec3 centre, glm::vec3 length, float radius, bool doubleSided, uint sides) {
    ModelData modelData{};
    ModelMetaData modelMetaData{};

    // Calc temp direction that is not parallel to length
    glm::vec3 tempVec;
    if (length.y == 0.0f && length.z == 0.0f) {
        tempVec = {0.0f, 1.0f, 0.0f};
    } else {
        tempVec = {1.0f, 0.0f, 0.0f};
    }

    // Calc directions
    glm::vec3 tangent = glm::normalize(glm::cross(tempVec, length));
    glm::vec3 cotangent = glm::cross(glm::normalize(length), tangent);

    // Calculate points of an aligned circle at origin
    std::vector<glm::vec3> circleEdges{};
    for (uint side = 0; side < sides; side++) {
        float theta = ((float) side / (float) sides) * 2.0f * (float) M_PI;
        glm::vec3 pos = cosf(theta) * tangent * radius + sinf(theta) * cotangent * radius;
        circleEdges.push_back(pos);
    }

    // Add front side circle vertices
    for (uint side = 0; side < sides; side++) {
        modelData.vertices.push_back(Vertex{
                centre + 0.5f * length + circleEdges[side],
                glm::vec3{0},
                glm::vec2{0},
        });
    }

    // Add back side circle vertices
    for (uint side = 0; side < sides; side++) {
        modelData.vertices.push_back(Vertex{
                centre - 0.5f * length + circleEdges[side],
                glm::vec3{0},
                glm::vec2{0},
        });
    }

    // Generate side rectangles
    for (uint side = 0; side < sides; side++) {
        size_t TL = side;
        size_t BL = side + sides;
        size_t BR = ((side + 1) % sides) + sides;
        size_t TR = ((side + 1) % sides);

        modelData.indices.push_back(TL);
        modelData.indices.push_back(BL);
        modelData.indices.push_back(TR);

        modelData.indices.push_back(TR);
        modelData.indices.push_back(BL);
        modelData.indices.push_back(BR);

        if (doubleSided) {
            modelData.indices.push_back(TR);
            modelData.indices.push_back(BL);
            modelData.indices.push_back(TL);

            modelData.indices.push_back(BR);
            modelData.indices.push_back(BL);
            modelData.indices.push_back(TR);
        }
    }

    // Calc meta data
    for (const auto& vertex : modelData.vertices) {
        modelMetaData.boundingAABB.min = glm::min(modelMetaData.boundingAABB.min, vertex.position);
        modelMetaData.boundingAABB.max = glm::max(modelMetaData.boundingAABB.max, vertex.position);
    }

    return Mesh(modelData, modelMetaData);
}

Mesh MeshGenerator::genConeNoNormNoTex(glm::vec3 baseCentre, glm::vec3 height, float radius, uint sides) {
    ModelData modelData{};
    ModelMetaData modelMetaData{};

    // Calc temp direction that is not parallel to length
    glm::vec3 tempVec;
    if (height.y == 0.0f && height.z == 0.0f) {
        tempVec = {0.0f, 1.0f, 0.0f};
    } else {
        tempVec = {1.0f, 0.0f, 0.0f};
    }

    // Calc directions
    glm::vec3 tangent = glm::normalize(glm::cross(tempVec, height));
    glm::vec3 cotangent = glm::cross(glm::normalize(height), tangent);

    // Calculate points of an aligned circle at origin
    std::vector<glm::vec3> circleEdges{};
    for (uint side = 0; side < sides; side++) {
        float theta = ((float) side / (float) sides) * 2.0f * (float) M_PI;
        glm::vec3 pos = cosf(theta) * tangent * radius + sinf(theta) * cotangent * radius;
        circleEdges.push_back(pos);
    }

    // Add base circle vertices
    for (uint side = 0; side < sides; side++) {
        modelData.vertices.push_back(Vertex{
                baseCentre + circleEdges[side],
                glm::vec3{0},
                glm::vec2{0},
        });
    }

    // Add base circle  centre
    size_t base_centre_index = modelData.vertices.size();
    modelData.vertices.push_back(Vertex{
            baseCentre,
            glm::vec3{0},
            glm::vec2{0},
    });

    // Add point centre
    size_t point_centre_index = modelData.vertices.size();
    modelData.vertices.push_back(Vertex{
            baseCentre + height,
            glm::vec3{0},
            glm::vec2{0},
    });

    //Generate base triangles
    for (uint side = 0; side < sides; side++) {
        modelData.indices.push_back(base_centre_index);
        modelData.indices.push_back((side + 1) % sides);
        modelData.indices.push_back(side);
    }

    // Generate side triangles
    for (uint side = 0; side < sides; side++) {
        modelData.indices.push_back(point_centre_index);
        modelData.indices.push_back(side);
        modelData.indices.push_back((side + 1) % sides);
    }

    // Calc meta data
    for (const auto& vertex : modelData.vertices) {
        modelMetaData.boundingAABB.min = glm::min(modelMetaData.boundingAABB.min, vertex.position);
        modelMetaData.boundingAABB.max = glm::max(modelMetaData.boundingAABB.max, vertex.position);
    }

    return Mesh(modelData, modelMetaData);
}

Mesh MeshGenerator::genParallelepipedNoNormNoTex(glm::vec3 centre, glm::vec3 side1, glm::vec3 side2, glm::vec3 side3) {
    ModelData modelData{};
    ModelMetaData modelMetaData{};

    glm::vec3 centerOffset = 0.5f * side1 + 0.5f * side2 + 0.5f * side3;
    glm::vec3 sides[] = {side1, side2, side3};

    size_t corners[2][2][2];

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            for (int k = 0; k < 2; k++) {
                corners[i][j][k] = modelData.vertices.size();
                modelData.vertices.push_back({
                    centre - centerOffset + (float) i * side1 + (float) j * side2 + (float) k * side3,
                    glm::vec3{0.0f},
                    glm::vec3{0.0f}
                });
            }
        }
    }

    size_t faceCorners[6][4] = {
            { corners[0][0][0], corners[0][0][1], corners[0][1][1], corners[0][1][0] },
            { corners[1][0][0], corners[1][0][1], corners[1][1][1], corners[1][1][0] },
            { corners[0][0][0], corners[0][0][1], corners[1][0][1], corners[1][0][0] },
            { corners[0][1][0], corners[0][1][1], corners[1][1][1], corners[1][1][0] },
            { corners[0][0][0], corners[0][1][0], corners[1][1][0], corners[1][0][0] },
            { corners[0][0][1], corners[0][1][1], corners[1][1][1], corners[1][0][1] },
    };

    for (int face = 0; face < 6; face++) {
        glm::vec3 edge1 = modelData.vertices[faceCorners[face][0]].position - modelData.vertices[faceCorners[face][1]].position;
        glm::vec3 edge2 = modelData.vertices[faceCorners[face][2]].position - modelData.vertices[faceCorners[face][1]].position;

        glm::vec3 cross = glm::cross(edge1, edge2);

        glm::vec3 middle = centre - modelData.vertices[faceCorners[face][1]].position;

        if (glm::dot(cross, middle) > 0.0f) {
            modelData.indices.push_back(faceCorners[face][0]);
            modelData.indices.push_back(faceCorners[face][1]);
            modelData.indices.push_back(faceCorners[face][2]);

            modelData.indices.push_back(faceCorners[face][0]);
            modelData.indices.push_back(faceCorners[face][2]);
            modelData.indices.push_back(faceCorners[face][3]);
        } else {
            modelData.indices.push_back(faceCorners[face][2]);
            modelData.indices.push_back(faceCorners[face][1]);
            modelData.indices.push_back(faceCorners[face][0]);

            modelData.indices.push_back(faceCorners[face][3]);
            modelData.indices.push_back(faceCorners[face][2]);
            modelData.indices.push_back(faceCorners[face][0]);
        }
    }

    return Mesh(modelData, modelMetaData);
}

Mesh MeshGenerator::genSquareToroidNoNormNoTex(glm::vec3 centre, glm::vec3 length, float averageRadius, float thickness, uint sides) {
    ModelData modelData{};
    ModelMetaData modelMetaData{};

    float innerRadius = averageRadius - 0.5f * std::abs(thickness);
    float outerRadius = averageRadius + 0.5f * std::abs(thickness);

    // Calc temp direction that is not parallel to length
    glm::vec3 tempVec;
    if (length.y == 0.0f && length.z == 0.0f) {
        tempVec = {0.0f, 1.0f, 0.0f};
    } else {
        tempVec = {1.0f, 0.0f, 0.0f};
    }

    // Calc directions
    glm::vec3 tangent = glm::normalize(glm::cross(tempVec, length));
    glm::vec3 cotangent = glm::cross(glm::normalize(length), tangent);

    // Calculate points of an aligned circle at origin
    std::vector<glm::vec3> innerCircleEdges{};
    std::vector<glm::vec3> outerCircleEdges{};
    for (uint side = 0; side < sides; side++) {
        float theta = ((float) side / (float) sides) * 2.0f * (float) M_PI;
        glm::vec3 innerPos = cosf(theta) * tangent * innerRadius + sinf(theta) * cotangent * innerRadius;
        glm::vec3 outerPos = cosf(theta) * tangent * outerRadius + sinf(theta) * cotangent * outerRadius;
        innerCircleEdges.push_back(innerPos);
        outerCircleEdges.push_back(outerPos);
    }

    // Outer side
    {
        // Add outer front side circle vertices
        for (uint side = 0; side < sides; side++) {
            modelData.vertices.push_back(Vertex{
                    centre + 0.5f * length + outerCircleEdges[side],
                    glm::vec3{0},
                    glm::vec2{0},
            });
        }

        // Add outer back side circle vertices
        for (uint side = 0; side < sides; side++) {
            modelData.vertices.push_back(Vertex{
                    centre - 0.5f * length + outerCircleEdges[side],
                    glm::vec3{0},
                    glm::vec2{0},
            });
        }

        // Generate outer side rectangles
        for (uint side = 0; side < sides; side++) {
            size_t TL = side;
            size_t BL = side + sides;
            size_t BR = ((side + 1) % sides) + sides;
            size_t TR = ((side + 1) % sides);

            modelData.indices.push_back(TL);
            modelData.indices.push_back(BL);
            modelData.indices.push_back(TR);

            modelData.indices.push_back(TR);
            modelData.indices.push_back(BL);
            modelData.indices.push_back(BR);
        }
    }

    // Inner side
    {
        size_t indexOffset = modelData.vertices.size();

        // Add inner front side circle vertices
        for (uint side = 0; side < sides; side++) {
            modelData.vertices.push_back(Vertex{
                    centre + 0.5f * length + innerCircleEdges[side],
                    glm::vec3{0},
                    glm::vec2{0},
            });
        }

        // Add inner back side circle vertices
        for (uint side = 0; side < sides; side++) {
            modelData.vertices.push_back(Vertex{
                    centre - 0.5f * length + innerCircleEdges[side],
                    glm::vec3{0},
                    glm::vec2{0},
            });
        }

        // Generate inner side rectangles
        for (uint side = 0; side < sides; side++) {
            size_t TL = side;
            size_t BL = side + sides;
            size_t BR = ((side + 1) % sides) + sides;
            size_t TR = ((side + 1) % sides);

            modelData.indices.push_back(TR + indexOffset);
            modelData.indices.push_back(BL + indexOffset);
            modelData.indices.push_back(TL + indexOffset);

            modelData.indices.push_back(BR + indexOffset);
            modelData.indices.push_back(BL + indexOffset);
            modelData.indices.push_back(TR + indexOffset);
        }
    }

    // Front side
    for (uint side = 0; side < sides; side++) {
        size_t TL = side;
        size_t BL = side + sides * 2;
        size_t BR = ((side + 1) % sides) + sides * 2;
        size_t TR = ((side + 1) % sides);

        modelData.indices.push_back(TR);
        modelData.indices.push_back(BL);
        modelData.indices.push_back(TL);

        modelData.indices.push_back(BR);
        modelData.indices.push_back(BL);
        modelData.indices.push_back(TR);
    }

    // Back side
    for (uint side = 0; side < sides; side++) {
        size_t TL = side + sides;
        size_t BL = side + sides * 3;
        size_t BR = ((side + 1) % sides) + sides * 3;
        size_t TR = ((side + 1) % sides) + sides;

        modelData.indices.push_back(TL);
        modelData.indices.push_back(BL);
        modelData.indices.push_back(TR);

        modelData.indices.push_back(TR);
        modelData.indices.push_back(BL);
        modelData.indices.push_back(BR);
    }

    // Calc meta data
    for (const auto& vertex : modelData.vertices) {
        modelMetaData.boundingAABB.min = glm::min(modelMetaData.boundingAABB.min, vertex.position);
        modelMetaData.boundingAABB.max = glm::max(modelMetaData.boundingAABB.max, vertex.position);
    }

    return Mesh(modelData, modelMetaData);
}
