#ifndef MESH_GENERATOR_H
#define MESH_GENERATOR_H

#include "../rendering/Model.h"

struct Mesh {
    ModelData modelData;
    ModelMetaData modelMetaData;

    Mesh(ModelData modelData, const ModelMetaData &modelMetaData) : modelData(std::move(modelData)), modelMetaData(modelMetaData) {}
};

class MeshGenerator {
public:
    static Mesh mergeMeshs(std::vector<Mesh> meshes);

    static Mesh genParallelogramNoNormNoTex(glm::vec3 position, glm::vec3 norm, glm::vec3 tangent, float sideLength, bool doubleSided = false);
    static Mesh genCylinderNoNormNoTex(glm::vec3 centre, glm::vec3 length, float radius, uint sides = 16);
    static Mesh genOpenCylinderNoNormNoTex(glm::vec3 centre, glm::vec3 length, float radius, bool doubleSided = false, uint sides = 16);
    static Mesh genConeNoNormNoTex(glm::vec3 baseCentre, glm::vec3 height, float radius, uint sides = 16);
    static Mesh genParallelepipedNoNormNoTex(glm::vec3 centre, glm::vec3 side1, glm::vec3 side2, glm::vec3 side3);
    static Mesh genSquareToroidNoNormNoTex(glm::vec3 centre, glm::vec3 length, float averageRadius, float thickness, uint sides = 16);
};

#endif //MESH_GENERATOR_H
