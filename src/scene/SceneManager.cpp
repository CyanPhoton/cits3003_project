#include "SceneManager.h"

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "../rendering/imgui/ImGuiManager.h"
#include "MeshGenerator.h"

const size_t SceneManager::DEFAULT_TEXTURE = 0;

const size_t SceneManager::GROUND_MODEL = 0;
const size_t SceneManager::SPHERE_MODEL = 55;
const size_t SceneManager::DEFAULT_MODEL = SceneManager::SPHERE_MODEL;
const size_t SceneManager::LIGHT_MODEL = SceneManager::SPHERE_MODEL;

const float SceneManager::FLOOR_SCALE = 10.0f;
const float SceneManager::SPHERE_SCALE = 0.1f;

const float SceneManager::OBJECT_SCALE = 0.005f;

static bool disablePushed = false;

#define PUSH_DISABLE(should_disable)\
disablePushed = should_disable;\
if (disablePushed) {\
    ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);\
    ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);\
}

#define POP_DISABLE()\
if (disablePushed) {\
    disablePushed = false;\
    ImGui::PopItemFlag();\
    ImGui::PopStyleVar();\
}

void SceneManager::SceneObject::updateData(MasterRenderer &masterRenderer) const {
    float baseScale = OBJECT_SCALE;
    if (modelIndex == SPHERE_MODEL) {
        baseScale = SPHERE_SCALE;
    }

    glm::mat4 modelMatrix = glm::translate(position) * glm::scale(glm::vec3{scale * baseScale})
                            * glm::rotate(eulerRotation.x, glm::vec3{1.0f, 0.0f, 0.0f})
                            * glm::rotate(eulerRotation.y, glm::vec3{0.0f, 1.0f, 0.0f})
                            * glm::rotate(eulerRotation.z, glm::vec3{0.0f, 0.0f, 1.0f});

    EntityData &entity = masterRenderer.getEntity(entityID);
    masterRenderer.setEntityModelMatrix(entityID, modelMatrix);

    entity.tintColour_TexScale = glm::vec4{tint, textureScale};
    entity.diffuseFactor = diffuseFactor * totalBrightnessFactor;
    entity.specularFactor = specularFactor * totalBrightnessFactor;
    entity.globalFactor = ambientFactor * totalBrightnessFactor;
    entity.shininessExponent = shininessExponent;
}

void SceneManager::SceneLight::updateData(MasterRenderer &masterRenderer) const {
    masterRenderer.changeLightType(lightID, makeVariant());

    FlatEntityData &entityData = masterRenderer.getFlatEntity(entityID);

    glm::mat4 modelMatrix = glm::scale(glm::vec3{SPHERE_SCALE});
    if (type == LightType::DirectionalLight) {
        float horizontalLen = cosf(direction.y);
        glm::vec3 directionVec{horizontalLen * cosf(direction.x), sinf(direction.y), horizontalLen * sinf(direction.x)};

        modelMatrix = glm::translate(-directionVec) * modelMatrix;
    } else {
        modelMatrix = glm::translate(position) * modelMatrix;
    }

    masterRenderer.setEntityModelMatrix(entityID, modelMatrix);

    float brightnessScale = std::max(std::max(colour.x, colour.y), colour.z);
    glm::vec3 brightColour = colour;
    if (brightnessScale != 0.0f) {
        brightColour /= brightnessScale;
    }

    entityData.colour = glm::vec4{brightColour, 1.0f};
}

LightVariant SceneManager::SceneLight::makeVariant() const {

    float horizontalLen = cosf(direction.y);

    glm::vec3 directionVec{horizontalLen * cosf(direction.x), sinf(direction.y), horizontalLen * sinf(direction.x)};

    switch (type) {
        case PointLight:
            return PointLightData{
                    position,
                    colour * brightness,
                    attenuation
            };
        case DirectionalLight:
            return DirectionalLightData{
                    directionVec,
                    colour * brightness
            };
        case ConeLight:
            return ConeLightData{
                    glm::vec4{position, softness},
                    glm::vec4{directionVec, cosf(halfAngle)},
                    colour * brightness,
                    attenuation
            };
    }

    return LightVariant{};
}

SceneManager::SceneManager(MasterRenderer &masterRenderer)
        : moveGizmo(GizmoManager::createMoveGizmo(masterRenderer)),
          scaleGizmo(GizmoManager::createScaleGizmo(masterRenderer)),
          rotateGizmo(GizmoManager::createRotateGizmo(masterRenderer)),
          yawPitchGizmo(GizmoManager::createYawPitchGizmo(masterRenderer)) {
    textureInfos = {
            //<editor-fold desc="Texture Infos">
            TextureInfo{"0 White", "texture.bmp"},
            TextureInfo{"1 Plain", "texture1.bmp"},
            TextureInfo{"2 Rust", "texture2.bmp"},
            TextureInfo{"3 Concrete", "texture3.bmp"},
            TextureInfo{"4 Carpet", "texture4.bmp"},
            TextureInfo{"5 Beach Sand", "texture5.bmp"},
            TextureInfo{"6 Rocky", "texture6.bmp"},
            TextureInfo{"7 Brick", "texture7.bmp"},
            TextureInfo{"8 Water", "texture8.bmp"},
            TextureInfo{"9 Paper", "texture9.bmp"},
            TextureInfo{"10 Marble", "texture10.bmp"},
            TextureInfo{"11 Wood", "texture11.bmp"},
            TextureInfo{"12 Scales", "texture12.bmp"},
            TextureInfo{"13 Fur", "texture13.bmp"},
            TextureInfo{"14 Denim", "texture14.bmp"},
            TextureInfo{"15 Hessian", "texture15.bmp"},
            TextureInfo{"16 Orange Peel", "texture16.bmp"},
            TextureInfo{"17 Ice Crystals", "texture17.bmp"},
            TextureInfo{"18 Grass", "texture18.bmp"},
            TextureInfo{"19 Corrugated Iron", "texture19.bmp"},
            TextureInfo{"20 Styrofoam", "texture20.bmp"},
            TextureInfo{"21 Bubble Wrap", "texture21.bmp"},
            TextureInfo{"22 Leather", "texture22.bmp"},
            TextureInfo{"23 Camouflage", "texture23.bmp"},
            TextureInfo{"24 Asphalt", "texture24.bmp"},
            TextureInfo{"25 Scratched Ice", "texture25.bmp"},
            TextureInfo{"26 Rattan", "texture26.bmp"},
            TextureInfo{"27 Snow", "texture27.bmp"},
            TextureInfo{"28 Dry Mud", "texture28.bmp"},
            TextureInfo{"29 Old Concrete", "texture29.bmp"},
            TextureInfo{"30 Leopard Skin", "texture30.bmp"},
            //</editor-fold>
    };

    modelInfos = {
            //<editor-fold desc="Model Infos">
            ModelInfo{"0 2D Plane", "model0.x"},
            ModelInfo{"1 Thin Dinosaur", "model1.x"},
            ModelInfo{"2 Big Dog", "model2.x"},
            ModelInfo{"3 Saddle Dinosaur", "model3.x"},
            ModelInfo{"4 Dragon", "model4.x"},
            ModelInfo{"5 Cleopatra", "model5.x"},
            ModelInfo{"6 Bone I", "model6.x"},
            ModelInfo{"7 Bone II", "model7.x"},
            ModelInfo{"8 Rabbit", "model8.x"},
            ModelInfo{"9 Long Dragon", "model9.x"},
            ModelInfo{"10 Buddha", "model10.x"},
            ModelInfo{"11 Sitting Rabbit", "model11.x"},
            ModelInfo{"12 Frog", "model12.x"},
            ModelInfo{"13 Cow", "model13.x"},
            ModelInfo{"14 Monster", "model14.x"},
            ModelInfo{"15 Sea Horse", "model15.x"},
            ModelInfo{"16 Head", "model16.x"},
            ModelInfo{"17 Pelican", "model17.x"},
            ModelInfo{"18 Horse", "model18.x"},
            ModelInfo{"19 Kneeling Angel", "model19.x"},
            ModelInfo{"20 Porsche I", "model20.x"},
            ModelInfo{"21 Truck", "model21.x"},
            ModelInfo{"22 Statue of Liberty", "model22.x"},
            ModelInfo{"23 Sitting Angel", "model23.x"},
            ModelInfo{"24 Metal Part", "model24.x"},
            ModelInfo{"25 Car", "model25.x"},
            ModelInfo{"26 Apatosaurus", "model26.x"},
            ModelInfo{"27 Airliner", "model27.x"},
            ModelInfo{"28 Motorbike", "model28.x"},
            ModelInfo{"29 Dolphin", "model29.x"},
            ModelInfo{"30 Spaceman", "model30.x"},
            ModelInfo{"31 Winnie the Pooh", "model31.x"},
            ModelInfo{"32 Shark", "model32.x"},
            ModelInfo{"33 Crocodile", "model33.x"},
            ModelInfo{"34 Toddler", "model34.x"},
            ModelInfo{"35 Fat Dinosaur", "model35.x"},
            ModelInfo{"36 Chihuahua", "model36.x"},
            ModelInfo{"37 Sabre-toothed Tiger", "model37.x"},
            ModelInfo{"38 Lioness", "model38.x"},
            ModelInfo{"39 Fish", "model39.x"},
            ModelInfo{"40 Horse (head down)", "model40.x"},
            ModelInfo{"41 Horse (head up)", "model41.x"},
            ModelInfo{"42 Skull", "model42.x"},
            ModelInfo{"43 Fighter Jet I", "model43.x"},
            ModelInfo{"44 Toad", "model44.x"},
            ModelInfo{"45 Convertible", "model45.x"},
            ModelInfo{"46 Porsche II", "model46.x"},
            ModelInfo{"47 Hare", "model47.x"},
            ModelInfo{"48 Vintage Car", "model48.x"},
            ModelInfo{"49 Fighter Jet II", "model49.x"},
            ModelInfo{"50 Gargoyle", "model50.x"},
            ModelInfo{"51 Chef", "model51.x"},
            ModelInfo{"52 Parasaurolophus", "model52.x"},
            ModelInfo{"53 Rooster", "model53.x"},
            ModelInfo{"54 T-rex", "model54.x"},
            ModelInfo{"55 Sphere", "model55.x"},
            //</editor-fold>
    };

    masterRenderer.setSelectionHighlightThickness(baseSelectionThickness);

    textureInfos[DEFAULT_TEXTURE].essential = "Default Fallback Texture";
    textureInfos[DEFAULT_TEXTURE].loaded = masterRenderer.loadTexture(textureInfos[DEFAULT_TEXTURE].location);

    modelInfos[DEFAULT_MODEL].essential = "Default Fallback Model";
    modelInfos[DEFAULT_MODEL].loaded = masterRenderer.loadModel(modelInfos[DEFAULT_MODEL].location);

    if (!modelInfos[LIGHT_MODEL].loaded.has_value()) {
        modelInfos[LIGHT_MODEL].essential = "Light Object Model";
        modelInfos[LIGHT_MODEL].loaded = masterRenderer.loadModel(modelInfos[LIGHT_MODEL].location);
    }

    if (!modelInfos[GROUND_MODEL].loaded.has_value()) {
        modelInfos[GROUND_MODEL].essential = "Ground Model";
        modelInfos[GROUND_MODEL].loaded = masterRenderer.loadModel(modelInfos[GROUND_MODEL].location);
    }

    const ModelMetaData::AABB &sphereAABB = masterRenderer.getModelMeta(modelInfos[SPHERE_MODEL].loaded.value()).boundingAABB;
    sphereRadius = (sphereAABB.max.y - sphereAABB.min.y) / 2.0f;

    SceneObject groundObject = newSceneObject(masterRenderer, "Ground", GROUND_MODEL, DEFAULT_TEXTURE);
    groundObject.canChangeModel = false;
    groundObject.canRemove = false;
    groundObject.canMove = false;

    groundObject.eulerRotation.x = glm::radians(-90.0f);
    groundObject.scale = glm::vec3{FLOOR_SCALE / OBJECT_SCALE};
    groundObject.updateData(masterRenderer);
    sceneObjects.insert({groundObject.entityID, groundObject});

//    auto testMesh = MeshGenerator::genParallelogramNoNormNoTex({0, 0, 0}, {1, sqrt(2.0f), 1}, {1, -sqrt(2.0f), 1}, 15.0f, true);
//    auto testMesh = MeshGenerator::genCylinderNoNormNoTex({0,4.0f,0}, {2.0f, 2.0f, 2.0f}, 2.0f, 1024);
//    auto testMesh = MeshGenerator::genConeNoNormNoTex({0,4.0f,0}, {2.0f, 2.0f, 2.0f}, 2.0f, 16);

//    auto testModel = masterRenderer.loadModel(testMesh.modelData, testMesh.modelMetaData);

//    masterRenderer.addEntity(testModel, textureInfos[DEFAULT_TEXTURE].loaded.value(), EntityData::identity());
//    moveGizmo.enable(masterRenderer, true);
}

void SceneManager::toolUpdate(MasterRenderer &masterRenderer, const Window &window, const Camera &camera) {
    bool selected = selection.has_value() && currentSelectionType != SelectionType::None;

    bool canMove = (currentSelectionType == SelectionType::Object && sceneObjects[selection.value()].canMove)
                   ||
                   (currentSelectionType == SelectionType::Light && (sceneLights[selection.value()].type == LightType::PointLight || sceneLights[selection.value()].type == LightType::ConeLight));

    bool canScale = (currentSelectionType == SelectionType::Object && sceneObjects[selection.value()].canMove);

    bool canRotate = (currentSelectionType == SelectionType::Object && sceneObjects[selection.value()].canMove)
                     ||
                     (currentSelectionType == SelectionType::Light && (sceneLights[selection.value()].type == LightType::DirectionalLight || sceneLights[selection.value()].type == LightType::ConeLight));

    if (ImGui::BeginPopupContextVoid("Tool Context Popup", ImGuiMouseButton_Right)) {

        if (ImGui::MenuItem("No Tool", "V")) {
            toolMode = ToolMode::None;
        }

        if (ImGui::MenuItem("Move Tool", "Z", nullptr, selected && canMove)) {
            toolMode = ToolMode::Move;
        }

        if (ImGui::MenuItem("Scale Tool", "X", nullptr, selected && canScale)) {
            toolMode = ToolMode::Scale;
        }

        if (ImGui::MenuItem("Rotate Tool", "C", nullptr, selected && canRotate)) {
            toolMode = ToolMode::Rotate;
        }

        if (ImGui::MenuItem("Select Tool", "Q")) {
            if (toolMode != ToolMode::Select) {
                oldSelection = selection;
            }
            toolMode = ToolMode::Select;
        }

        if (ImGui::MenuItem("Clear Selection", "B")) {
            selection = {};
            currentSelectionType = SelectionType::None;
            toolMode = ToolMode::None;
        }

        ImGui::EndPopup();
    }

    if (window.wasKeyPressed(GLFW_KEY_V)) {
        toolMode = ToolMode::None;
    } else if (window.wasKeyPressed(GLFW_KEY_Z) && selected && canMove) {
        toolMode = ToolMode::Move;
    } else if (window.wasKeyPressed(GLFW_KEY_X) && selected && canScale) {
        toolMode = ToolMode::Scale;
    } else if (window.wasKeyPressed(GLFW_KEY_C) && selected && canRotate) {
        toolMode = ToolMode::Rotate;
    } else if (window.wasKeyPressed(GLFW_KEY_Q)) {
        if (toolMode != ToolMode::Select) {
            oldSelection = selection;
        }
        toolMode = ToolMode::Select;
    } else if (window.wasKeyPressed(GLFW_KEY_B)) {
        selection = {};
        currentSelectionType = SelectionType::None;
        toolMode = ToolMode::None;

    }

    switch (currentSelectionType) {
        case SelectionType::Object:
            if (!sceneObjects[selection.value()].canMove) {
                if (toolMode == ToolMode::Move || toolMode == ToolMode::Rotate || toolMode == ToolMode::Scale) {
                    toolMode = ToolMode::None;
                }
            }
            break;
        case SelectionType::Light:
            switch (sceneLights[selection.value()].type) {
                case PointLight:
                    if (toolMode == ToolMode::Rotate) {
                        toolMode = ToolMode::None;
                    }
                    break;
                case DirectionalLight:
                    if (toolMode == ToolMode::Move) {
                        toolMode = ToolMode::None;
                    }
                    break;
                case ConeLight:
                    break;
            }
            if (toolMode == ToolMode::Scale) {
                toolMode = ToolMode::None;
            }
            break;
        case SelectionType::None:
            break;
    }

    GizmoManager::update(masterRenderer);

    GizmoManager::getPointerDirViewSpace(masterRenderer, window);

    switch (toolMode) {
        case ToolMode::None:
            break;
        case ToolMode::Move:
            if (selection.has_value()) {
                switch (currentSelectionType) {
                    case SelectionType::Object: {
                        SceneObject &sceneObject = sceneObjects[selection.value()];

                        if (moveGizmo.update(masterRenderer, window, sceneObject.position, overallGizmoScale)) {
                            sceneObject.updateData(masterRenderer);
                        }
                        break;
                    }
                    case SelectionType::Light: {
                        SceneLight &sceneLight = sceneLights[selection.value()];

                        switch (sceneLight.type) {
                            case PointLight:
                                if (moveGizmo.update(masterRenderer, window, sceneLight.position, overallGizmoScale)) {
                                    sceneLight.updateData(masterRenderer);
                                }
                                break;
                            case DirectionalLight:
                                break;
                            case ConeLight:
                                if (moveGizmo.update(masterRenderer, window, sceneLight.position, overallGizmoScale)) {
                                    sceneLight.updateData(masterRenderer);
                                }
                                break;
                        }

                        break;
                    }
                    case SelectionType::None:
                        break;
                }
            }
            break;
        case ToolMode::Scale:
            if (selection.has_value()) {
                switch (currentSelectionType) {
                    case SelectionType::Object:
                        if (scaleGizmo.update(masterRenderer, window, sceneObjects[selection.value()].position, sceneObjects[selection.value()].scale, overallGizmoScale)) {
                            sceneObjects[selection.value()].updateData(masterRenderer);
                        }
                        break;
                    case SelectionType::Light:
                        break;
                    case SelectionType::None:
                        break;
                }
            }
            break;
        case ToolMode::Rotate:
            if (selection.has_value()) {
                switch (currentSelectionType) {
                    case SelectionType::Object: {
                        SceneObject &sceneObject = sceneObjects[selection.value()];

                        if (rotateGizmo.update(masterRenderer, window, sceneObject.position, sceneObject.eulerRotation, overallGizmoScale)) {
                            sceneObject.updateData(masterRenderer);
                        }
                        break;
                    }
                    case SelectionType::Light: {
                        SceneLight &sceneLight = sceneLights[selection.value()];

                        switch (sceneLight.type) {
                            case PointLight:
                                break;
                            case DirectionalLight: {
                                float pitchTemp = sceneLight.direction.y + M_PI;
                                if (yawPitchGizmo.update(masterRenderer, window, camera, glm::vec3{0.0f}, sceneLight.direction.x, pitchTemp, overallGizmoScale, false)) {
                                    sceneLight.updateData(masterRenderer);
                                    sceneLight.direction.y = sceneLight.direction.y + (pitchTemp - (sceneLight.direction.y + M_PI));

                                    sceneLight.direction.y += M_PI;
                                    PERIOD(sceneLight.direction.y, 2.0f * M_PI);
                                    sceneLight.direction.y -= M_PI;
                                }
                                break;
                            }
                            case ConeLight:\
                                if (yawPitchGizmo.update(masterRenderer, window, camera, sceneLight.position, sceneLight.direction.x, sceneLight.direction.y, overallGizmoScale)) {
                                    sceneLight.updateData(masterRenderer);
                                }
                                break;
                        }

                        break;
                    }
                    case SelectionType::None:
                        break;
                }
            }
            break;
        case ToolMode::Select:
            auto entityID = masterRenderer.getPickedEntity();
            if (entityID.has_value()) {
                EntityID entityIDVal = entityID.value();

                masterRenderer.setSelection({entityIDVal});

                selection = entityIDVal;

                if (sceneObjects.count(entityIDVal) > 0) {
                    currentSelectionType = SelectionType::Object;
                } else if (sceneLights.count(entityIDVal) > 0) {
                    currentSelectionType = SelectionType::Light;
                } else {
                    currentSelectionType = SelectionType::None;

                    selection = {};
                }
            } else {
                currentSelectionType = SelectionType::None;
                selection = {};
                masterRenderer.setSelection({});
            }

            if (window.isMousePressed(GLFW_MOUSE_BUTTON_LEFT)) {
                toolMode = ToolMode::None;

                if (!selection.has_value()) {
                    selection = oldSelection;
                }
            }

            if (selection.has_value()) {
                masterRenderer.setSelection({selection.value()});

                if (sceneObjects.count(selection.value()) > 0) {
                    currentSelectionType = SelectionType::Object;
                } else if (sceneLights.count(selection.value()) > 0) {
                    currentSelectionType = SelectionType::Light;
                } else {
                    currentSelectionType = SelectionType::None;

                    selection = {};
                }
            }

            break;
    }

    if (selection.has_value()) {
        glm::vec3 pos;
        if (currentSelectionType==SelectionType::Object) {
            pos = sceneObjects[selection.value()].position;
        } else if (currentSelectionType==SelectionType::Light) {
            pos = sceneLights[selection.value()].position;
        }

        pos = masterRenderer.getCurrentViewMatrix() * glm::vec4{pos, 1.0f};

        float viewDist = -pos.z;

        masterRenderer.setSelectionHighlightThickness(baseSelectionThickness * viewDist);
    }

}

void SceneManager::addImGui(MasterRenderer &masterRenderer) {
    if (ImGui::Begin("Selection Editor", nullptr, ImGuiWindowFlags_NoFocusOnAppearing)) {
        selectionImGui(masterRenderer);
    }
    ImGui::End();

    if (ImGui::Begin("Objects & Lights", nullptr, ImGuiWindowFlags_NoFocusOnAppearing)) {
        objectLightsImGui(masterRenderer);
    }
    ImGui::End();

    if (ImGui::Begin("Model & Textures", nullptr, ImGuiWindowFlags_NoFocusOnAppearing)) {
        modelTexturesImGui(masterRenderer);
    }
    ImGui::End();
}

void SceneManager::addImGuiSection(MasterRenderer &masterRenderer) {
    if (ImGui::CollapsingHeader("Scene Options")) {
        ImGui::SliderFloat("Selection Highlight Thickness", &baseSelectionThickness, 0.0f, 0.05f, "%.4f");

        if (ImGui::ColorEdit3("Selection Highlight Colour", &selectionColour[0])) {
            masterRenderer.setSelectionHighlightColour(selectionColour);
        }

        ImGui::DragFloat("Gizmo Scale", &overallGizmoScale, 0.001f, 0.0f, 3.0f);
    }
}

void SceneManager::selectionImGui(MasterRenderer &masterRenderer) {
    ImGui::Text("Current Selection");

    ImGui::SameLine(ImGui::GetWindowContentRegionMax().x - 150.0f);

    PUSH_DISABLE(currentSelectionType == SelectionType::None)
    if (ImGui::Button("Clear Selection", ImVec2(150, 0))) {
        currentSelectionType = SelectionType::None;
        selection = {};
    }
    POP_DISABLE()

    ImGui::NewLine();

    switch (currentSelectionType) {
        case SelectionType::Object:
            objectSelectionImGui(masterRenderer);
            break;
        case SelectionType::Light:
            lightSelectionImGui(masterRenderer);
            break;
        case SelectionType::None:
            ImGui::Text("None");
            masterRenderer.setSelection({});
            break;
    }
}

void SceneManager::objectSelectionImGui(MasterRenderer &masterRenderer) {
    ImGui::Text("Object:");

    SceneObject &obj = sceneObjects[selection.value()];

    PUSH_DISABLE(!obj.canRename)
    ImGui::InputText("Name", &obj.name, ImGuiInputTextFlags_None);
    POP_DISABLE()

    PUSH_DISABLE(!obj.canChangeModel)
    if (ImGui::BeginCombo("Model##Selection", modelInfos[obj.modelIndex].name.c_str())) {
        for (auto modelIndex = 0; modelIndex < modelInfos.size(); modelIndex++) {
            ModelInfo &modelInfo = modelInfos[modelIndex];
            if (!modelInfo.loaded.has_value())
                continue;

            bool isSelected = (modelIndex == obj.modelIndex);
            if (ImGui::Selectable(modelInfo.name.c_str(), isSelected)) {

                modelInfo.usages++;
                modelInfos[obj.modelIndex].usages--;

                obj.modelIndex = modelIndex;
                masterRenderer.changeEntityModel(obj.entityID, modelInfo.loaded.value());
                obj.updateData(masterRenderer);
            }
            if (isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }
    POP_DISABLE()

    if (ImGui::BeginCombo("Texture##Selection", textureInfos[obj.textureIndex].name.c_str())) {
        for (auto textureIndex = 0; textureIndex < textureInfos.size(); textureIndex++) {
            TextureInfo &textureInfo = textureInfos[textureIndex];
            if (!textureInfo.loaded.has_value())
                continue;

            bool isSelected = (textureIndex == obj.textureIndex);
            if (ImGui::Selectable(textureInfo.name.c_str(), isSelected)) {

                textureInfos[obj.textureIndex].usages--;
                textureInfo.usages++;

                obj.textureIndex = textureIndex;
                masterRenderer.changeEntityTexture(obj.entityID, textureInfo.loaded.value());
            }
            if (isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }

    bool needUpdate = false;

    PUSH_DISABLE(!obj.canMove)
    needUpdate |= ImGui::DragFloat3("Position", &obj.position[0], 0.01f);

    needUpdate |= ImGui::SliderFloat3("Rotation", &obj.eulerRotation[0], 0.0f, 2.0f * M_PI);

    glm::vec3 oldScale = obj.scale;
    if (ImGui::DragFloat3("Scale", &obj.scale[0], 0.01f, 0.0f, FLT_MAX)) {
        if (scaleLink) {
            if (oldScale.x != obj.scale.x) {
                float factor = obj.scale.x / oldScale.x;
                obj.scale.y *= factor;
                obj.scale.z *= factor;
            } else if (oldScale.y != obj.scale.y) {
                float factor = obj.scale.y / oldScale.y;
                obj.scale.x *= factor;
                obj.scale.z *= factor;
            } else if (oldScale.z != obj.scale.z) {
                float factor = obj.scale.z / oldScale.z;
                obj.scale.x *= factor;
                obj.scale.y *= factor;
            }
        }

        needUpdate |= true;
    }

    ImGui::SameLine();

    ImGui::Checkbox("Link", &scaleLink);

    POP_DISABLE()

    needUpdate |= ImGui::ColorEdit3("Tint", &obj.tint[0]);

    needUpdate |= ImGui::DragFloat("Diffuse Factor", &obj.diffuseFactor, 0.01f, 0.0f, FLT_MAX);

    needUpdate |= ImGui::DragFloat("Specular Factor", &obj.specularFactor, 0.01f, 0.0f, FLT_MAX);

    needUpdate |= ImGui::DragFloat("Ambient Factor", &obj.ambientFactor, 0.01f, 0.0f, FLT_MAX);

    needUpdate |= ImGui::DragFloat("Brightness Factor", &obj.totalBrightnessFactor, 0.01f, 0.0f, FLT_MAX);

    needUpdate |= ImGui::DragFloat("Shininess Exponent", &obj.shininessExponent, 1.0f, 1.0f, FLT_MAX);

    needUpdate |= ImGui::DragFloat("Texture Scale", &obj.textureScale, 0.01f, 0.0f, FLT_MAX);

    if (needUpdate) {
        obj.updateData(masterRenderer);
    }
}

void SceneManager::lightSelectionImGui(MasterRenderer &masterRenderer) {
    ImGui::Text("Light");

    SceneLight &light = sceneLights[selection.value()];
    ImGui::InputText("Name", &light.name, ImGuiInputTextFlags_None);

    bool needUpdate = false;

    const char *LIGHT_TYPES[] = {"Point Light", "Directional Light", "Spot (Cone) Light"};
    needUpdate |= ImGui::Combo("Light Type##Selection", (int *) &light.type, LIGHT_TYPES, 3);

    if (light.type == LightType::PointLight || light.type == LightType::ConeLight) {
        needUpdate |= ImGui::DragFloat3("Position", &light.position[0], 0.01f);
    }

    needUpdate |= ImGui::ColorEdit3("Colour", &light.colour[0]);

    if (light.type == LightType::DirectionalLight) {
        CLAMP(light.brightness, 0.0f, 1.0f);
        needUpdate |= ImGui::SliderFloat("Brightness", &light.brightness, 0.0f, 1.0f);
    } else {
        needUpdate |= ImGui::DragFloat("Brightness", &light.brightness, 0.1f, 0.0f, FLT_MAX, "%.3f", 4.0f);

        float intensity;
        {
            float a = light.attenuation.x;
            float b = light.attenuation.y;
            float c = light.attenuation.z;

            if (a != 0) {
                intensity = (-b + std::sqrt(b * b - 4.0f * a * (c - light.brightness))) / (2.0f * a);
            } else if (b != 0) {
                intensity = (light.brightness - c) / b;
            } else {
                intensity = std::numeric_limits<float>::infinity();
            }
        }

        PUSH_DISABLE(std::isinf(intensity))
        if (ImGui::DragFloat("Intensity", &intensity, 0.1f, 0.0f, FLT_MAX, "%.3f", 4.0f)) {
            float a = light.attenuation.x;
            float b = light.attenuation.y;
            float c = light.attenuation.z;

            light.brightness = a * intensity * intensity + b * intensity + c;

            needUpdate |= true;
        }
        POP_DISABLE()

        ImGui::SameLine();

        ImGui::HelpMarker("This is not brightness, but rather the distance at which the brightness falls to 1.\nNot a valid constraint if constant attenuation is used.");
    }

    if (light.type == LightType::PointLight || light.type == LightType::ConeLight) {
        //needUpdate |= ImGui::DragFloat3("Position", &light.position[0], 0.01f);

        const char *ATTENUATION_MODES[] = {"Constant", "Hyperbolic", "Inverse Square", "Custom"};

        const float R = sphereRadius * SPHERE_SCALE;
        const float L = LUMINANCE;

        {

            float &a = light.attenuation.x;
            float &b = light.attenuation.y;
            float &c = light.attenuation.z;

            float n = 5;
            float twoNthDist;

            if (a == 0.0f) {
                twoNthDist = (pow(2.0f, n) - c) / b;
            } else {
                twoNthDist = (-b + sqrt(b * b - 4 * a * (c - pow(2.0f, n)))) / (2 * a);
            }

            if (isnan(twoNthDist) || isinf(twoNthDist) || twoNthDist <= R) {
                twoNthDist = R + 1.0f;
            }

            const size_t SAMPLES = 100;
            float values[SAMPLES];

            for (auto i = 0; i < SAMPLES; ++i) {
                float dist = R + (twoNthDist - R) * ((float) i / (float) (SAMPLES - 1));

                values[i] = 1.0f / (a * dist * dist + b * dist + c);
            }

//                        graphName << "Test"
            ImGui::PlotLines("Attenuation Func##AttenuationGraph", values, SAMPLES, 0, nullptr, 0.0f, 1.0f, ImVec2(0, 100));
//                        ImGui::PlotHistogram("Attenuation Func##AttenuationGraph", values, SAMPLES, 0,nullptr,0.0f, 1.0f, ImVec2(0, 100));
        }

        {
            if (light.attenuationMode != SceneLight::Custom) {
                ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
            }

            glm::vec3 previousAttenuation = light.attenuation;

            float width = ImGui::CalcItemWidth();
            float spacing = ImGui::GetStyle().ItemInnerSpacing.x;

            bool attenuationChange = false;

            ImGui::PushItemWidth((width - 2.0f * spacing) / 3.0f);
            attenuationChange |= ImGui::DragFloat("##AttenuationA", &light.attenuation.x, 0.01f / (R * R * L), 0.0f, FLT_MAX);
            ImGui::SameLine(0, spacing);
            attenuationChange |= ImGui::DragFloat("##AttenuationB", &light.attenuation.y, 0.01f / (R * L), 0.0f, FLT_MAX);
            ImGui::SameLine(0, spacing);
            attenuationChange |= ImGui::DragFloat("Constants##AttenuationC", &light.attenuation.z, 0.01f, 0.0f, FLT_MAX);
            ImGui::PopItemWidth();

            if (light.attenuationMode != SceneLight::Custom) {
                ImGui::PopItemFlag();
                ImGui::PopStyleVar();
            }

            if (attenuationChange) {
                needUpdate |= true;

                // All this normalises the attenuation by making A(R) = L

                float &a = light.attenuation.x;
                float &b = light.attenuation.y;
                float &c = light.attenuation.z;

                a = std::max(a, 0.0f);
                b = std::max(b, 0.0f);
                c = std::max(c, 0.0f);

                const float &prevA = previousAttenuation.x;
                const float &prevB = previousAttenuation.y;
                const float &prevC = previousAttenuation.z;

                if (a != prevA) {
                    float lambdaA = (1.0f - L * a * R * R) / (L * prevB * R + L * prevC);
                    if (isinf(lambdaA) || isnan(lambdaA) || lambdaA < 0.0f) {
                        a = prevA;
                    } else {
                        b *= lambdaA;
                        c *= lambdaA;
                    }
                } else if (b != prevB) {
                    float lambdaB = (1.0f - L * b * R) / (L * prevA * R * R + L * prevC);
                    if (isinf(lambdaB) || isnan(lambdaB) || lambdaB < 0.0f) {
                        b = prevB;
                    } else {
                        a *= lambdaB;
                        c *= lambdaB;
                    }
                } else if (c != prevC) {
                    float lambdaC = (1.0f - L * c) / (L * prevA * R * R + L * prevB * R);
                    if (isinf(lambdaC) || isnan(lambdaC) || lambdaC < 0.0f) {
                        c = prevC;
                    } else {
                        a *= lambdaC;
                        b *= lambdaC;
                    }
                }

//                std::cout << "Normed: " << L << " = " << (a * R * R + b * R + c) << std::endl;

                light.customAttenuation = light.attenuation;
            }
        }

        if (ImGui::Combo("Attenuation Mode##", (int *) &light.attenuationMode, ATTENUATION_MODES, 4)) {
            needUpdate |= true;

            switch (light.attenuationMode) {
                case SceneLight::Constant:
                    light.attenuation = glm::vec3{0.0f, 0.0f, 1.0f};
                    break;
                case SceneLight::Linear:
                    light.attenuation = glm::vec3{0.0f, 1.0f / (R * L), 0.0f};
                    break;
                case SceneLight::InverseSquare:
                    light.attenuation = glm::vec3{1.0f / (R * R * L), 0.1f, 0.0f};
                    break;
                case SceneLight::Custom:
                    light.attenuation = light.customAttenuation;
                    break;
            }
        }
    }

    if (light.type == LightType::DirectionalLight || light.type == LightType::ConeLight) {
        needUpdate |= ImGui::SliderFloat("Direction Yaw", &light.direction.x, 0, 2.0f * M_PI);
        needUpdate |= ImGui::SliderFloat("Direction Pitch", &light.direction.y, -M_PI, M_PI);
    }

    if (light.type == LightType::ConeLight) {
        needUpdate |= ImGui::SliderFloat("Half Angle", &light.halfAngle, 0, M_PI);
        needUpdate |= ImGui::SliderFloat("Softness", &light.softness, 0, M_PI_2);
    }

    if (needUpdate) {
        light.updateData(masterRenderer);
    }
}

void SceneManager::objectLightsImGui(MasterRenderer &masterRenderer) {
    if (ImGui::ListBoxHeader("Scene Objects", 10)) {

        for (const auto &entityID_SceneObject : sceneObjects) {
            const EntityID &entityID = entityID_SceneObject.first;
            const SceneObject &sceneObject = entityID_SceneObject.second;

            bool isSelected = currentSelectionType == SelectionType::Object && selection == entityID;
            if (ImGui::Selectable(sceneObject.name.c_str(), isSelected)) {
                selection = entityID;
                currentSelectionType = SelectionType::Object;
                masterRenderer.setSelection({sceneObject.entityID});

                if (toolMode == ToolMode::Select) {
                    toolMode = ToolMode::None;
                }
            }
            if (isSelected) {
                ImGui::SetItemDefaultFocus();
            }

        }
        ImGui::ListBoxFooter();
    }

    if (ImGui::Button("Add Object")) {
        SceneObject newObject = newSceneObject(masterRenderer, "Object " + std::to_string(nextObjectNumber++), DEFAULT_MODEL, DEFAULT_TEXTURE);
        newObject.updateData(masterRenderer);
        sceneObjects.insert({newObject.entityID, newObject});

        currentSelectionType = SelectionType::Object;
        selection = newObject.entityID;
        masterRenderer.setSelection({newObject.entityID});

        if (toolMode == ToolMode::Select) {
            toolMode = ToolMode::None;
        }
    }

    ImGui::SameLine();

    PUSH_DISABLE(!selection.has_value() || currentSelectionType != SelectionType::Object)
    if (ImGui::Button("Duplicate Object")) {
        SceneObject sceneObject = sceneObjects[selection.value()];

        SceneObject newSceneObject = sceneObject;
        newSceneObject.name = std::string(sceneObject.name + " Dup");
        newSceneObject.entityID = masterRenderer.addEntity(modelInfos[sceneObject.modelIndex].loaded.value(), textureInfos[sceneObject.textureIndex].loaded.value(), masterRenderer.getEntity(sceneObject.entityID));
        newSceneObject.updateData(masterRenderer);
        sceneObjects.insert({newSceneObject.entityID, newSceneObject});
    }
    POP_DISABLE()

    ImGui::SameLine();

    PUSH_DISABLE(!selection.has_value() || currentSelectionType != SelectionType::Object || !sceneObjects[selection.value()].canRemove)
    if (ImGui::Button("Remove Object")) {
        SceneObject sceneObject = sceneObjects[selection.value()];

        sceneObjects.erase(sceneObject.entityID);

        selection = {};
        currentSelectionType = SelectionType::None;
        masterRenderer.setSelection({});

        modelInfos[sceneObject.modelIndex].usages--;
        textureInfos[sceneObject.textureIndex].usages--;

        masterRenderer.removeEntity(sceneObject.entityID);
    }
    POP_DISABLE()

    ImGui::NewLine();

    if (ImGui::ListBoxHeader("Scene Lights", 10)) {
        for (const auto &entityID_SceneLight : sceneLights) {
            const EntityID &entityID = entityID_SceneLight.first;
            const SceneLight &sceneLight = entityID_SceneLight.second;

            bool isSelected = currentSelectionType == SelectionType::Light && selection == entityID;
            if (ImGui::Selectable(sceneLight.name.c_str(), isSelected)) {
                selection = entityID;
                currentSelectionType = SelectionType::Light;
                masterRenderer.setSelection({sceneLight.entityID});

                if (toolMode == ToolMode::Select) {
                    toolMode = ToolMode::None;
                }
            }
            if (isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::ListBoxFooter();
    }

    if (ImGui::Button("Add Light")) {
        SceneLight newLight = newSceneLight(masterRenderer, "Light " + std::to_string(nextLightNumber++));
        newLight.updateData(masterRenderer);
        sceneLights.insert({newLight.entityID, newLight});

        currentSelectionType = SelectionType::Light;
        selection = newLight.entityID;
        masterRenderer.setSelection({newLight.entityID});
    }

    ImGui::SameLine();

    PUSH_DISABLE(!selection.has_value() || currentSelectionType != SelectionType::Light)
    if (ImGui::Button("Duplicate Light")) {
        SceneLight sceneLight = sceneLights[selection.value()];

        SceneLight newSceneLight = sceneLight;
        newSceneLight.name = std::string(sceneLight.name + " Dup");
        newSceneLight.lightID = masterRenderer.addLight(newSceneLight.makeVariant());
        newSceneLight.entityID = masterRenderer.addFlatEntity(modelInfos[SPHERE_MODEL].loaded.value(), masterRenderer.getFlatEntity(sceneLight.entityID));
        newSceneLight.updateData(masterRenderer);
        sceneLights.insert({newSceneLight.entityID, newSceneLight});
    }
    POP_DISABLE()

    ImGui::SameLine();

    PUSH_DISABLE(!selection.has_value() || currentSelectionType != SelectionType::Light)
    if (ImGui::Button("Remove Light")) {
        SceneLight sceneLight = sceneLights[selection.value()];

        sceneLights.erase({sceneLight.entityID});
        currentSelectionType = SelectionType::None;
        masterRenderer.setSelection({});

        modelInfos[LIGHT_MODEL].usages--;
        textureInfos[DEFAULT_TEXTURE].usages--;

        masterRenderer.removeLight(sceneLight.lightID);
        masterRenderer.removeEntity(sceneLight.entityID);
    }
    POP_DISABLE()
}

void SceneManager::modelTexturesImGui(MasterRenderer &masterRenderer) {

    ImGui::Text("Models");

    if (ImGui::ListBoxHeader("Loaded##Models", 5)) {
        for (auto modelIndex = 0; modelIndex < modelInfos.size(); modelIndex++) {
            const ModelInfo &modelInfo = modelInfos[modelIndex];
            if (!modelInfo.loaded.has_value())
                continue;

            bool isSelected = selectedModel == modelIndex;
            if (ImGui::Selectable(modelInfo.name.c_str(), isSelected)) {
                selectedModel = modelIndex;
            }
            if (isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::ListBoxFooter();
    }

    if (ImGui::ListBoxHeader("Unloaded##Models", 5)) {
        for (auto modelIndex = 0; modelIndex < modelInfos.size(); modelIndex++) {
            const ModelInfo &modelInfo = modelInfos[modelIndex];
            if (modelInfo.loaded.has_value())
                continue;

            bool isSelected = selectedModel == modelIndex;
            if (ImGui::Selectable(modelInfo.name.c_str(), isSelected)) {
                selectedModel = modelIndex;
            }
            if (isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::ListBoxFooter();
    }

    PUSH_DISABLE(selectedModel == -1 || modelInfos[selectedModel].loaded.has_value())
    if (ImGui::Button("Load##Models")) {
        modelInfos[selectedModel].loaded = masterRenderer.loadModel(modelInfos[selectedModel].location);
    }
    POP_DISABLE()

    ImGui::SameLine();

    PUSH_DISABLE(selectedModel == -1 || !modelInfos[selectedModel].loaded.has_value() || modelInfos[selectedModel].essential.has_value() || modelInfos[selectedModel].usages > 0)

    if (ImGui::Button("Unload##Models")) {
        masterRenderer.unloadModel(modelInfos[selectedModel].loaded.value());
        modelInfos[selectedModel].loaded = {};
    }

    POP_DISABLE()

    if (selectedModel != -1 && (modelInfos[selectedModel].essential.has_value() || modelInfos[selectedModel].usages > 0)) {
        ImGui::SameLine();
        if (modelInfos[selectedModel].essential.has_value()) {
            std::string reason = modelInfos[selectedModel].essential.value();
            ImGui::Text("Essential: %s", reason.c_str());
        } else {
            ImGui::Text("Model In Use");
        }
    }

    ImGui::NewLine();

    ImGui::Text("Textures");

    if (ImGui::ListBoxHeader("Loaded##Textures", 5)) {
        for (auto textureIndex = 0; textureIndex < textureInfos.size(); textureIndex++) {
            const TextureInfo &textureInfo = textureInfos[textureIndex];
            if (!textureInfo.loaded.has_value())
                continue;

            bool isSelected = selectedTexture == textureIndex;
            if (ImGui::Selectable(textureInfo.name.c_str(), isSelected)) {
                selectedTexture = textureIndex;
            }
            if (isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::ListBoxFooter();
    }

    if (ImGui::ListBoxHeader("Unloaded##Textures", 5)) {
        for (auto textureIndex = 0; textureIndex < textureInfos.size(); textureIndex++) {
            const TextureInfo &textureInfo = textureInfos[textureIndex];
            if (textureInfo.loaded.has_value())
                continue;

            bool isSelected = selectedTexture == textureIndex;
            if (ImGui::Selectable(textureInfo.name.c_str(), isSelected)) {
                selectedTexture = textureIndex;
            }
            if (isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::ListBoxFooter();
    }

    PUSH_DISABLE(selectedTexture == -1 || textureInfos[selectedTexture].loaded.has_value())
    if (ImGui::Button("Load##Textures")) {
        textureInfos[selectedTexture].loaded = masterRenderer.loadTexture(textureInfos[selectedTexture].location);
    }
    POP_DISABLE()

    ImGui::SameLine();

    PUSH_DISABLE(selectedTexture == -1 || !textureInfos[selectedTexture].loaded.has_value() || textureInfos[selectedTexture].essential.has_value() || textureInfos[selectedTexture].usages > 0)

    if (ImGui::Button("Unload##Textures")) {
        masterRenderer.unloadTexture(textureInfos[selectedTexture].loaded.value());
        textureInfos[selectedTexture].loaded = {};
    }

    POP_DISABLE()

    if (selectedTexture != -1 && (textureInfos[selectedTexture].essential.has_value() || textureInfos[selectedTexture].usages > 0)) {
        ImGui::SameLine();
        if (textureInfos[selectedTexture].essential.has_value()) {
            std::string reason = textureInfos[selectedTexture].essential.value();
            ImGui::Text("Essential: %s", reason.c_str());
        } else {
            ImGui::Text("Texture In Use");
        }
    }
}

SceneManager::SceneObject SceneManager::newSceneObject(MasterRenderer &masterRenderer, std::string name, size_t modelIndex, size_t textureIndex) {
    if (!modelInfos[modelIndex].loaded.has_value()) {
        std::cerr << "Can not add scene object with unloaded model" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (!textureInfos[textureIndex].loaded.has_value()) {
        std::cerr << "Can not add scene object with unloaded texture" << std::endl;
        exit(EXIT_FAILURE);
    }

    SceneObject sceneObject{};
    sceneObject.name = std::move(name);
    sceneObject.modelIndex = modelIndex;
    sceneObject.textureIndex = textureIndex;
    sceneObject.entityID = masterRenderer.addEntity(modelInfos[modelIndex].loaded.value(), textureInfos[textureIndex].loaded.value(), EntityData::identity());
    sceneObject.updateData(masterRenderer);

    modelInfos[modelIndex].usages++;
    textureInfos[textureIndex].usages++;
    sceneObject.updateData(masterRenderer);

    return sceneObject;
}

SceneManager::SceneLight SceneManager::newSceneLight(MasterRenderer &masterRenderer, std::string name) {
    SceneLight sceneLight{};
    sceneLight.name = std::move(name);
    sceneLight.lightID = masterRenderer.addLight(sceneLight.makeVariant());

    sceneLight.entityID = masterRenderer.addFlatEntity(modelInfos[LIGHT_MODEL].loaded.value(), FlatEntityData::identity());
    sceneLight.updateData(masterRenderer);

    return sceneLight;
}
