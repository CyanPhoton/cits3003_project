#include "GizmoManager.h"

#include <glm/gtx/transform.hpp>

#include "MeshGenerator.h"

const float GizmoManager::moveArrowLength = 0.5f;
const float GizmoManager::moveArrowHeadLengthRatio = 0.15f;
const float GizmoManager::moveArrowCylinderRadius = 0.01f;
const float GizmoManager::moveArrowHeadRadiusRatio = 2.0f;
const float GizmoManager::moveArrowCentreBoxScale = 2.0f;
const float GizmoManager::moveArrowPlaneScale = 0.2f;
const float GizmoManager::moveArrowOverallScale = 0.4f;

const uint GizmoManager::sides = 128;

const float GizmoManager::gizmoAlpha = 0.5f;

MoveGizmo GizmoManager::createMoveGizmo(MasterRenderer &masterRenderer) {
    return MoveGizmo(masterRenderer);
}

ScaleGizmo GizmoManager::createScaleGizmo(MasterRenderer &masterRenderer) {
    return ScaleGizmo(masterRenderer);
}

RotateGizmo GizmoManager::createRotateGizmo(MasterRenderer &masterRenderer) {
    return RotateGizmo(masterRenderer);
}

YawPitchGizmo GizmoManager::createYawPitchGizmo(MasterRenderer &masterRenderer) {
    return YawPitchGizmo(masterRenderer);
}

void GizmoManager::update(MasterRenderer &masterRenderer) {
    masterRenderer.setGizmoEntities({});
}

ModelID GizmoManager::generateAxisArrow(MasterRenderer &masterRenderer, glm::vec3 axis) {
    const float cylinderLength = moveArrowLength * (1.0f - moveArrowHeadLengthRatio);
    const float headLength = moveArrowLength * moveArrowHeadLengthRatio;
    const float headRadius = moveArrowCylinderRadius * moveArrowHeadRadiusRatio;

    Mesh axisCylinder = MeshGenerator::genCylinderNoNormNoTex((cylinderLength / 2.0f) * axis, cylinderLength * axis, moveArrowCylinderRadius, sides);
    Mesh axisArrowCone = MeshGenerator::genConeNoNormNoTex(cylinderLength * axis, headLength * axis, headRadius, sides);

    Mesh axisArrowMesh = MeshGenerator::mergeMeshs({axisArrowCone, axisCylinder});

    return masterRenderer.loadModel(axisArrowMesh.modelData, axisArrowMesh.modelMetaData);
}

ModelID GizmoManager::generateAxisScaleArm(MasterRenderer &masterRenderer, glm::vec3 axis, glm::vec3 tangent) {
    const float armLength = moveArrowLength * (1.0f - moveArrowHeadLengthRatio);
    const float armHeadSize = moveArrowCylinderRadius * moveArrowHeadRadiusRatio * 2.0f;
    const float armSize = moveArrowCylinderRadius * 2.0f;

    glm::vec3 cotangent = glm::cross(axis, tangent);

    Mesh axisArm = MeshGenerator::genParallelepipedNoNormNoTex(axis * armLength / 2.0f, axis * armLength, tangent * armSize, cotangent * armSize);
    Mesh axisArmHead = MeshGenerator::genParallelepipedNoNormNoTex(axis * (armLength + armHeadSize / 2.0f), axis * armHeadSize, tangent * armHeadSize, cotangent * armHeadSize);

    Mesh armMesh = MeshGenerator::mergeMeshs({axisArm, axisArmHead});

    return masterRenderer.loadModel(armMesh.modelData, armMesh.modelMetaData);
}

ModelID GizmoManager::generateAxisRing(MasterRenderer &masterRenderer, glm::vec3 axis, float widthScale) {
    const float radius = moveArrowLength;
    const float width = moveArrowCylinderRadius * moveArrowHeadRadiusRatio * 2.0f * widthScale;

//    Mesh ringMesh = MeshGenerator::genOpenCylinderNoNormNoTex(glm::vec3{0.0f}, axis * width, radius, true, sides);
    Mesh ringMesh = MeshGenerator::genSquareToroidNoNormNoTex(glm::vec3{0.0f}, axis * width, radius, width, sides);

    return masterRenderer.loadModel(ringMesh.modelData, ringMesh.modelMetaData);
}

glm::vec3 GizmoManager::getPointerDirViewSpace(MasterRenderer &masterRenderer, const Window &window) {
    glm::vec2 pointerNDC = window.getMousePosNDC();

    glm::vec4 dirNDC = {pointerNDC.x, pointerNDC.y, -masterRenderer.getCurrentProjectionMatrix()[2][2], 1.0f};

    // Manually construct inverse from known form of perspective, since should be faster than general case
    glm::mat4 inverseProjection = glm::mat4{1.0f};
    inverseProjection[0][0] = 1.0f / masterRenderer.getCurrentProjectionMatrix()[0][0];
    inverseProjection[1][1] = 1.0f / masterRenderer.getCurrentProjectionMatrix()[1][1];
    inverseProjection[2][2] = 0.0f;
    inverseProjection[3][2] = -1.0f;
    inverseProjection[2][3] = 1.0f / masterRenderer.getCurrentProjectionMatrix()[3][2];
    inverseProjection[3][3] = masterRenderer.getCurrentProjectionMatrix()[2][2] / masterRenderer.getCurrentProjectionMatrix()[3][2];;

    glm::vec4 dirViewSpace = inverseProjection * dirNDC;

    return glm::normalize(glm::vec3(dirViewSpace));
}

glm::vec3 GizmoManager::getPointerDirWorldSpace(MasterRenderer &masterRenderer, const Window &window) {
    glm::vec3 pointerDirViewSpace = getPointerDirViewSpace(masterRenderer, window);

    glm::vec4 pointerDirWorldSpace = glm::inverse(masterRenderer.getCurrentViewMatrix()) * glm::vec4{pointerDirViewSpace, 0.0f};

    return glm::normalize(glm::vec3{pointerDirWorldSpace});
}

float GizmoManager::intersectionOfLineAndPlane(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 planePoint, glm::vec3 planeNormal) {
    float denominator = glm::dot(lineDir, planeNormal);

    return glm::dot(planePoint - linePoint, planeNormal) / denominator;
}

std::pair<GizmoManager::IntersectionType, GizmoManager::IntersectionVariant> GizmoManager::intersectionOfLineAndSquare(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 planePoint, glm::vec3 planeNormal, glm::vec3 tangent) {
    planeNormal = glm::normalize(planeNormal);
    tangent = glm::normalize(tangent);
    glm::vec cotangent = glm::cross(planeNormal, tangent);

    float lambda = glm::dot(planePoint - linePoint, planeNormal) / glm::dot(lineDir, planeNormal);

    if (std::isnan(lambda)) {
        return {IntersectionType::None, NoneIntersection{}};
    }

    glm::vec3 intersectionPoint = linePoint + lambda * lineDir;

    float tangentCoefficient = glm::dot(intersectionPoint - planePoint, tangent);
    float cotangentCoefficient = glm::dot(intersectionPoint - planePoint, cotangent);

    float distance = std::max(std::fabs(tangentCoefficient), std::fabs(cotangentCoefficient));

    return {IntersectionType::Single, SingleIntersection{
            lambda,
            distance // Distance here is the max of the distance of the point from the center, in the two axis defined by the square, and the max of those, so for a square of size L this will range [0, L/2]
    }};
}

std::pair<GizmoManager::IntersectionType, GizmoManager::IntersectionVariant> GizmoManager::intersectionOfLineAndCylinder(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 cylinderPoint, glm::vec3 cylinderDir, float radius) {
    float Lambda = glm::dot(lineDir, cylinderDir);
    glm::vec3 d = linePoint - cylinderPoint;

    float BetaDotChi = glm::dot(lineDir - Lambda * cylinderDir, d);
    float BetaMagSquared = glm::dot(d, d) + powf(glm::dot(d, cylinderDir), 2.0f) * (glm::dot(cylinderDir, cylinderDir) - 2.0f);
    float ChiMagSquared = glm::dot(lineDir, lineDir) + Lambda * Lambda * (glm::dot(cylinderDir, cylinderDir) - 2.0f);

    float sqrtPart = sqrtf(BetaDotChi * BetaDotChi - ChiMagSquared * (BetaMagSquared - radius * radius));

    if (std::isnan(sqrtPart)) {
        return {IntersectionType::None, NoneIntersection{}};
    }

    float min_lambda = (-BetaDotChi - sqrtPart) / ChiMagSquared;
    float minMu = glm::dot(d, cylinderDir) + min_lambda * Lambda;

    if (sqrtPart == 0.0f) {
        return {
                IntersectionType::Single,
                SingleIntersection{
                        min_lambda,
                        minMu
                }
        };
    }

    float max_lambda = (-BetaDotChi + sqrtPart) / ChiMagSquared;
    float maxMu = glm::dot(d, cylinderDir) + max_lambda * Lambda;

    return {
            IntersectionType::Double,
            DoubleIntersection{
                    SingleIntersection{
                            min_lambda,
                            minMu
                    },
                    SingleIntersection{
                            max_lambda,
                            maxMu
                    }
            }
    };
}

GizmoManager::ClosestApproach GizmoManager::closestApproachLineAndLine(glm::vec3 line1Point, glm::vec3 line1Dir, glm::vec3 line2Point, glm::vec3 line2Dir) {
    float Lambda = glm::dot(line1Dir, line2Dir);
    glm::vec3 d = line1Point - line2Point;

    float BetaDotChi = glm::dot(line1Dir - Lambda * line2Dir, d);
    float BetaMagSquared = glm::dot(d, d) + powf(glm::dot(d, line2Dir), 2.0f) * (glm::dot(line2Dir, line2Dir) - 2.0f);
    float ChiMagSquared = glm::dot(line1Dir, line1Dir) + Lambda * Lambda * (glm::dot(line2Dir, line2Dir) - 2.0f);

    float lambda = -BetaDotChi / ChiMagSquared;
    float mu = glm::dot(d, line2Dir) + Lambda * lambda;

    float radius = sqrtf(BetaMagSquared + lambda * lambda * ChiMagSquared + 2 * lambda * BetaDotChi);

    return ClosestApproach{
            lambda,
            mu,
            radius
    };
}

std::pair<size_t, float> GizmoManager::closestIntersection(const std::vector<std::pair<IntersectionType, IntersectionVariant>> &intersections, const std::vector<std::pair<float, float>> &objectParamRanges) {
    bool closestFound = false;
    float closest = -std::numeric_limits<float>::infinity();
    size_t closestIndex = 0;

    for (size_t i = 0; i < intersections.size(); i++) {
        const auto &intersection = intersections[i];

        switch (intersection.first) {
            case None:
                break;
            case Single: {
                float dist = std::get<SingleIntersection>(intersection.second).lineParameter;
                float objParam = std::get<SingleIntersection>(intersection.second).objectParameter;
                if (dist > 0.0f && (!closestFound || dist < closest)) {
                    if (i >= objectParamRanges.size() || (objectParamRanges[i].first <= objParam && objParam <= objectParamRanges[i].second)) {
                        closestFound = true;
                        closest = dist;
                        closestIndex = i;
                    }
                }
                break;
            }
            case Double: {
                float dist1 = std::get<DoubleIntersection>(intersection.second).minLinePair.lineParameter;
                float objParam1 = std::get<DoubleIntersection>(intersection.second).minLinePair.objectParameter;
                if (dist1 > 0.0f && (!closestFound || dist1 < closest)) {
                    if (i >= objectParamRanges.size() || (objectParamRanges[i].first <= objParam1 && objParam1 <= objectParamRanges[i].second)) {
                        closestFound = true;
                        closest = dist1;
                        closestIndex = i;
                    }
                }
                float dist2 = std::get<DoubleIntersection>(intersection.second).maxLinePair.lineParameter;
                float objParam2 = std::get<DoubleIntersection>(intersection.second).maxLinePair.objectParameter;
                if (dist2 > 0.0f && (!closestFound || dist2 < closest)) {
                    if (i >= objectParamRanges.size() || (objectParamRanges[i].first <= objParam2 && objParam2 <= objectParamRanges[i].second)) {
                        closestFound = true;
                        closest = dist2;
                        closestIndex = i;
                    }
                }
                break;
            }
        }
    }

    return std::pair<size_t, float>(closestIndex, closest);
}

std::pair<GizmoManager::IntersectionType, GizmoManager::IntersectionVariant> GizmoManager::intersectionOfLineAndSphere(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 sphereCenter, float radius) {
    glm::vec3 d = linePoint - sphereCenter;
    float dDotV = glm::dot(d, lineDir);
    float dSquared = glm::dot(d, d);
    float vSquared = glm::dot(lineDir, lineDir);

    float discriminant = dDotV * dDotV - vSquared * (dSquared - radius * radius);
    if (discriminant < 0) {
        return {IntersectionType::None, NoneIntersection{}};
    }

    if (discriminant == 0) {
        return {IntersectionType::Single, SingleIntersection{
                -dDotV / vSquared,
                radius
        }};
    }

    float sqrtDiscriminant = std::sqrt(discriminant);

    return {IntersectionType::Double, DoubleIntersection{
            SingleIntersection{
                    (-dDotV + sqrtDiscriminant) / vSquared,
                    radius
            },
            SingleIntersection{
                    (-dDotV - sqrtDiscriminant) / vSquared,
                    radius
            }
    }};
}

std::pair<GizmoManager::IntersectionType, GizmoManager::IntersectionVariant> GizmoManager::intersectionOfLineAndCircle(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 circleCenter, glm::vec3 norm) {
    if (glm::dot(lineDir, norm) == 0) {
        return {IntersectionType::None, NoneIntersection{}};
    }

    float planeIntersection = GizmoManager::intersectionOfLineAndPlane(linePoint, lineDir, circleCenter, norm);

    glm::vec3 intersectionPoint = linePoint + lineDir * planeIntersection;

    glm::vec3 offset = intersectionPoint - circleCenter;
    float radius = glm::length(offset);

    return {IntersectionType::Single, SingleIntersection{
            planeIntersection,
            radius
    }};
}

float GizmoManager::closestIntersectionOfLineAndSquareToroid(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 centre, glm::vec3 length, float averageRadius, float thickness) {

    float innerRadius = averageRadius - 0.5f * std::abs(thickness);
    float outerRadius = averageRadius + 0.5f * std::abs(thickness);

    float len = glm::length(length);
    glm::vec3 normLen = glm::normalize(length);

    auto outerCylinderIntersection = intersectionOfLineAndCylinder(linePoint, lineDir, centre, normLen, outerRadius);
    auto innerCylinderIntersection = intersectionOfLineAndCylinder(linePoint, lineDir, centre, normLen, innerRadius);

    auto side1Intersection = intersectionOfLineAndCircle(linePoint, lineDir, centre + 0.5f * length, normLen);

    auto side2Intersection = intersectionOfLineAndCircle(linePoint, lineDir, centre - 0.5f * length, normLen);

    return closestIntersection(
            {outerCylinderIntersection, innerCylinderIntersection, side1Intersection, side2Intersection},
            {{-len / 2.0f, len / 2.0f},
             {-len / 2.0f, len / 2.0f},
             {innerRadius, outerRadius},
             {innerRadius, outerRadius}}).second;
}

MoveGizmo::MoveGizmo(MasterRenderer &masterRenderer) {
    ModelID xAxisArrow = GizmoManager::generateAxisArrow(masterRenderer, {1.0f, 0.0f, 0.0f});
    ModelID yAxisArrow = GizmoManager::generateAxisArrow(masterRenderer, {0.0f, 1.0f, 0.0f});
    ModelID zAxisArrow = GizmoManager::generateAxisArrow(masterRenderer, {0.0f, 0.0f, 1.0f});

    float boxSize = GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowCentreBoxScale * 2.0f; // x2 from diameter = 2 x radius
    Mesh centreBoxMesh = MeshGenerator::genParallelepipedNoNormNoTex(glm::vec3{0.0f}, glm::vec3{boxSize, 0.0f, 0.0f}, glm::vec3{0.0f, boxSize, 0.0f}, glm::vec3{0.0f, 0.0f, boxSize});
    ModelID centreBox = masterRenderer.loadModel(centreBoxMesh.modelData, centreBoxMesh.modelMetaData);

    float planeOffset = GizmoManager::moveArrowLength / 2.0f;
    float planeSize = GizmoManager::moveArrowLength * GizmoManager::moveArrowPlaneScale;
    Mesh yzPlaneQuadMesh = MeshGenerator::genParallelogramNoNormNoTex({0, planeOffset, planeOffset}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f}, planeSize, true);
    Mesh xzPlaneQuadMesh = MeshGenerator::genParallelogramNoNormNoTex({planeOffset, 0, planeOffset}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, planeSize, true);
    Mesh xyPlaneQuadMesh = MeshGenerator::genParallelogramNoNormNoTex({planeOffset, planeOffset, 0}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}, planeSize, true);

    ModelID yzPlane = masterRenderer.loadModel(yzPlaneQuadMesh.modelData, yzPlaneQuadMesh.modelMetaData);
    ModelID xzPlane = masterRenderer.loadModel(xzPlaneQuadMesh.modelData, xzPlaneQuadMesh.modelMetaData);
    ModelID xyPlane = masterRenderer.loadModel(xyPlaneQuadMesh.modelData, xyPlaneQuadMesh.modelMetaData);

    xAxisEntity = {
            xAxisArrow,
            glm::mat4{1.0f},
            {1.0f, 0.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    yAxisEntity = {
            yAxisArrow,
            glm::mat4{1.0f},
            {0.0f, 1.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    zAxisEntity = {
            zAxisArrow,
            glm::mat4{1.0f},
            {0.0f, 0.0f, 1.0f, GizmoManager::gizmoAlpha}
    };
    centreEntity = {
            centreBox,
            glm::mat4{1.0f},
            glm::vec4{1.0f}
    };
    yzPlaneEntity = {
            yzPlane,
            glm::mat4{1.0f},
            glm::vec4{1.0f, 0.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    xzPlaneEntity = {
            xzPlane,
            glm::mat4{1.0f},
            glm::vec4{0.0f, 1.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    xyPlaneEntity = {
            xyPlane,
            glm::mat4{1.0f},
            glm::vec4{0.0f, 0.0f, 1.0f, GizmoManager::gizmoAlpha}
    };
}

bool MoveGizmo::update(MasterRenderer &masterRenderer, const Window &window, glm::vec3 &position, float overallScale) {
    glm::vec linePoint = masterRenderer.getCurrentCameraPos();
    glm::vec lineDir = GizmoManager::getPointerDirWorldSpace(masterRenderer, window);

    float initScale;
    {
        glm::vec4 viewPos = masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f};
        initScale = -viewPos.z * GizmoManager::moveArrowOverallScale * overallScale;
    }

    float arrowRadius = GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowHeadRadiusRatio * initScale;
    float arrowLength = GizmoManager::moveArrowLength * initScale;

    float planeOffset = (GizmoManager::moveArrowLength / 2.0f) * initScale;
    float planeSize = (GizmoManager::moveArrowLength * GizmoManager::moveArrowPlaneScale) * initScale;

    bool mouseIsPressed = window.isMousePressed(GLFW_MOUSE_BUTTON_LEFT);

    bool justEnabled = false;
    if (mouseWasPressed && !mouseIsPressed) {
        selectionMode = None;
        mouseWasPressed = false;
    } else if (!mouseWasPressed && mouseIsPressed) {
        mouseWasPressed = true;
        justEnabled = true;

        auto xIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{1.0f, 0.0f, 0.0f}, arrowRadius);
        auto yIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{0.0f, 1.0f, 0.0f}, arrowRadius);
        auto zIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{0.0f, 0.0f, 1.0f}, arrowRadius);

        auto yzIntersection = GizmoManager::intersectionOfLineAndSquare(linePoint, lineDir, position + glm::vec3{0.0f, planeOffset, planeOffset}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f});
        auto xzIntersection = GizmoManager::intersectionOfLineAndSquare(linePoint, lineDir, position + glm::vec3{planeOffset, 0.0f, planeOffset}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f});
        auto xyIntersection = GizmoManager::intersectionOfLineAndSquare(linePoint, lineDir, position + glm::vec3{planeOffset, planeOffset, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 0.0f});

        auto closest = GizmoManager::closestIntersection(
                {xIntersection, yIntersection, zIntersection, yzIntersection, xzIntersection, xyIntersection},
                {
                        {0, arrowLength},
                        {0, arrowLength},
                        {0, arrowLength},
                        {0, planeSize / 2.0f},
                        {0, planeSize / 2.0f},
                        {0, planeSize / 2.0f},
                });

        if (closest.second >= 0.0f) {
            switch (closest.first) {
                case 0:
                    selectionMode = SelectionMode::xAxis;
                    break;
                case 1:
                    selectionMode = SelectionMode::yAxis;
                    break;
                case 2:
                    selectionMode = SelectionMode::zAxis;
                    break;
                case 3:
                    selectionMode = SelectionMode::yzPlane;
                    break;
                case 4:
                    selectionMode = SelectionMode::xzPlane;
                    break;
                case 5:
                    selectionMode = SelectionMode::xyPlane;
                    break;
            }
        } else {
            selectionMode = SelectionMode::viewPlane;
        }
    }

    if (!mouseIsPressed) {
        xAxisEntity.colour.a = GizmoManager::gizmoAlpha;
        yAxisEntity.colour.a = GizmoManager::gizmoAlpha;
        zAxisEntity.colour.a = GizmoManager::gizmoAlpha;

        yzPlaneEntity.colour.a = GizmoManager::gizmoAlpha;
        xzPlaneEntity.colour.a = GizmoManager::gizmoAlpha;
        xyPlaneEntity.colour.a = GizmoManager::gizmoAlpha;

        auto xIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{1.0f, 0.0f, 0.0f}, arrowRadius);
        auto yIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{0.0f, 1.0f, 0.0f}, arrowRadius);
        auto zIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{0.0f, 0.0f, 1.0f}, arrowRadius);

        auto yzIntersection = GizmoManager::intersectionOfLineAndSquare(linePoint, lineDir, position + glm::vec3{0.0f, planeOffset, planeOffset}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f});
        auto xzIntersection = GizmoManager::intersectionOfLineAndSquare(linePoint, lineDir, position + glm::vec3{planeOffset, 0.0f, planeOffset}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f});
        auto xyIntersection = GizmoManager::intersectionOfLineAndSquare(linePoint, lineDir, position + glm::vec3{planeOffset, planeOffset, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 0.0f});

        auto closest = GizmoManager::closestIntersection(
                {xIntersection, yIntersection, zIntersection, yzIntersection, xzIntersection, xyIntersection},
                {
                        {0, arrowLength},
                        {0, arrowLength},
                        {0, arrowLength},
                        {0, planeSize / 2.0f},
                        {0, planeSize / 2.0f},
                        {0, planeSize / 2.0f},
                });

        if (closest.second >= 0.0f) {
            switch (closest.first) {
                case 0:
                    xAxisEntity.colour.a = 1.0f;
                    break;
                case 1:
                    yAxisEntity.colour.a = 1.0f;
                    break;
                case 2:
                    zAxisEntity.colour.a = 1.0f;
                    break;
                case 3:
                    yzPlaneEntity.colour.a = 1.0f;
                    break;
                case 4:
                    xzPlaneEntity.colour.a = 1.0f;
                    break;
                case 5:
                    xyPlaneEntity.colour.a = 1.0f;
                    break;
            }
        }
    }

    switch (selectionMode) {
        case None:
            break;
        case xAxis: {
            auto closestApproach = GizmoManager::closestApproachLineAndLine(linePoint, lineDir, glm::vec3{0, position.y, position.z}, glm::vec3{1.0f, 0.0f, 0.0f});
            if (justEnabled) {
                initialOffset.x = (closestApproach.line2Parameter - position.x) / initScale;
            }
            position.x = closestApproach.line2Parameter - initialOffset.x * initScale;
            break;
        }
        case yAxis: {
            auto closestApproach = GizmoManager::closestApproachLineAndLine(linePoint, lineDir, glm::vec3{position.x, 0, position.z}, glm::vec3{0.0f, 1.0f, 0.0f});
            if (justEnabled) {
                initialOffset.x = (closestApproach.line2Parameter - position.y) / initScale;
            }
            position.y = closestApproach.line2Parameter - initialOffset.x * initScale;
            break;
        }
        case zAxis: {
            auto closestApproach = GizmoManager::closestApproachLineAndLine(linePoint, lineDir, glm::vec3{position.x, position.y, 0}, glm::vec3{0.0f, 0.0f, 1.0f});
            if (justEnabled) {
                initialOffset.x = (closestApproach.line2Parameter - position.z) / initScale;
            }
            position.z = closestApproach.line2Parameter - initialOffset.x * initScale;
            break;
        }
        case yzPlane: {
            if (std::abs(glm::dot(glm::normalize(lineDir), glm::vec3{1.0f, 0.0f, 0.0f})) < 1.0e-2f) {
                break;
            }

            float yzIntersection = GizmoManager::intersectionOfLineAndPlane(linePoint, lineDir, position, {1.0f, 0.0f, 0.0f});

            if (yzIntersection < 0) {
                break;
            }

            glm::vec3 intersectionPoint = linePoint + yzIntersection * lineDir;

            if (justEnabled) {
                initialOffset.x = (intersectionPoint.y - position.y) / initScale;
                initialOffset.y = (intersectionPoint.z - position.z) / initScale;
            }

            position.y = intersectionPoint.y - initialOffset.x * initScale;
            position.z = intersectionPoint.z - initialOffset.y * initScale;

            break;
        }
        case xzPlane: {
            if (std::abs(glm::dot(glm::normalize(lineDir), glm::vec3{0.0f, 1.0f, 0.0f})) < 1.0e-2f) {
                break;
            }

            float xzIntersection = GizmoManager::intersectionOfLineAndPlane(linePoint, lineDir, position, {0.0f, 1.0f, 0.0f});

            if (xzIntersection < 0) {
                break;
            }

            glm::vec3 intersectionPoint = linePoint + xzIntersection * lineDir;

            if (justEnabled) {
                initialOffset.x = (intersectionPoint.x - position.x) / initScale;
                initialOffset.y = (intersectionPoint.z - position.z) / initScale;
            }

            position.x = intersectionPoint.x - initialOffset.x * initScale;
            position.z = intersectionPoint.z - initialOffset.y * initScale;

            break;
        }
        case xyPlane: {
            if (std::abs(glm::dot(glm::normalize(lineDir), glm::vec3{0.0f, 0.0f, 1.0f})) < 1.0e-2f) {
                break;
            }

            float xyIntersection = GizmoManager::intersectionOfLineAndPlane(linePoint, lineDir, position, {0.0f, 0.0f, 1.0f});

            if (xyIntersection < 0) {
                break;
            }

            glm::vec3 intersectionPoint = linePoint + xyIntersection * lineDir;

            if (justEnabled) {
                initialOffset.x = (intersectionPoint.x - position.x) / initScale;
                initialOffset.y = (intersectionPoint.y - position.y) / initScale;
            }

            position.x = intersectionPoint.x - initialOffset.x * initScale;
            position.y = intersectionPoint.y - initialOffset.y * initScale;

            break;
        }
        case viewPlane: {
            glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};

            glm::vec3 linePointViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{linePoint, 1.0f}};
            glm::vec3 lineDirViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{lineDir, 0.0f}};

            glm::vec3 positionViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f}};

            float viewPlaneIntersectionVS = GizmoManager::intersectionOfLineAndPlane(linePointViewS, lineDirViewS, positionViewS, viewDirection);

            glm::vec3 intersectionPointVS = linePointViewS + viewPlaneIntersectionVS * lineDirViewS;

            if (justEnabled) {
                initialOffset = (intersectionPointVS - positionViewS);
            }

            position = glm::vec3{glm::inverse(masterRenderer.getCurrentViewMatrix()) * glm::vec4{intersectionPointVS - initialOffset, 1.0f}};

            break;
        }
    }

    glm::vec4 viewPos = masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f};

    glm::mat4 modelMatrix = glm::translate(position) * glm::scale(glm::vec3{-viewPos.z * GizmoManager::moveArrowOverallScale * overallScale});

    xAxisEntity.modelMatrix = modelMatrix;
    yAxisEntity.modelMatrix = modelMatrix;
    zAxisEntity.modelMatrix = modelMatrix;
    centreEntity.modelMatrix = modelMatrix;
    yzPlaneEntity.modelMatrix = modelMatrix;
    xzPlaneEntity.modelMatrix = modelMatrix;
    xyPlaneEntity.modelMatrix = modelMatrix;

    masterRenderer.setGizmoEntities({xAxisEntity, yAxisEntity, zAxisEntity, centreEntity, yzPlaneEntity, xzPlaneEntity, xyPlaneEntity});
    return selectionMode != SelectionMode::None;
}

ScaleGizmo::ScaleGizmo(MasterRenderer &masterRenderer) {
    ModelID xAxisScale = GizmoManager::generateAxisScaleArm(masterRenderer, {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f});
    ModelID yAxisScale = GizmoManager::generateAxisScaleArm(masterRenderer, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f});
    ModelID zAxisScale = GizmoManager::generateAxisScaleArm(masterRenderer, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 0.0f});

    float boxSize = GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowCentreBoxScale * 2.0f; // x2 from diameter = 2 x radius
    Mesh centreBoxMesh = MeshGenerator::genParallelepipedNoNormNoTex(glm::vec3{0.0f}, glm::vec3{boxSize, 0.0f, 0.0f}, glm::vec3{0.0f, boxSize, 0.0f}, glm::vec3{0.0f, 0.0f, boxSize});
    ModelID centreBox = masterRenderer.loadModel(centreBoxMesh.modelData, centreBoxMesh.modelMetaData);

    xAxisEntity = {
            xAxisScale,
            glm::mat4{1.0f},
            {1.0f, 0.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    yAxisEntity = {
            yAxisScale,
            glm::mat4{1.0f},
            {0.0f, 1.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    zAxisEntity = {
            zAxisScale,
            glm::mat4{1.0f},
            {0.0f, 0.0f, 1.0f, GizmoManager::gizmoAlpha}
    };
    centreEntity = {
            centreBox,
            glm::mat4{1.0f},
            glm::vec4{1.0f}
    };
}

bool ScaleGizmo::update(MasterRenderer &masterRenderer, const Window &window, const glm::vec3 &position, glm::vec3 &scale, float overallScale) {
    glm::vec linePoint = masterRenderer.getCurrentCameraPos();
    glm::vec lineDir = GizmoManager::getPointerDirWorldSpace(masterRenderer, window);

    float initScale;
    {
        glm::vec4 viewPos = masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f};
        initScale = -viewPos.z * GizmoManager::moveArrowOverallScale * overallScale;
    }

    float arrowRadius = GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowHeadRadiusRatio * initScale;
    float arrowLength = GizmoManager::moveArrowLength * initScale;

    bool mouseIsPressed = window.isMousePressed(GLFW_MOUSE_BUTTON_LEFT);

    bool justEnabled = false;
    if (mouseWasPressed && !mouseIsPressed) {
        selectionMode = None;
        mouseWasPressed = false;
    } else if (!mouseWasPressed && mouseIsPressed) {
        mouseWasPressed = true;
        justEnabled = true;

        auto xIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{1.0f, 0.0f, 0.0f}, arrowRadius);
        auto yIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{0.0f, 1.0f, 0.0f}, arrowRadius);
        auto zIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{0.0f, 0.0f, 1.0f}, arrowRadius);

        auto closest = GizmoManager::closestIntersection(
                {xIntersection, yIntersection, zIntersection},
                {
                        {0, arrowLength},
                        {0, arrowLength},
                        {0, arrowLength},
                });

        if (closest.second >= 0.0f) {
            switch (closest.first) {
                case 0:
                    selectionMode = SelectionMode::xAxis;
                    break;
                case 1:
                    selectionMode = SelectionMode::yAxis;
                    break;
                case 2:
                    selectionMode = SelectionMode::zAxis;
                    break;
            }
        } else {
            selectionMode = SelectionMode::uniform;
        }
    }

    if (!mouseIsPressed) {
        xAxisEntity.colour.a = GizmoManager::gizmoAlpha;
        yAxisEntity.colour.a = GizmoManager::gizmoAlpha;
        zAxisEntity.colour.a = GizmoManager::gizmoAlpha;

        auto xIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{1.0f, 0.0f, 0.0f}, arrowRadius);
        auto yIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{0.0f, 1.0f, 0.0f}, arrowRadius);
        auto zIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, glm::vec3{0.0f, 0.0f, 1.0f}, arrowRadius);

        auto closest = GizmoManager::closestIntersection(
                {xIntersection, yIntersection, zIntersection},
                {
                        {0, arrowLength},
                        {0, arrowLength},
                        {0, arrowLength},
                });

        if (closest.second >= 0.0f) {
            switch (closest.first) {
                case 0:
                    xAxisEntity.colour.a = 1.0f;
                    break;
                case 1:
                    yAxisEntity.colour.a = 1.0f;
                    break;
                case 2:
                    zAxisEntity.colour.a = 1.0f;
                    break;
            }
        }
    }

    switch (selectionMode) {
        case xAxis: {
            auto closestApproach = GizmoManager::closestApproachLineAndLine(linePoint, lineDir, position, glm::vec3{1.0f, 0.0f, 0.0f});
            if (justEnabled) {
                initialObjScale = scale;
                initialOffset.x = closestApproach.line2Parameter;
            }
            scale.x = initialObjScale.x * (closestApproach.line2Parameter / initialOffset.x);
            break;
        }
        case yAxis: {
            auto closestApproach = GizmoManager::closestApproachLineAndLine(linePoint, lineDir, position, glm::vec3{0.0f, 1.0f, 0.0f});
            if (justEnabled) {
                initialObjScale = scale;
                initialOffset.x = closestApproach.line2Parameter;
            }
            scale.y = initialObjScale.y * (closestApproach.line2Parameter / initialOffset.x);
            break;
        }
        case zAxis: {
            auto closestApproach = GizmoManager::closestApproachLineAndLine(linePoint, lineDir, position, glm::vec3{0.0f, 0.0f, 1.0f});
            if (justEnabled) {
                initialObjScale = scale;
                initialOffset.x = closestApproach.line2Parameter;
            }
            scale.z = initialObjScale.z * (closestApproach.line2Parameter / initialOffset.x);
            break;
        }
        case uniform: {
            glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};

            glm::vec3 linePointViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{linePoint, 1.0f}};
            glm::vec3 lineDirViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{lineDir, 0.0f}};

            glm::vec3 positionViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f}};

            float viewPlaneIntersectionVS = GizmoManager::intersectionOfLineAndPlane(linePointViewS, lineDirViewS, positionViewS, viewDirection);

            glm::vec3 intersectionPointVS = linePointViewS + viewPlaneIntersectionVS * lineDirViewS;

            if (justEnabled) {
                initialOffset = (intersectionPointVS - positionViewS);
                initialObjScale = scale;
            }

            scale = initialObjScale * glm::length(intersectionPointVS - positionViewS) / glm::length(initialOffset);

            break;
        }
        case None:
            break;
    }

    glm::vec4 viewPos = masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f};

    glm::mat4 modelMatrix = glm::translate(position) * glm::scale(glm::vec3{-viewPos.z * GizmoManager::moveArrowOverallScale * overallScale});

    xAxisEntity.modelMatrix = modelMatrix;
    yAxisEntity.modelMatrix = modelMatrix;
    zAxisEntity.modelMatrix = modelMatrix;
    centreEntity.modelMatrix = modelMatrix;

    masterRenderer.setGizmoEntities({xAxisEntity, yAxisEntity, zAxisEntity, centreEntity});
    return selectionMode != SelectionMode::None;
}

RotateGizmo::RotateGizmo(MasterRenderer &masterRenderer) {
    ModelID xAxisRot = GizmoManager::generateAxisRing(masterRenderer, {1.0f, 0.0f, 0.0f}, 1.05f);
    ModelID yAxisRot = GizmoManager::generateAxisRing(masterRenderer, {0.0f, 1.0f, 0.0f});
    ModelID zAxisRot = GizmoManager::generateAxisRing(masterRenderer, {0.0f, 0.0f, 1.0f}, 0.95f);

    xAxisEntity = {
            xAxisRot,
            glm::mat4{1.0f},
            {1.0f, 0.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    yAxisEntity = {
            yAxisRot,
            glm::mat4{1.0f},
            {0.0f, 1.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    zAxisEntity = {
            zAxisRot,
            glm::mat4{1.0f},
            {0.0f, 0.0f, 1.0f, GizmoManager::gizmoAlpha}
    };
}

bool RotateGizmo::update(MasterRenderer &masterRenderer, const Window &window, const glm::vec3 &position, glm::vec3 &rotation, float overallScale) {

    glm::vec linePoint = masterRenderer.getCurrentCameraPos();
    glm::vec lineDir = GizmoManager::getPointerDirWorldSpace(masterRenderer, window);

    float initScale;
    {
        glm::vec4 viewPos = masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f};
        initScale = -viewPos.z * GizmoManager::moveArrowOverallScale * overallScale;
    }

    const float radius = (GizmoManager::moveArrowLength) *initScale;
    const float width = (GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowHeadRadiusRatio * 2.0f) * initScale;

    const glm::mat4 xRotMatrix = glm::rotate(rotation.x, glm::vec3{1.0f, 0.0f, 0.0f});
    const glm::mat4 yRotMatrix = xRotMatrix * glm::rotate(rotation.y, glm::vec3{0.0f, 1.0f, 0.0f});
    const glm::mat4 zRotMatrix = yRotMatrix * glm::rotate(rotation.z, glm::vec3{0.0f, 0.0f, 1.0f});

    const glm::vec3 rotXAxis = glm::vec3{xRotMatrix * glm::vec4{1.0f, 0.0f, 0.0f, 0.0f}};
    const glm::vec3 rotYAxis = glm::vec3{yRotMatrix * glm::vec4{0.0f, 1.0f, 0.0f, 0.0f}};
    const glm::vec3 rotZAxis = glm::vec3{zRotMatrix * glm::vec4{0.0f, 0.0f, 1.0f, 0.0f}};

    bool mouseIsPressed = window.isMousePressed(GLFW_MOUSE_BUTTON_LEFT);

    bool justEnabled = false;
    if (mouseWasPressed && !mouseIsPressed) {
        selectionMode = None;
        mouseWasPressed = false;
    } else if (!mouseWasPressed && mouseIsPressed) {
        mouseWasPressed = true;
        justEnabled = true;

//        auto xIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, rotXAxis, radius);
//        auto yIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, rotYAxis, radius);
//        auto zIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, rotZAxis, radius);

        auto xIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, rotXAxis * width, radius, width);
        auto yIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, rotYAxis * width, radius, width);
        auto zIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, rotZAxis * width, radius, width);

        std::vector<float> intersection = {xIntersection, yIntersection, zIntersection};
        size_t closest = -1;
        for (size_t i = 0; i < intersection.size(); i++) {
            if (intersection[i] >= 0.0f && (closest == -1 || intersection[i] < intersection[closest])) {
                closest = i;
            }
        }

        if (closest != -1) {
            switch (closest) {
                case 0:
                    selectionMode = SelectionMode::xAxis;
                    break;
                case 1:
                    selectionMode = SelectionMode::yAxis;
                    break;
                case 2:
                    selectionMode = SelectionMode::zAxis;
                    break;
            }
        } else {
            selectionMode = SelectionMode::screenMove;
        }
    }

    if (!mouseIsPressed) {
        xAxisEntity.colour.a = GizmoManager::gizmoAlpha;
        yAxisEntity.colour.a = GizmoManager::gizmoAlpha;
        zAxisEntity.colour.a = GizmoManager::gizmoAlpha;

        auto xIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, rotXAxis * width, radius, width);
        auto yIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, rotYAxis * width, radius, width);
        auto zIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, rotZAxis * width, radius, width);

        std::vector<float> intersection = {xIntersection, yIntersection, zIntersection};
        size_t closest = -1;
        for (size_t i = 0; i < intersection.size(); i++) {
            if (intersection[i] >= 0.0f && (closest == -1 || intersection[i] < intersection[closest])) {
                closest = i;
            }
        }

        switch (closest) {
            case 0:
                xAxisEntity.colour.a = 1.0f;
                break;
            case 1:
                yAxisEntity.colour.a = 1.0f;
                break;
            case 2:
                zAxisEntity.colour.a = 1.0f;
                break;
        }
    }

    float angle;
    if (selectionMode != SelectionMode::None) {
        glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};

        glm::vec3 linePointViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{linePoint, 1.0f}};
        glm::vec3 lineDirViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{lineDir, 0.0f}};

        glm::vec3 positionViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f}};

        float viewPlaneIntersectionVS = GizmoManager::intersectionOfLineAndPlane(linePointViewS, lineDirViewS, positionViewS, viewDirection);

        glm::vec3 intersectionPointVS = linePointViewS + viewPlaneIntersectionVS * lineDirViewS;

        glm::vec3 dirVec = glm::normalize(intersectionPointVS - positionViewS);

        if (glm::dot({0.0f, 1.0f, 0.0f}, dirVec) >= 0.0f) {
            angle = std::acos(glm::dot({1.0f, 0.0f, 0.0f}, dirVec));
        } else {
            angle = std::acos(glm::dot({-1.0f, 0.0f, 0.0f}, dirVec)) + (float) M_PI;
        }
    }

    if (justEnabled) {
        initialAngle = angle;
        initialRotate = rotation;
    }

    switch (selectionMode) {
        case xAxis: {
            glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};
            glm::vec3 orientationVec = glm::vec4{masterRenderer.getCurrentViewMatrix() * xRotMatrix * glm::vec4{-1.0f, 0.0f, 0.0f, 0.0f}};
            if (glm::dot(orientationVec, viewDirection) > 0.0f) {
                rotation.x = angle - initialAngle + initialRotate.x;
            } else {
                rotation.x = -angle + initialAngle + initialRotate.x;
            }

            PERIOD(rotation.x, 2.0f * M_PI);
            break;
        }
        case yAxis: {
            glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};
            glm::vec3 orientationVec = glm::vec4{masterRenderer.getCurrentViewMatrix() * yRotMatrix * glm::vec4{0.0f, -1.0f, 0.0f, 0.0f}};
            if (glm::dot(orientationVec, viewDirection) > 0.0f) {
                rotation.y = angle - initialAngle + initialRotate.y;
            } else {
                rotation.y = -angle + initialAngle + initialRotate.y;
            }

            PERIOD(rotation.y, 2.0f * M_PI);
            break;
        }
        case zAxis: {
            glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};
            glm::vec3 orientationVec = glm::vec4{masterRenderer.getCurrentViewMatrix() * zRotMatrix * glm::vec4{0.0f, 0.0f, -1.0f, 0.0f}};
            if (glm::dot(orientationVec, viewDirection) > 0.0f) {
                rotation.z = angle - initialAngle + initialRotate.z;
            } else {
                rotation.z = -angle + initialAngle + initialRotate.z;
            }

            PERIOD(rotation.z, 2.0f * M_PI);
            break;
        }
        case screenMove: {
            //TODO: to practically perform this kind of free rotation I would really need to implement Quaternions, so we'll see
            break;
        }
        case None:
            break;
    }

    glm::vec4 viewPos = masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f};

    glm::mat4 modelMatrix = glm::translate(position) * glm::scale(glm::vec3{-viewPos.z * GizmoManager::moveArrowOverallScale});
    xAxisEntity.modelMatrix = modelMatrix * xRotMatrix;
    yAxisEntity.modelMatrix = modelMatrix * yRotMatrix;
    zAxisEntity.modelMatrix = modelMatrix * zRotMatrix;

    masterRenderer.setGizmoEntities({xAxisEntity, yAxisEntity, zAxisEntity});
    return selectionMode != SelectionMode::None;
}

YawPitchGizmo::YawPitchGizmo(MasterRenderer &masterRenderer) {
    ModelID yAxisRot = GizmoManager::generateAxisRing(masterRenderer, {0.0f, 1.0f, 0.0f}, 0.95f);
    ModelID zAxisRot = GizmoManager::generateAxisRing(masterRenderer, {0.0f, 0.0f, 1.0f});

    float boxSize = GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowCentreBoxScale * 2.0f * 1.05f; // x2 from diameter = 2 x radius
    const float radius = GizmoManager::moveArrowLength;

    Mesh freeMoveBoxMesh = MeshGenerator::genParallelepipedNoNormNoTex(glm::vec3{radius, 0.0f, 0.0f}, glm::vec3{boxSize, 0.0f, 0.0f}, glm::vec3{0.0f, boxSize, 0.0f}, glm::vec3{0.0f, 0.0f, boxSize});
    ModelID freeMoveBox = masterRenderer.loadModel(freeMoveBoxMesh.modelData, freeMoveBoxMesh.modelMetaData);

    Mesh reverseFreeMoveBoxMesh = MeshGenerator::genParallelepipedNoNormNoTex(glm::vec3{-radius, 0.0f, 0.0f}, glm::vec3{boxSize, 0.0f, 0.0f}, glm::vec3{0.0f, boxSize, 0.0f}, glm::vec3{0.0f, 0.0f, boxSize});
    ModelID reverseFreeMoveBox = masterRenderer.loadModel(reverseFreeMoveBoxMesh.modelData, reverseFreeMoveBoxMesh.modelMetaData);

    ModelID directionLineModel = masterRenderer.loadModel(
            {
                    Vertex{glm::vec3{0.0f}, glm::vec3{0.0f}, glm::vec2{0.0f}},
                    Vertex{glm::vec3{1.0f, 0.0f, 0.0f}, glm::vec3{0.0f}, glm::vec2{0.0f}}
            }, {0, 1});

    yawEntity = {
            yAxisRot,
            glm::mat4{1.0f},
            {0.0f, 1.0f, 0.0f, GizmoManager::gizmoAlpha}
    };
    pitchEntity = {
            zAxisRot,
            glm::mat4{1.0f},
            {0.0f, 0.0f, 1.0f, GizmoManager::gizmoAlpha}
    };
    freeMoveEntity = {
            freeMoveBox,
            glm::mat4{1.0f},
            glm::vec4{1.0f}
    };
    reverseFreeMoveEntity = {
            reverseFreeMoveBox,
            glm::mat4{1.0f},
            glm::vec4{1.0f, 0.0f, 0.0f, 1.0f}
    };
    directionLineEntity = {
            directionLineModel,
            glm::mat4{1.0f},
            glm::vec4{1.0f}
    };
}

bool YawPitchGizmo::update(MasterRenderer &masterRenderer, const Window &window, const Camera &camera, const glm::vec3 &position, float &yaw, float &pitch, float overallScale, bool extendLine) {

    glm::vec linePoint = masterRenderer.getCurrentCameraPos();
    glm::vec lineDir = GizmoManager::getPointerDirWorldSpace(masterRenderer, window);

    float initScale;
    {
        glm::vec4 viewPos = masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f};
        initScale = -viewPos.z * GizmoManager::moveArrowOverallScale * overallScale;
    }

    const float radius = (GizmoManager::moveArrowLength) *initScale;
    const float width = (GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowHeadRadiusRatio * 2.0f) * initScale;

    const glm::mat4 yawMatrix = glm::rotate(-yaw, glm::vec3{0.0f, 1.0f, 0.0f});
    const glm::mat4 yawPitchMatrix = yawMatrix * glm::rotate(pitch, glm::vec3{0.0f, 0.0f, 1.0f});

    const glm::vec3 yawAxis = glm::vec3{0.0f, 1.0f, 0.0f};
    const glm::vec3 pitchAxis = yawMatrix * glm::vec4{0.0f, 0.0f, 1.0f, 0.0f};

    bool mouseIsPressed = window.isMousePressed(GLFW_MOUSE_BUTTON_LEFT);

    bool justEnabled = false;
    if (mouseWasPressed && !mouseIsPressed) {
        selectionMode = None;
        mouseWasPressed = false;
    } else if (!mouseWasPressed && mouseIsPressed) {
        mouseWasPressed = true;
        justEnabled = true;

//        auto yawIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, yawAxis, radius);
//        auto pitchIntersection = GizmoManager::intersectionOfLineAndCylinder(linePoint, lineDir, position, pitchAxis, radius);

        auto yawIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, yawAxis * width, radius, width);
        auto pitchIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, pitchAxis * width, radius, width);

        float freeMoveRadius = GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowCentreBoxScale * std::sqrt(3.0f) * initScale;
        glm::vec3 freeMoveOffset = yawPitchMatrix * (glm::vec4{radius, 0.0f, 0.0f, 0.0f});
        glm::vec3 reverseFreeMoveOffset = yawPitchMatrix * (glm::vec4{-radius, 0.0f, 0.0f, 0.0f});

        auto freeMoveIntersection = GizmoManager::intersectionOfLineAndSphere(linePoint, lineDir, position + freeMoveOffset, freeMoveRadius);
        auto reverseFreeMoveIntersection = GizmoManager::intersectionOfLineAndSphere(linePoint, lineDir, position + reverseFreeMoveOffset, freeMoveRadius);

        auto closestFree = GizmoManager::closestIntersection(
                {freeMoveIntersection},
                {});

        auto closestReverseFree = GizmoManager::closestIntersection(
                {reverseFreeMoveIntersection},
                {});

        std::vector<float> intersection = {yawIntersection, pitchIntersection, closestFree.second, closestReverseFree.second};
        size_t closest = -1;
        for (size_t i = 0; i < intersection.size(); i++) {
            if (intersection[i] >= 0.0f && (closest == -1 || intersection[i] < intersection[closest])) {
                closest = i;
            }
        }

        if (closest != -1) {
            switch (closest) {
                case 0:
                    selectionMode = SelectionMode::yaw;
                    break;
                case 1:
                    selectionMode = SelectionMode::pitch;
                    break;
                case 2:
                    selectionMode = SelectionMode::freeMove;
                    break;
                case 3:
                    selectionMode = SelectionMode::reverseFreeMove;
                    break;
            }
        } else {
            selectionMode = SelectionMode::screenMove;
        }
    }

    if (!mouseIsPressed) {
        yawEntity.colour.a = GizmoManager::gizmoAlpha;
        pitchEntity.colour.a = GizmoManager::gizmoAlpha;

        auto yawIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, yawAxis * width, radius, width);
        auto pitchIntersection = GizmoManager::closestIntersectionOfLineAndSquareToroid(linePoint, lineDir, position, pitchAxis * width, radius, width);

        float freeMoveRadius = GizmoManager::moveArrowCylinderRadius * GizmoManager::moveArrowCentreBoxScale * std::sqrt(3.0f) * initScale;
        glm::vec3 freeMoveOffset = yawPitchMatrix * (glm::vec4{radius, 0.0f, 0.0f, 0.0f});
        glm::vec3 reverseFreeMoveOffset = yawPitchMatrix * (glm::vec4{-radius, 0.0f, 0.0f, 0.0f});

        auto freeMoveIntersection = GizmoManager::intersectionOfLineAndSphere(linePoint, lineDir, position + freeMoveOffset, freeMoveRadius);
        auto reverseFreeMoveIntersection = GizmoManager::intersectionOfLineAndSphere(linePoint, lineDir, position + reverseFreeMoveOffset, freeMoveRadius);

        auto closestFree = GizmoManager::closestIntersection(
                {freeMoveIntersection},
                {});

        auto closestReverseFree = GizmoManager::closestIntersection(
                {reverseFreeMoveIntersection},
                {});

        std::vector<float> intersection = {yawIntersection, pitchIntersection, closestFree.second, closestReverseFree.second};
        size_t closest = -1;
        for (size_t i = 0; i < intersection.size(); i++) {
            if (intersection[i] >= 0.0f && (closest == -1 || intersection[i] < intersection[closest])) {
                closest = i;
            }
        }

        switch (closest) {
            case 0:
                yawEntity.colour.a = 1.0f;
                break;
            case 1:
                pitchEntity.colour.a = 1.0f;
                break;
            case 2:
//                selectionMode = SelectionMode::freeMove;
                break;
            case 3:
//                selectionMode = SelectionMode::reverseFreeMove;
                break;
        }
    }

    float angle;
    if (selectionMode != SelectionMode::None) {
        glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};

        glm::vec3 linePointViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{linePoint, 1.0f}};
        glm::vec3 lineDirViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{lineDir, 0.0f}};

        glm::vec3 positionViewS = glm::vec3{masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f}};

        float viewPlaneIntersectionVS = GizmoManager::intersectionOfLineAndPlane(linePointViewS, lineDirViewS, positionViewS, viewDirection);

        glm::vec3 intersectionPointVS = linePointViewS + viewPlaneIntersectionVS * lineDirViewS;

        glm::vec3 dirVec = glm::normalize(intersectionPointVS - positionViewS);

        if (glm::dot({0.0f, 1.0f, 0.0f}, dirVec) >= 0.0f) {
            angle = std::acos(glm::dot({1.0f, 0.0f, 0.0f}, dirVec));
        } else {
            angle = std::acos(glm::dot({-1.0f, 0.0f, 0.0f}, dirVec)) + (float) M_PI;
        }
    }

    if (justEnabled) {
        initialAngle = angle;
        initialYaw = yaw;
        initialPitch = pitch;
    }

    switch (selectionMode) {
        case SelectionMode::yaw: {
            glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};
            glm::vec3 orientationVec = masterRenderer.getCurrentViewMatrix() * glm::vec4{0.0f, 1.0f, 0.0f, 0.0f};
            if (glm::dot(orientationVec, viewDirection) > 0.0f) {
                yaw = angle - initialAngle + initialYaw;
            } else {
                yaw = -angle + initialAngle + initialYaw;
            }

            PERIOD(yaw, 2.0f * M_PI);
            break;
        }
        case SelectionMode::pitch: {
            glm::vec3 viewDirection = glm::vec3{0.0f, 0.0f, -1.0f};
            glm::vec3 orientationVec = masterRenderer.getCurrentViewMatrix() * yawMatrix * glm::vec4{0.0f, 0.0f, -1.0f, 0.0f};
            if (glm::dot(orientationVec, viewDirection) > 0.0f) {
                pitch = angle - initialAngle + initialPitch;
            } else {
                pitch = -angle + initialAngle + initialPitch;
            }

            pitch += M_PI;
            PERIOD(pitch, 2.0f * M_PI);
            pitch -= M_PI;

            break;
        }
        case SelectionMode::freeMove: {
            auto gizmoSphereIntersection = GizmoManager::intersectionOfLineAndSphere(linePoint, lineDir, position, radius);

            auto closestIntersection = GizmoManager::closestIntersection({gizmoSphereIntersection}, {});

            if (closestIntersection.second < 0.0f) {
                yaw = -camera.getYaw();
                pitch = angle;

                break;
            }

            glm::vec3 intersectionPoint = linePoint + lineDir * closestIntersection.second;

            glm::vec3 dir = glm::normalize(intersectionPoint - position);

            float dirPitch = std::asin(dir.y);

            dir.y = 0;
            dir = glm::normalize(dir);

            glm::vec3 a = {1.0f, 0.0f, 0.0f};
            glm::vec3 b = {0.0f, 0.0f, 1.0f};

            float dirYaw = 0.0f;
            if (glm::dot(b, dir) > 0.0f) {
                dirYaw = std::acos(glm::dot(dir, a));
            } else {
                dirYaw = std::acos(-glm::dot(dir, a)) + (float) M_PI;
            }

            pitch = dirPitch;
            yaw = dirYaw;

            break;
        }
        case SelectionMode::reverseFreeMove: {
            auto gizmoSphereIntersection = GizmoManager::intersectionOfLineAndSphere(linePoint, lineDir, position, radius);

            auto closestIntersection = GizmoManager::closestIntersection({gizmoSphereIntersection}, {});

            if (closestIntersection.second < 0.0f) {
                yaw = -camera.getYaw() + (float) M_PI;
                pitch = -angle;

                break;
            }

            glm::vec3 intersectionPoint = linePoint + lineDir * closestIntersection.second;

            glm::vec3 dir = glm::normalize(intersectionPoint - position);

            float dirPitch = std::asin(dir.y);

            dir.y = 0;
            dir = glm::normalize(dir);

            glm::vec3 a = {1.0f, 0.0f, 0.0f};
            glm::vec3 b = {0.0f, 0.0f, 1.0f};

            float dirYaw = 0.0f;
            if (glm::dot(b, dir) > 0.0f) {
                dirYaw = std::acos(glm::dot(dir, a));
            } else {
                dirYaw = std::acos(-glm::dot(dir, a)) + (float) M_PI;
            }

            pitch = -dirPitch;
            yaw = dirYaw + (float) M_PI;

            break;
        }
        case SelectionMode::screenMove: {
            glm::vec2 mouseDelta = window.getMouseMotion(GLFW_MOUSE_BUTTON_LEFT) / glm::dvec2{window.getWindowSize()};

            yaw += mouseDelta.x * 10.0f;
            pitch += mouseDelta.y * 10.0f;

            break;
        }
        case None:
            break;
    }

    glm::vec4 viewPos = masterRenderer.getCurrentViewMatrix() * glm::vec4{position, 1.0f};

    float lineLength;
    if (extendLine) {
        lineLength = MasterRenderer::FAR;
    } else {
        lineLength = radius;
    }


    glm::mat4 modelMatrix = glm::translate(position) * glm::scale(glm::vec3{-viewPos.z * GizmoManager::moveArrowOverallScale * overallScale});
    yawEntity.modelMatrix = modelMatrix * yawMatrix;
    pitchEntity.modelMatrix = modelMatrix * yawPitchMatrix;
    freeMoveEntity.modelMatrix = modelMatrix * yawPitchMatrix;
    reverseFreeMoveEntity.modelMatrix = modelMatrix * yawPitchMatrix;
    directionLineEntity.modelMatrix = glm::translate(position) * yawPitchMatrix * glm::scale(glm::vec3{lineLength});

    masterRenderer.setGizmoEntities({yawEntity, pitchEntity, freeMoveEntity, reverseFreeMoveEntity}, {directionLineEntity});
    return selectionMode != SelectionMode::None;
}