#ifndef GIZMO_MANAGER_H
#define GIZMO_MANAGER_H

#include "../rendering/MasterRenderer.h"

class MoveGizmo {
    GizmoEntityData xAxisEntity{};
    GizmoEntityData yAxisEntity{};
    GizmoEntityData zAxisEntity{};
    GizmoEntityData centreEntity{};
    GizmoEntityData yzPlaneEntity{};
    GizmoEntityData xzPlaneEntity{};
    GizmoEntityData xyPlaneEntity{};

    bool mouseWasPressed = false;

    enum SelectionMode {
        None,
        xAxis,
        yAxis,
        zAxis,
        yzPlane,
        xzPlane,
        xyPlane,
        viewPlane
    } selectionMode = None;

    glm::vec3 initialOffset = glm::vec3{0.0f};

    explicit MoveGizmo(MasterRenderer &masterRenderer);

    friend class GizmoManager;

public:
    bool update(MasterRenderer &masterRenderer, const Window &window, glm::vec3 &position, float overallScale = 1.0f);
};

class ScaleGizmo {
    GizmoEntityData xAxisEntity{};
    GizmoEntityData yAxisEntity{};
    GizmoEntityData zAxisEntity{};
    GizmoEntityData centreEntity{};

    bool mouseWasPressed = false;

    enum SelectionMode {
        None,
        xAxis,
        yAxis,
        zAxis,
        uniform
    } selectionMode = None;

    glm::vec3 initialObjScale = glm::vec3{0.0f};
    glm::vec3 initialOffset = glm::vec3{0.0f};

    explicit ScaleGizmo(MasterRenderer &masterRenderer);

    friend class GizmoManager;

public:
    bool update(MasterRenderer &masterRenderer, const Window &window, const glm::vec3 &position, glm::vec3 &scale, float overallScale = 1.0f);
};

class RotateGizmo {
    GizmoEntityData xAxisEntity{};
    GizmoEntityData yAxisEntity{};
    GizmoEntityData zAxisEntity{};

    bool mouseWasPressed = false;

    enum SelectionMode {
        None,
        xAxis,
        yAxis,
        zAxis,
        screenMove
    } selectionMode = None;

    float initialAngle = 0.0f;
    glm::vec3 initialRotate = glm::vec3{0.0f};

    explicit RotateGizmo(MasterRenderer &masterRenderer);

    friend class GizmoManager;

public:
    bool update(MasterRenderer &masterRenderer, const Window &window, const glm::vec3 &position, glm::vec3 &rotation, float overallScale = 1.0f);
};

class YawPitchGizmo {
    GizmoEntityData yawEntity{};
    GizmoEntityData pitchEntity{};
    GizmoEntityData freeMoveEntity{};
    GizmoEntityData reverseFreeMoveEntity{};
    GizmoEntityData directionLineEntity{};

    bool mouseWasPressed = false;

    enum SelectionMode {
        None,
        yaw,
        pitch,
        freeMove,
        reverseFreeMove,
        screenMove
    } selectionMode = None;

    float initialAngle = 0.0f;
    float initialYaw = 0.0f;
    float initialPitch = 0.0f;

    explicit YawPitchGizmo(MasterRenderer &masterRenderer);

    friend class GizmoManager;

public:
    bool update(MasterRenderer &masterRenderer, const Window &window, const Camera &camera, const glm::vec3 &position, float &yaw, float &pitch, float overallScale = 1.0f, bool extendLine = true);
};

class GizmoManager {
    const static float moveArrowLength;
    const static float moveArrowHeadLengthRatio;
    const static float moveArrowCylinderRadius;
    const static float moveArrowHeadRadiusRatio;
    const static float moveArrowCentreBoxScale;
    const static float moveArrowPlaneScale;
    const static float moveArrowOverallScale;

    const static uint sides;

    const static float gizmoAlpha;

    static ModelID generateAxisArrow(MasterRenderer &masterRenderer, glm::vec3 axis);
    static ModelID generateAxisScaleArm(MasterRenderer &masterRenderer, glm::vec3 axis, glm::vec3 tangent);
    static ModelID generateAxisRing(MasterRenderer &masterRenderer, glm::vec3 axis, float widthScale = 1.0f);

    // Returns parametrisation factor of intersection line
    static float intersectionOfLineAndPlane(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 planePoint, glm::vec3 planeNormal);

    enum IntersectionType {
        None,
        Single,
        Double,
    };

    struct NoneIntersection {
    };
    struct SingleIntersection {
        float lineParameter;
        float objectParameter;
    };
    struct DoubleIntersection {
        SingleIntersection minLinePair;
        SingleIntersection maxLinePair;
    };

    typedef std::variant<NoneIntersection, SingleIntersection, DoubleIntersection> IntersectionVariant;

    static std::pair<GizmoManager::IntersectionType, GizmoManager::IntersectionVariant> intersectionOfLineAndSquare(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 planePoint, glm::vec3 planeNormal, glm::vec3 tangent);

    static std::pair<IntersectionType, IntersectionVariant> intersectionOfLineAndCylinder(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 cylinderPoint, glm::vec3 cylinderDir, float radius);

    static std::pair<IntersectionType, IntersectionVariant> intersectionOfLineAndSphere(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 sphereCenter, float radius);

    static std::pair<IntersectionType, IntersectionVariant> intersectionOfLineAndCircle(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 circleCenter, glm::vec3 norm);

    static float closestIntersectionOfLineAndSquareToroid(glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 centre, glm::vec3 length, float averageRadius, float thickness);

    struct ClosestApproach {
        float line1Parameter;
        float line2Parameter;
        float distance;
    };

    static ClosestApproach closestApproachLineAndLine(glm::vec3 line1Point, glm::vec3 line1Dir, glm::vec3 line2Point, glm::vec3 line2Dir);

    static std::pair<size_t, float> closestIntersection(const std::vector<std::pair<IntersectionType, IntersectionVariant>> &intersections, const std::vector<std::pair<float, float>> &objectParamRanges);

    friend class MoveGizmo;

    friend class ScaleGizmo;

    friend class RotateGizmo;

    friend class YawPitchGizmo;

public:
    static glm::vec3 getPointerDirViewSpace(MasterRenderer &masterRenderer, const Window &window);
    static glm::vec3 getPointerDirWorldSpace(MasterRenderer &masterRenderer, const Window &window);

    static MoveGizmo createMoveGizmo(MasterRenderer &masterRenderer);
    static ScaleGizmo createScaleGizmo(MasterRenderer &masterRenderer);
    static RotateGizmo createRotateGizmo(MasterRenderer &masterRenderer);
    static YawPitchGizmo createYawPitchGizmo(MasterRenderer &masterRenderer);

    static void update(MasterRenderer &masterRenderer);
};

#endif //GIZMO_MANAGER_H
