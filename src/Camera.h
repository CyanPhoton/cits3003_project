#ifndef CAMERA_H
#define CAMERA_H

#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#include <math.h>
#endif

#include <glm/glm.hpp>

#include "system_interfaces/WindowManager.h"

class Camera {
    float initDistance = 1.0f;
    glm::vec3 initFocusPoint = {0.0f, 0.0f, 0.0f};
    float initPitch = 0.0f;
    float initYaw = 0.0f;

    float initFOV = glm::radians(90.0f);

    float distance = initDistance;
    glm::vec3 focusPoint = initFocusPoint;
    float pitch = initPitch;
    float yaw = initYaw;

    float fov = initFOV;

    const glm::vec3 UP = {0.0f, 1.0f, 0.0f};
    const glm::vec3 RIGHT = {1.0f, 0.0f, 0.0f};
    const glm::vec3 FORWARD = {0.0f, 0.0f, -1.0f};

    const float YAW_SPEED = 0.01f;
    const float PITCH_SPEED = 0.01f;
    const float ZOOM_SPEED = 0.1f;
    const float ZOOM_SCROLL_MULTIPLIER = 2.0f;
    const float MOVE_SPEED = 1.0f;
    const float ELEVATION_SPEED = 1.0f;

    const float MIN_DISTANCE = 0.001f;
    const float MAX_DISTANCE = 10000.0f;
    const float YAW_PERIOD = 2.0f * M_PI;
    const float PITCH_MIN = -M_PI_2 + 0.01f;
    const float PITCH_MAX = M_PI_2 - 0.01f;

    const glm::vec2 MIN_POS = {-10.0f, -10.0f};
    const glm::vec2 MAX_POS = {10.0f, 10.0f};

    glm::vec3 lastCameraPos{};
    glm::mat4 lastViewMatrix{};
public:
    Camera() = default;
    Camera(float distance, glm::vec3 focusPoint, float pitch, float yaw);

    void update(const Window& window, float dt, bool controlsEnabled = true);

    void reset();
    void addImGuiSection();

    glm::mat4 getViewMatrix() const;
    glm::vec3 getPosition() const;
    float getFOV() const;

    float getPitch() const;
    float getYaw() const;
};

#endif //CAMERA_H
