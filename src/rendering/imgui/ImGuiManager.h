#ifndef IMGUI_MANAGER_H
#define IMGUI_MANAGER_H

#include "../../system_interfaces/WindowManager.h"
#include "ImGuiImpl.h"

class ImGuiManager {
    Window window;
public:
    explicit ImGuiManager(Window window);

    void newFrame();
    void render();
    void cleanup();

    void enableMainWindowDocking();
};

namespace ImGui {
    bool InputText(const char *label, std::string *str, ImGuiInputTextFlags flags);

    void HelpMarker(const char* desc);
}

#endif //IMGUI_MANAGER_H
