#include "Model.h"

#include <iostream>
#include <utility>

#include <assimp/postprocess.h>

ModelID ModelManager::loadModel(const std::string& name) {
    const aiScene *scene = importer.ReadFile(MODEL_DIR + "/" + name, aiProcessPreset_TargetRealtime_Quality | aiProcess_TransformUVCoords
    | aiProcess_MakeLeftHanded // Note: Actually converts to RH, as models appear to be LH
    // | aiProcess_FlipUVs // Possibly? Hard to tell without an intended properly mapped texture
    );

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        std::cout << "Failed to load model (" << name << "): \n\t" << importer.GetErrorString() << std::endl;
        exit(EXIT_FAILURE);
    }

    ModelID modelID{};
    modelID.id = nextID++;

    // Assume model is just 1 mesh at origin:
    ModelMetaData modelMetaData{};
    ModelData modelData = loadMeshToData(scene->mMeshes[0], modelMetaData);
    if (scene->mNumMeshes > 1) {
        std::cerr << "Mesh count for (" << name << ") = " << scene->mNumMeshes << std::endl;
        std::cerr << "However assuming only 1, so some data may be missing" << std::endl;
    }

    modelDataMap.insert({modelID, modelData});

    modelMetaDataMap.insert({modelID, modelMetaData});

    importer.FreeScene();
    return modelID;
}

ModelID ModelManager::loadModel(std::vector<Vertex> vertices, std::vector<uint32_t> indices) {
    ModelID modelID{};
    modelID.id = nextID++;


    ModelMetaData modelMetaData{};
    ModelData modelData = loadMeshToData(std::move(vertices), std::move(indices), modelMetaData);

    modelDataMap.insert({modelID, modelData});
    modelMetaDataMap.insert({modelID, modelMetaData});

    return modelID;
}
ModelID ModelManager::loadModel(ModelData modelData, ModelMetaData modelMetaData) {
    ModelID modelID{};
    modelID.id = nextID++;

    modelDataMap.insert({modelID, std::move(modelData)});
    modelMetaDataMap.insert({modelID, modelMetaData});

    return modelID;
}

void ModelManager::unloadModel(ModelID id) {
    modelDataMap.erase(id);
    modelMetaDataMap.erase(id);
}

ModelData ModelManager::loadMeshToData(aiMesh* mesh, ModelMetaData& metaData) {
    ModelData modelData;

    for (auto i = 0; i < mesh->mNumVertices; i++) {
        Vertex vertex{};

        vertex.position.x = mesh->mVertices[i].x;
        vertex.position.y = mesh->mVertices[i].y;
        vertex.position.z = mesh->mVertices[i].z;

        metaData.boundingAABB.min = glm::min(metaData.boundingAABB.min, vertex.position);
        metaData.boundingAABB.max = glm::max(metaData.boundingAABB.max, vertex.position);

        vertex.normal.x = mesh->mNormals[i].x;
        vertex.normal.y = mesh->mNormals[i].y;
        vertex.normal.z = mesh->mNormals[i].z;

        vertex.textCoords.s = mesh->mTextureCoords[0][i].x;
        vertex.textCoords.t = mesh->mTextureCoords[0][i].y;

        modelData.vertices.push_back(vertex);
    }

    for (auto i = 0; i < mesh->mNumFaces; ++i) {
        aiFace face = mesh->mFaces[i];
        for (auto j = 0; j < face.mNumIndices; j++) {
            modelData.indices.push_back(face.mIndices[j]);
        }
    }

    return modelData;
}

ModelData ModelManager::loadMeshToData(std::vector<Vertex> vertices, std::vector<uint32_t> indices, ModelMetaData& metaData) {
    ModelData modelData;

    modelData.vertices = std::move(vertices);
    modelData.indices = std::move(indices);

    for (const auto& vertex : modelData.vertices) {
        metaData.boundingAABB.min = glm::min(metaData.boundingAABB.min, vertex.position);
        metaData.boundingAABB.max = glm::max(metaData.boundingAABB.max, vertex.position);
    }

    return modelData;
}

bool ModelManager::modelExists(ModelID id) {
    return modelDataMap.count(id) > 0;
}

const ModelData &ModelManager::getModel(ModelID id) {
    return modelDataMap.at(id);
}

const ModelMetaData &ModelManager::getModelMeta(ModelID id) {
    return modelMetaDataMap.at(id);
}

std::size_t std::hash<ModelID>::operator()(const ModelID &k) const {
    using std::hash;

    return hash<uint>()(k.id);
}