#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>

#include "../utility/Defs.h"

class ModelManager;

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 textCoords;
};

struct ModelData {
    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;
};

struct ModelMetaData {
    struct AABB {
        glm::vec3 min;
        glm::vec3 max;
    } boundingAABB;
};

struct ModelID;

namespace std {
    template<>
    struct hash<ModelID> {
        std::size_t operator()(const ModelID &k) const;
    };
}

struct ModelID {
    bool operator==(const ModelID &other) const {
        return id == other.id;
    }

private:
    uint id;

    friend class ModelManager;
    friend std::size_t std::hash<ModelID>::operator()(const ModelID &k) const;
};

class ModelManager {
    const std::string MODEL_DIR = "res/models-textures";

    uint nextID = 0;
    std::unordered_map<ModelID, ModelData> modelDataMap;
    std::unordered_map<ModelID, ModelMetaData> modelMetaDataMap;

    Assimp::Importer importer;
public:
    ModelManager() = default;

    ModelID loadModel(const std::string &name);
    ModelID loadModel(std::vector<Vertex> vertices, std::vector<uint32_t> indices);
    ModelID loadModel(ModelData modelData, ModelMetaData modelMetaData);
    void unloadModel(ModelID id);

    bool modelExists(ModelID id);
    const ModelData &getModel(ModelID id);
    const ModelMetaData &getModelMeta(ModelID id);
private:
    static ModelData loadMeshToData(aiMesh *mesh, ModelMetaData& metaData);
    static ModelData loadMeshToData(std::vector<Vertex> vertices, std::vector<uint32_t> indices, ModelMetaData& metaData);
};

#endif //MODEL_H
