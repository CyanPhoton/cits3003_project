#ifndef BASIC_SHADER_H
#define BASIC_SHADER_H

#include <glm/glm.hpp>

#include "ShaderInterface.h"

class BasicShader : public ShaderInterface {
public:
    BasicShader();

    void setProjViewMatrix(glm::mat4 projViewMatrix);
    void setCameraPos(glm::vec3 cameraPos);
};

#endif //BASIC_SHADER_H
