#include "ShaderInterface.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include <glad/gl.h>

ShaderInterface::ShaderInterface(const std::string &vertexPath, const std::string &fragmentPath,
                                 const std::unordered_map<std::string, std::string> &vertDefs, const std::unordered_map<std::string, std::string> &fragDefs)
        : uniformLocations() {

    vertexCode = loadShaderFile(vertexPath);
    fragmentCode = loadShaderFile(fragmentPath);

    std::string realisedVertexCode = applyDefs(vertexCode, vertDefs);
    std::string realisedFragmentCode = applyDefs(fragmentCode, fragDefs);

    uint vertexShader = compileShaderCode(realisedVertexCode, GL_VERTEX_SHADER);
    uint fragmentShader = compileShaderCode(realisedFragmentCode, GL_FRAGMENT_SHADER);

    programID = linkProgram(vertexShader, fragmentShader);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

uint ShaderInterface::id() const {
    return programID;
}

void ShaderInterface::use() const {
    glUseProgram(programID);
}

void ShaderInterface::recompile(const std::unordered_map<std::string, std::string> &vertDefs, const std::unordered_map<std::string, std::string> &fragDefs) {
    cleanup();

    std::string realisedVertexCode = applyDefs(vertexCode, vertDefs);
    std::string realisedFragmentCode = applyDefs(fragmentCode, fragDefs);

    uint vertexShader = compileShaderCode(realisedVertexCode, GL_VERTEX_SHADER);
    uint fragmentShader = compileShaderCode(realisedFragmentCode, GL_FRAGMENT_SHADER);

    programID = linkProgram(vertexShader, fragmentShader);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    uniformLocations.clear();
}

std::string ShaderInterface::loadShaderFile(const std::string &shaderPath) {
    std::string shaderCode;
    std::ifstream shaderFile;

    shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        shaderFile.open(SHADER_DIR + "/" + shaderPath);
        std::stringstream shaderStream;

        shaderStream << shaderFile.rdbuf();

        shaderFile.close();

        shaderCode = shaderStream.str();
    } catch (std::ifstream::failure &e) {
        std::cout << "Failed to load shader file: " << SHADER_DIR << "/" << shaderPath << std::endl;
        exit(EXIT_FAILURE);
    }

    return shaderCode;
}

std::string ShaderInterface::applyDefs(const std::string &code, const std::unordered_map<std::string, std::string> &defs) {
    std::stringstream output;

    auto versionStart = code.find("#version", 0);
    if (versionStart == std::string::npos) {
        std::cerr << "GLSL shader must specify version using #version" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto versionLineEnd = code.find('\n', versionStart);

    output << code.substr(0, versionLineEnd + 1);

    for (const auto &def : defs) {
        output << "#define " << def.first << " " << def.second << "\n";
    }

    output << code.substr(versionLineEnd + 1);

    return output.str();
}

uint ShaderInterface::compileShaderCode(const std::string &shaderCode, uint shaderType) {
    const int MSG_LEN = 512;

    uint shader = glCreateShader(shaderType);

    const char *sourceCStr = shaderCode.c_str();
    glShaderSource(shader, 1, &sourceCStr, nullptr);
    glCompileShader(shader);

    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        char infoLog[MSG_LEN];

        glGetShaderInfoLog(shader, 512, nullptr, infoLog);
        std::cout << "Failed to compile " << (shaderType == GL_VERTEX_SHADER ? "Vertex" : "Fragment") << " shader\n" << infoLog << std::endl;
        exit(EXIT_FAILURE);
    }

    return shader;
}

uint ShaderInterface::linkProgram(uint vertexShader, uint fragmentShader) {
    const int MSG_LEN = 512;

    uint program = glCreateProgram();

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    // print linking errors if any
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[MSG_LEN];

        glGetProgramInfoLog(program, 512, nullptr, infoLog);
        std::cout << "Failed to link program\n" << infoLog << std::endl;
        exit(EXIT_FAILURE);
    }

    return program;
}

void ShaderInterface::setUniformLocation(const std::string &name, int location) {
    if (location == -1) {
        getUniformLocation(name); // Automatically fetch
        return;
    }

    uniformLocations.insert({name, location});
}

int ShaderInterface::getUniformLocation(const std::string &name) {
    auto search = uniformLocations.find(name);

    if (search != uniformLocations.end()) {
        return search->second;
    } else {
        int location = glGetUniformLocation(programID, name.c_str());

        uniformLocations.insert({name, location});

        return location;
    }
}

void ShaderInterface::cleanup() {
    glDeleteProgram(id());
}