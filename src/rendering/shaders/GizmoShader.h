#ifndef GIZMO_SHADER_H
#define GIZMO_SHADER_H

#include <glm/glm.hpp>

#include "ShaderInterface.h"

class GizmoShader : public ShaderInterface {
public:
    GizmoShader();
    void setProjViewMatrix(glm::mat4 projViewMatrix);
    void setModelMatrix(glm::mat4 modelMatrix);
    void setColour(glm::vec4 colour);
};

#endif //GIZMO_SHADER_H
