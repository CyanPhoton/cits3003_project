#ifndef PICKER_SHADER_H
#define PICKER_SHADER_H

#include <glm/glm.hpp>

#include "ShaderInterface.h"

class PickerShader : public ShaderInterface {
public:
    PickerShader();
    void setProjViewMatrix(glm::mat4 projViewMatrix);
};

#endif //PICKER_SHADER_H
