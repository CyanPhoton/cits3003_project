#include "PickerShader.h"

#include <glad/gl.h>

PickerShader::PickerShader() : ShaderInterface("picker/vert.glsl", "picker/frag.glsl") {}

void PickerShader::setProjViewMatrix(glm::mat4 projViewMatrix) {
    glProgramUniformMatrix4fv(id(), getUniformLocation("projViewMatrix"), 1, false, &projViewMatrix[0][0]);
}