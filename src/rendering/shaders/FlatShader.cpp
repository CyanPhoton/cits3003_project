#include "FlatShader.h"

#include <glad/gl.h>

FlatShader::FlatShader() : ShaderInterface("flat/vert.glsl", "flat/frag.glsl") {}

void FlatShader::setProjViewMatrix(glm::mat4 projViewMatrix) {
    glProgramUniformMatrix4fv(id(), getUniformLocation("projViewMatrix"), 1, false, &projViewMatrix[0][0]);
}