#ifndef FLAT_SHADER_H
#define FLAT_SHADER_H

#include <glm/glm.hpp>

#include "ShaderInterface.h"

class FlatShader : public ShaderInterface {
public:
    FlatShader();
    void setProjViewMatrix(glm::mat4 projViewMatrix);
};

#endif //FLAT_SHADER_H
