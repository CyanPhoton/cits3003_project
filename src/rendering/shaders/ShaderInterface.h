#ifndef SHADER_INTERFACE_H
#define SHADER_INTERFACE_H

#include <string>
#include <unordered_map>
#include "../../utility/Defs.h"

class ShaderInterface {
    const std::string SHADER_DIR = "res/shaders";

    uint programID;

    std::unordered_map<std::string, int> uniformLocations;

    std::string vertexCode;
    std::string fragmentCode;
public:
    ShaderInterface(const std::string &vertexPath, const std::string &fragmentPath,
                    const std::unordered_map<std::string, std::string> &vertDefs = {}, const std::unordered_map<std::string, std::string> &fragDefs = {});

    uint id() const;

    void use() const;

    void recompile(const std::unordered_map<std::string, std::string> &vertDefs = {}, const std::unordered_map<std::string, std::string> &fragDefs = {});

    virtual void cleanup();

private:
    std::string loadShaderFile(const std::string &shaderPath);
    static std::string applyDefs(const std::string &code, const std::unordered_map<std::string, std::string> &defs);
    static uint compileShaderCode(const std::string &shaderCode, uint shaderType);
    static uint linkProgram(uint vertexShader, uint fragmentShader);

protected:
    void setUniformLocation(const std::string &name, int location = -1);
    int getUniformLocation(const std::string &name);
};

#endif //SHADER_INTERFACE_H
