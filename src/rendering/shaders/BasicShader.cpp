#include "BasicShader.h"

#include <glad/gl.h>

BasicShader::BasicShader()
        : ShaderInterface("basic/vert.glsl", "basic/frag.glsl") {
}

void BasicShader::setProjViewMatrix(glm::mat4 projViewMatrix) {
    glProgramUniformMatrix4fv(id(), getUniformLocation("projViewMatrix"), 1, false, &projViewMatrix[0][0]);
}

void BasicShader::setCameraPos(glm::vec3 cameraPos) {
    glProgramUniform3f(id(), getUniformLocation("cameraPos"), cameraPos.x, cameraPos.y, cameraPos.z);
}
