#ifndef OUTLINE_SHADER_H
#define OUTLINE_SHADER_H

#include <glm/glm.hpp>

#include "ShaderInterface.h"

class OutlineShader : public ShaderInterface {
public:
    OutlineShader();

    void setProjViewMatrix(glm::mat4 projViewMatrix);
    void setFlatColour(glm::vec4 flatColour);
    void setNormalDisplacement(float normalDisplacement);
};

#endif //OUTLINE_SHADER_H
