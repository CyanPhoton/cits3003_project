#include "OutlineShader.h"

#include <glad/gl.h>

OutlineShader::OutlineShader() : ShaderInterface("outline/vert.glsl", "outline/frag.glsl") {

}

void OutlineShader::setProjViewMatrix(glm::mat4 projViewMatrix) {
    glProgramUniformMatrix4fv(id(), getUniformLocation("projViewMatrix"), 1, false, &projViewMatrix[0][0]);
}

void OutlineShader::setFlatColour(glm::vec4 flatColour) {
    glProgramUniform4fv(id(), getUniformLocation("flatColour"), 1, &flatColour[0]);
}

void OutlineShader::setNormalDisplacement(float normalDisplacement) {
    glProgramUniform1f(id(), getUniformLocation("normalDisplacement"), normalDisplacement);
}
