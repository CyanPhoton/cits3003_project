#include "GizmoShader.h"

#include <glad/gl.h>

GizmoShader::GizmoShader() : ShaderInterface("gizmo/vert.glsl", "gizmo/frag.glsl") {
}

void GizmoShader::setProjViewMatrix(glm::mat4 projViewMatrix) {
    glUniformMatrix4fv(getUniformLocation("projViewMatrix"), 1, false, &projViewMatrix[0][0]);
}

void GizmoShader::setModelMatrix(glm::mat4 modelMatrix) {
    glUniformMatrix4fv(getUniformLocation("modelMatrix"), 1, false, &modelMatrix[0][0]);
}

void GizmoShader::setColour(glm::vec4 colour) {
    glUniform4fv(getUniformLocation("colour"), 1, &colour[0]);
}
