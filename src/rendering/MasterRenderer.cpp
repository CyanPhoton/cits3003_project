#include "MasterRenderer.h"

#include <vector>
#include <iostream>

#include <glad/gl.h>
#include <stb/stb_image.h>
#include <imgui/imgui.h>
#include <glm/gtx/transform.hpp>

const float MasterRenderer::NEAR = 0.001f;
const float MasterRenderer::FAR = 1.0e6f;

EntityData::EntityData(glm::mat4 modelMatrix) : modelMatrix(modelMatrix), normalMatrix(glm::mat3(1.0f)) {
    calcNormalMatrix();
}

EntityData EntityData::identity() {
    EntityData entityData(glm::mat4(1.0f));
    return entityData;
}

const glm::mat4 &EntityData::getModelMatrix() const {
    return modelMatrix;
}

void EntityData::setModelMatrix(glm::mat4 modelMat) {
    modelMatrix = modelMat;
    calcNormalMatrix();
}

void EntityData::calcNormalMatrix() {
    normalMatrix = glm::mat3(glm::transpose(glm::inverse(modelMatrix)));
}

FlatEntityData FlatEntityData::identity() {
    return FlatEntityData{glm::mat4(1.0f), glm::vec4(1.0f)};
}

FlatEntityData::FlatEntityData(glm::mat4 modelMatrix) : modelMatrix(modelMatrix), normalMatrix(glm::mat3{1.0f}) {
    calcNormalMatrix();
}

FlatEntityData::FlatEntityData(glm::mat4 modelMatrix, glm::vec4 colour) : modelMatrix(modelMatrix), normalMatrix(glm::mat3{1.0f}), colour(colour) {
    calcNormalMatrix();
}

const glm::mat4 &FlatEntityData::getModelMatrix() const {
    return modelMatrix;
}

void FlatEntityData::setModelMatrix(glm::mat4 modelMat) {
    modelMatrix = modelMat;
    calcNormalMatrix();
}

void FlatEntityData::calcNormalMatrix() {
    normalMatrix = glm::mat3(glm::transpose(glm::inverse(modelMatrix)));
}

GizmoEntityData GizmoEntityData::identity() {
    return GizmoEntityData();
}

EntityData::EntityData(const glm::mat4 &modelMatrix, const glm::mat3 &normalMatrix,
                       float diffuseFactor, float specularFactor, float globalFactor, float shininessExponent, const glm::vec3 &tintColour, float textureScale)
        : modelMatrix(modelMatrix), normalMatrix(normalMatrix),
          diffuseFactor(diffuseFactor), specularFactor(specularFactor), globalFactor(globalFactor), shininessExponent(shininessExponent), tintColour_TexScale(tintColour, textureScale) {}

MasterRenderer::MasterRenderer(const Window &window) : window(window) {
    glViewport(0, 0, window.getFramebufferWidth(), window.getFramebufferHeight());

//    glClearColor(0.1f, 0.4f, 0.45f, 1.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK); // Default: just being explicit
    glFrontFace(GL_CCW); // Default: just being explicit

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &vertexVBO);
    glGenBuffers(1, &indexVBO);
    glGenBuffers(1, &perInstanceVBO);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVBO);

    glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, position)));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, normal)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, textCoords)));

    glBindBuffer(GL_ARRAY_BUFFER, perInstanceVBO);
    // mat4 modelMatrix
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, modelMatrix) + 0 * sizeof(glm::vec4)));
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, modelMatrix) + 1 * sizeof(glm::vec4)));
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, modelMatrix) + 2 * sizeof(glm::vec4)));
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, modelMatrix) + 3 * sizeof(glm::vec4)));
    // mat3 normalMatrix
    glVertexAttribPointer(7, 3, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, normalMatrix) + 0 * sizeof(glm::vec3)));
    glVertexAttribPointer(8, 3, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, normalMatrix) + 1 * sizeof(glm::vec3)));
    glVertexAttribPointer(9, 3, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, normalMatrix) + 2 * sizeof(glm::vec3)));
    // vec4 materialProperties
    glVertexAttribPointer(10, 4, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, materialProperties)));
    // vec3 tintColour
    glVertexAttribPointer(11, 4, GL_FLOAT, GL_FALSE, sizeof(EntityData), reinterpret_cast<void *>(offsetof(EntityData, tintColour_TexScale)));

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    for (auto i = 3; i <= 11; i++) {
        glEnableVertexAttribArray(i);
        glVertexAttribDivisor(i, 1);
    }

    glBindVertexArray(0);

    // Flat stuff
    glGenVertexArrays(1, &flatVAO);
    glBindVertexArray(flatVAO);

    glGenBuffers(1, &perFlatInstanceVBO);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVBO);

    glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, position)));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, normal)));

    glBindBuffer(GL_ARRAY_BUFFER, perFlatInstanceVBO);
    // mat4 modelMatrix
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(FlatEntityData), reinterpret_cast<void *>(offsetof(FlatEntityData, modelMatrix) + 0 * sizeof(glm::vec4)));
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(FlatEntityData), reinterpret_cast<void *>(offsetof(FlatEntityData, modelMatrix) + 1 * sizeof(glm::vec4)));
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(FlatEntityData), reinterpret_cast<void *>(offsetof(FlatEntityData, modelMatrix) + 2 * sizeof(glm::vec4)));
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(FlatEntityData), reinterpret_cast<void *>(offsetof(FlatEntityData, modelMatrix) + 3 * sizeof(glm::vec4)));
    // mat3 normalMatrix
    glVertexAttribPointer(7, 3, GL_FLOAT, GL_FALSE, sizeof(FlatEntityData), reinterpret_cast<void *>(offsetof(FlatEntityData, normalMatrix) + 0 * sizeof(glm::vec3)));
    glVertexAttribPointer(8, 3, GL_FLOAT, GL_FALSE, sizeof(FlatEntityData), reinterpret_cast<void *>(offsetof(FlatEntityData, normalMatrix) + 1 * sizeof(glm::vec3)));
    glVertexAttribPointer(9, 3, GL_FLOAT, GL_FALSE, sizeof(FlatEntityData), reinterpret_cast<void *>(offsetof(FlatEntityData, normalMatrix) + 2 * sizeof(glm::vec3)));
    // vec4 colour
    glVertexAttribPointer(10, 4, GL_FLOAT, GL_FALSE, sizeof(FlatEntityData), reinterpret_cast<void *>(offsetof(FlatEntityData, colour)));

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    for (auto i = 3; i <= 10; i++) {
        glEnableVertexAttribArray(i);
        glVertexAttribDivisor(i, 1);
    }

    // Picker stuff
    glGenVertexArrays(1, &pickerVAO);
    glBindVertexArray(pickerVAO);

    glGenBuffers(1, &perPickerInstanceVBO);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVBO);

    glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, position)));

    glBindBuffer(GL_ARRAY_BUFFER, perPickerInstanceVBO);
    // mat4 modelMatrix
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(PickerEntityData), reinterpret_cast<void *>(offsetof(PickerEntityData, modelMatrix) + 0 * sizeof(glm::vec4)));
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(PickerEntityData), reinterpret_cast<void *>(offsetof(PickerEntityData, modelMatrix) + 1 * sizeof(glm::vec4)));
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(PickerEntityData), reinterpret_cast<void *>(offsetof(PickerEntityData, modelMatrix) + 2 * sizeof(glm::vec4)));
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(PickerEntityData), reinterpret_cast<void *>(offsetof(PickerEntityData, modelMatrix) + 3 * sizeof(glm::vec4)));
    // uint32 id/`colour`
    glVertexAttribIPointer(5, 1, GL_UNSIGNED_INT, sizeof(PickerEntityData), reinterpret_cast<void *>(offsetof(PickerEntityData, entityID)));

    glEnableVertexAttribArray(0);

    for (auto i = 1; i <= 5; i++) {
        glEnableVertexAttribArray(i);
        glVertexAttribDivisor(i, 1);
    }

    glBindVertexArray(0);

    {
        glGenFramebuffers(1, &pickerFBO);

        glBindFramebuffer(GL_FRAMEBUFFER, pickerFBO);

        glGenTextures(1, &pickerFBO_colour_Texture);
        glBindTexture(GL_TEXTURE_2D, pickerFBO_colour_Texture);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, 1, 1, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pickerFBO_colour_Texture, 0);

        glGenRenderbuffers(1, &pickerFBO_depthStencil_RBO);
        glBindRenderbuffer(GL_RENDERBUFFER, pickerFBO_depthStencil_RBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 1, 1);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, pickerFBO_depthStencil_RBO);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    // Light stuff
    glGenBuffers(1, &lightDataUB);
    glBindBuffer(GL_UNIFORM_BUFFER, lightDataUB);
}

void MasterRenderer::update() {
    if (window.getFramebufferWidth() == 0 || window.getFramebufferHeight() == 0)
        return;

    glViewport(0, 0, window.getFramebufferWidth(), window.getFramebufferHeight());

    auto aspectRatio = (float) window.getFramebufferWidth() / (float) window.getFramebufferHeight();
//    glm::mat4 projMat = glm::perspective(getVertFOV(), aspectRatio, NEAR, FAR);
    glm::mat4 projMat = glm::infinitePerspective(getVertFOV(), aspectRatio, NEAR);
    setProjectionMatrix(projMat);
}

void MasterRenderer::render() {
    if (window.getFramebufferWidth() == 0 || window.getFramebufferHeight() == 0)
        return;

    glm::mat4 projViewMatrix = projectionMatrix * viewMatrix;

    // Clear all buffers in FBO
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // Update vertex and uniform buffers, possibly recompile shader
    updateEntityData();
    updateLightData();
    updateShader();

    //
    // Main rendering
    //
    shader.use();
    shader.setProjViewMatrix(projViewMatrix);
    shader.setCameraPos(cameraPos);

    glBindVertexArray(VAO);

    for (const auto &texturePerModelEntityList : perTexturePerModelEntityList) {

        const auto &perModelEntityList = texturePerModelEntityList.second;
        uint textureID = textureInfos[texturePerModelEntityList.first].glTextureID;

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureID);

        for (const auto &modelEntityList: perModelEntityList) {

            const ModelData &model = modelManager.getModel(modelEntityList.first);
            const ModelInfo &modelInfo = modelInfos.at(modelEntityList.first);
            size_t entityCount = modelEntityList.second.size();

            uint instanceOffset = perTexturePerModelInstanceOffset[texturePerModelEntityList.first][modelEntityList.first];

            glDrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), entityCount, modelInfo.vertexOffset, instanceOffset);
        }
    }

    //
    // Flat rendering
    //
    flatShader.use();
    flatShader.setProjViewMatrix(projViewMatrix);

    glBindVertexArray(flatVAO);

    for (const auto &modelFlatEntityList: perModelFlatEntityList) {

        const ModelData &model = modelManager.getModel(modelFlatEntityList.first);
        const ModelInfo &modelInfo = modelInfos.at(modelFlatEntityList.first);
        size_t entityCount = modelFlatEntityList.second.size();

        uint instanceOffset = perModelFlatInstanceOffset[modelFlatEntityList.first];

        glDrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), entityCount, modelInfo.vertexOffset, instanceOffset);
    }

    //
    // Highlight rendering
    //


    if (enableWireframes)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    outlineShader.use();
    outlineShader.setProjViewMatrix(projViewMatrix);
    outlineShader.setFlatColour({selectedHighlightColour, 1.0f});
    outlineShader.setNormalDisplacement(0.0f);

    // Switch back to default VAO for highlight rendering
    glBindVertexArray(VAO);

    // Enable stencil test and disable depth testing to set stencil values
    glEnable(GL_STENCIL_TEST);
    // Clear Depth so highlights always render on top
    glClear(GL_DEPTH_BUFFER_BIT);

    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glStencilMask(0xFF);

    // Disable re-rendering the object
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

    for (const auto &entityID : selectedEntities) {
        if (entityInfoMap.count(entityID) > 0) {
            glBindVertexArray(VAO);
            const EntityInfo &entity = entityInfoMap[entityID];
            const ModelInfo &modelInfo = modelInfos.at(entity.modelID);
            const ModelData &model = modelManager.getModel(entity.modelID);

            const IDVec<EntityData> &dataVec = perTexturePerModelEntityList[entity.textureID][entity.modelID];

            uint instanceOffset = &dataVec.element(entity.dataID) - dataVec.data();
            instanceOffset += perTexturePerModelInstanceOffset[entity.textureID][entity.modelID];

            glDrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), 1, modelInfo.vertexOffset, instanceOffset);
        } else if (flatEntityInfoMap.count(entityID) > 0) {
            glBindVertexArray(flatVAO);

            const FlatEntityInfo &entity = flatEntityInfoMap[entityID];
            const ModelInfo &modelInfo = modelInfos.at(entity.modelID);
            const ModelData &model = modelManager.getModel(entity.modelID);

            const IDVec<FlatEntityData> &dataVec = perModelFlatEntityList[entity.modelID];

            uint instanceOffset = &dataVec.element(entity.dataID) - dataVec.data();
            instanceOffset += perModelFlatInstanceOffset[entity.modelID];

            glDrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), 1, modelInfo.vertexOffset, instanceOffset);
        } else {
            std::cerr << "[ERROR]: Tried to render highlighted entity that doesn't exist" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    // Use stencil buffer to draw highlights
    glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
    glStencilMask(0x00);
    glDisable(GL_DEPTH_TEST);
    glBindVertexArray(VAO);

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

    outlineShader.setNormalDisplacement(selectionThickness);

    for (const auto &entityID : selectedEntities) {
        if (entityInfoMap.count(entityID) > 0) {
            glBindVertexArray(VAO);
            const EntityInfo &entity = entityInfoMap[entityID];
            const ModelInfo &modelInfo = modelInfos.at(entity.modelID);
            const ModelData &model = modelManager.getModel(entity.modelID);

            const IDVec<EntityData> &dataVec = perTexturePerModelEntityList[entity.textureID][entity.modelID];

            uint instanceOffset = &dataVec.element(entity.dataID) - dataVec.data();
            instanceOffset += perTexturePerModelInstanceOffset[entity.textureID][entity.modelID];

            glDrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), 1, modelInfo.vertexOffset, instanceOffset);
        } else if (flatEntityInfoMap.count(entityID) > 0) {
            glBindVertexArray(flatVAO);

            const FlatEntityInfo &entity = flatEntityInfoMap[entityID];
            const ModelInfo &modelInfo = modelInfos.at(entity.modelID);
            const ModelData &model = modelManager.getModel(entity.modelID);

            const IDVec<FlatEntityData> &dataVec = perModelFlatEntityList[entity.modelID];

            uint instanceOffset = &dataVec.element(entity.dataID) - dataVec.data();
            instanceOffset += perModelFlatInstanceOffset[entity.modelID];

            glDrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), 1, modelInfo.vertexOffset, instanceOffset);
        } else {
            std::cerr << "[ERROR]: Tried to render highlighted entity that doesn't exist" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);

    //
    // Gizmo rendering
    //

    // Clear Depth so gizmo always renders on top
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);

    gizmoShader.use();
    gizmoShader.setProjViewMatrix(projViewMatrix);

    for (const auto &gizmoEntity: gizmoEntities) {
        const ModelData &model = modelManager.getModel(gizmoEntity.modelID);
        const ModelInfo &modelInfo = modelInfos.at(gizmoEntity.modelID);

        gizmoShader.setModelMatrix(gizmoEntity.modelMatrix);
        gizmoShader.setColour(gizmoEntity.colour);

        glDrawElementsBaseVertex(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), modelInfo.vertexOffset);
    }

    for (const auto &gizmoEntity: gizmoLineEntities) {
        const ModelData &model = modelManager.getModel(gizmoEntity.modelID);
        const ModelInfo &modelInfo = modelInfos.at(gizmoEntity.modelID);

        gizmoShader.setModelMatrix(gizmoEntity.modelMatrix);
        gizmoShader.setColour(gizmoEntity.colour);

        glDrawElementsBaseVertex(GL_LINES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), modelInfo.vertexOffset);
    }

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    if (enableWireframes)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void MasterRenderer::addImGuiSection(WindowManager &windowManager) {
    if (!ImGui::CollapsingHeader("Render Options")) {
        return;
    }

    if (ImGui::Checkbox("Back Face Culling", &enableBackFaceCulling)) {
        if (enableBackFaceCulling) {
            glEnable(GL_CULL_FACE);
        } else {
            glDisable(GL_CULL_FACE);
        }
    }

    if (ImGui::Checkbox("Wireframes", &enableWireframes)) {
        if (enableWireframes) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

    bool vSyncTemp = windowManager.getVSync();
    if (ImGui::Checkbox("V-Sync", &vSyncTemp)) {
        if (vSyncTemp) {
            windowManager.setVSync(true);
        } else {
            windowManager.setVSync(false);
        }
    }
}

std::optional<EntityID> MasterRenderer::getPickedEntity() {
    glm::ivec2 mousePosI = glm::ivec2{window.getMousePos()};
    if (mousePosI.x < 0 || mousePosI.y < 0 || mousePosI.x > window.getWindowWidth() || mousePosI.y > window.getWindowHeight()) {
        return {};
    }

    glBindFramebuffer(GL_FRAMEBUFFER, pickerFBO);
    mousePosI.y = window.getWindowHeight() - mousePosI.y;

    glViewport(-mousePosI.x, -mousePosI.y, window.getWindowWidth(), window.getWindowHeight());
    glEnable(GL_SCISSOR_TEST);
    glScissor(0, 0, 1, 1);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindVertexArray(pickerVAO);
    pickerShader.use();
    pickerShader.setProjViewMatrix(projectionMatrix * viewMatrix);

    for (const auto &modelID_PickerEntities: perModelPickerEntityList) {
        const ModelData &model = modelManager.getModel(modelID_PickerEntities.first);
        const ModelInfo &modelInfo = modelInfos.at(modelID_PickerEntities.first);
        size_t entityCount = modelID_PickerEntities.second.size();

        uint instanceOffset = perModelPickerInstanceOffset[modelID_PickerEntities.first];

        glDrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void *>(modelInfo.indexOffset * sizeof(uint32_t)), entityCount, modelInfo.vertexOffset, instanceOffset);
    }

    uint32_t value = -1;
    glReadPixels(0, 0, 1, 1, GL_RED_INTEGER, GL_UNSIGNED_INT, &value);

    //std::cout << "Value under mouse: " << value << std::endl;

    glDisable(GL_SCISSOR_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glViewport(0, 0, window.getFramebufferWidth(), window.getFramebufferHeight());

    if (value == -1 || value == 0) {
        return {};
    }

    EntityID entityID{};
    entityID.id = value;

    return entityID;
}

void MasterRenderer::setSelectionHighlightColour(glm::vec3 colour) {
    selectedHighlightColour = colour;
}

void MasterRenderer::setSelectionHighlightThickness(float thickness) {
    selectionThickness = thickness;
}

void MasterRenderer::setSelection(std::vector<EntityID> selected) {
    selectedEntities = std::move(selected);
}

void MasterRenderer::setGizmoEntities(std::vector<GizmoEntityData> entities, std::vector<GizmoEntityData> lineEntities) {
    gizmoEntities = std::move(entities);
    gizmoLineEntities = std::move(lineEntities);
}

void MasterRenderer::setProjectionMatrix(glm::mat4 mat) {
    projectionMatrix = mat;
}

void MasterRenderer::useCamera(const Camera &camera) {
    viewMatrix = camera.getViewMatrix();
    cameraPos = camera.getPosition();
}

glm::mat4 MasterRenderer::getCurrentProjectionMatrix() {
    return projectionMatrix;
}

glm::mat4 MasterRenderer::getCurrentViewMatrix() {
    return viewMatrix;
}

glm::vec3 MasterRenderer::getCurrentCameraPos() {
    return cameraPos;
}

float MasterRenderer::getVertFOV() {
    if (window.getFramebufferWidth() == 0 || window.getFramebufferHeight() == 0)
        return -1.0f;

    auto aspectRatio = (float) window.getFramebufferWidth() / (float) window.getFramebufferHeight();
    return aspectRatio < 1.0f ? 2.0f * atanf(tanf(cameraFOV / 2.0f) / aspectRatio) : cameraFOV;
}

ModelID MasterRenderer::loadModel(const std::string &file) {
    ModelID modelID = modelManager.loadModel(file);

    modelInfos.insert({modelID, ModelInfo{}});

    updateModelData();

    return modelID;
}

ModelID MasterRenderer::loadModel(std::vector<Vertex> vertices, std::vector<uint32_t> indices) {
    ModelID modelID = modelManager.loadModel(std::move(vertices), std::move(indices));

    modelInfos.insert({modelID, ModelInfo{}});

    updateModelData();

    return modelID;
}

ModelID MasterRenderer::loadModel(ModelData modelData, ModelMetaData modelMetaData) {
    ModelID modelID = modelManager.loadModel(std::move(modelData), modelMetaData);

    modelInfos.insert({modelID, ModelInfo{}});

    updateModelData();

    return modelID;
}

//TODO: Consider case of removing while 'in use'
void MasterRenderer::unloadModel(ModelID modelID) {
    if (!modelManager.modelExists(modelID)) {
        std::cerr << "Can not unloaded an unloaded model" << std::endl;
        exit(EXIT_FAILURE);
    }

    modelManager.unloadModel(modelID);
    modelInfos.erase(modelID);

    updateModelData();
}

const ModelMetaData &MasterRenderer::getModelMeta(ModelID modelId) {
    return modelManager.getModelMeta(modelId);
}

TextureID MasterRenderer::loadTexture(const std::string &file) {
    static const std::string TEXTURE_DIR = "res/models-textures";
    std::string fullFile = TEXTURE_DIR + "/" + file;

    int width, height;
    stbi_uc *data = stbi_load(fullFile.c_str(), &width, &height, nullptr, STBI_rgb);
    if (!data) {
        std::cerr << "Failed to load texture file: " << fullFile << "\n\t Reason: " << stbi_failure_reason() << std::endl;
        exit(EXIT_FAILURE);
    }

    uint glTexID;
    glGenTextures(1, &glTexID);
    glBindTexture(GL_TEXTURE_2D, glTexID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    stbi_image_free(data);

    TextureID textureID{};
    textureID.id = nextTextureID++;

    TextureInfo textureInfo{};
    textureInfo.glTextureID = glTexID;

    textureInfos.insert({textureID, textureInfo});

    return textureID;
}

//TODO: Consider case of removing while 'in use'
void MasterRenderer::unloadTexture(TextureID textureID) {
    if (textureInfos.count(textureID) == 0) {
        std::cerr << "Can not unloaded an unloaded texture" << std::endl;
        exit(EXIT_FAILURE);
    }

    glDeleteTextures(1, &textureInfos[textureID].glTextureID);
    textureInfos.erase(textureID);
}

EntityID MasterRenderer::addEntity(ModelID modelID, TextureID textureID, EntityData entityData, bool selectable) {
    if (!modelManager.modelExists(modelID)) {
        std::cerr << "Can not add an entity with an unloaded model" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (textureInfos.count(textureID) == 0) {
        std::cerr << "Can not add an entity with an unloaded texture" << std::endl;
        exit(EXIT_FAILURE);
    }

    ElementID dataID = perTexturePerModelEntityList[textureID][modelID].addData(entityData);

    EntityID entityID{};
    entityID.id = nextEntityID++;

    EntityInfo entityInfo{};
    entityInfo.modelID = modelID;
    entityInfo.textureID = textureID;
    entityInfo.dataID = dataID;
    entityInfo.selectable = selectable;

    entityInfoMap.insert({entityID, entityInfo});

    // Selection picking

    if (selectable) {
        PickerEntityData pickerData{
                entityData.modelMatrix,
                entityID.id
        };

        ElementID pickerDataID = perModelPickerEntityList[modelID].addData(pickerData);

        PickerEntityInfo pickerInfo{
                modelID,
                pickerDataID
        };

        pickerEntityInfoMap.insert({entityID, pickerInfo});
    }

    return entityID;
}

EntityID MasterRenderer::addFlatEntity(ModelID modelID, FlatEntityData entityData, bool selectable) {
    if (!modelManager.modelExists(modelID)) {
        std::cerr << "Can not add an entity with an unloaded model" << std::endl;
        exit(EXIT_FAILURE);
    }

    ElementID dataID = perModelFlatEntityList[modelID].addData(entityData);

    EntityID entityID{};
    entityID.id = nextEntityID++;

    FlatEntityInfo flatEntityInfo{};
    flatEntityInfo.modelID = modelID;
    flatEntityInfo.dataID = dataID;
    flatEntityInfo.selectable = selectable;

    flatEntityInfoMap.insert({entityID, flatEntityInfo});

    // Selection picking

    if (selectable) {
        PickerEntityData pickerData{
                entityData.modelMatrix,
                entityID.id
        };

        ElementID pickerDataID = perModelPickerEntityList[modelID].addData(pickerData);

        PickerEntityInfo pickerInfo{
                modelID,
                pickerDataID
        };

        pickerEntityInfoMap.insert({entityID, pickerInfo});
    }

    return entityID;
}

void MasterRenderer::removeEntity(EntityID entityID) {
    if (entityInfoMap.count(entityID) == 0 && flatEntityInfoMap.count(entityID) == 0) {
        std::cerr << "Can not remove an entity that is already removed" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (entityInfoMap.count(entityID) > 0) {
        EntityInfo info = entityInfoMap[entityID];

        perTexturePerModelEntityList[info.textureID][info.modelID].removeData(info.dataID);

        if (perTexturePerModelEntityList[info.textureID][info.modelID].size() == 0) {
            perTexturePerModelEntityList[info.textureID].erase(info.modelID);
            if (perTexturePerModelEntityList[info.textureID].empty()) {
                perTexturePerModelEntityList.erase(info.textureID);
            }
        }

        entityInfoMap.erase(entityID);
    } else {
        FlatEntityInfo info = flatEntityInfoMap[entityID];

        perModelFlatEntityList[info.modelID].removeData(info.dataID);

        if (perModelFlatEntityList[info.modelID].size() == 0) {
            perModelFlatEntityList.erase(info.modelID);
        }

        flatEntityInfoMap.erase(entityID);
    }

    if (pickerEntityInfoMap.count(entityID) != 0) {

        PickerEntityInfo pickerEntityInfo = pickerEntityInfoMap.at(entityID);
        pickerEntityInfoMap.erase(entityID);

        perModelPickerEntityList[pickerEntityInfo.modelID].removeData(pickerEntityInfo.dataID);

        if (perModelPickerEntityList[pickerEntityInfo.modelID].size() == 0) {
            perModelPickerEntityList.erase(pickerEntityInfo.modelID);
        }
    }
}

LightID MasterRenderer::addLight(LightVariant lightData) {
    LightID lightID{};
    lightID.id = nextLightID++;

    LightInfo lightInfo{};
    lightInfo.lightType = LIGHT_INDEX_TYPE[lightData.index()];

    switch (lightInfo.lightType) {
        case PointLight:
            lightInfo.dataID = pointLights.addData(std::get<PointLightData>(lightData));
            break;
        case DirectionalLight:
            lightInfo.dataID = directionalLights.addData(std::get<DirectionalLightData>(lightData));
            break;
        case ConeLight:
            lightInfo.dataID = coneLights.addData(std::get<ConeLightData>(lightData));
            break;
    }

    lightInfos.insert({lightID, lightInfo});

    return lightID;
}

void MasterRenderer::removeLight(LightID lightID) {
    if (lightInfos.count(lightID) == 0) {
        std::cerr << "Can not remove a light that is already removed" << std::endl;
        exit(EXIT_FAILURE);
    }

    const LightInfo &info = lightInfos[lightID];

    switch (info.lightType) {
        case PointLight:
            pointLights.removeData(info.dataID);
            break;
        case DirectionalLight:
            directionalLights.removeData(info.dataID);
            break;
        case ConeLight:
            coneLights.removeData(info.dataID);
            break;
    }

    lightInfos.erase(lightID);
}

void MasterRenderer::changeEntityModel(EntityID entityID, ModelID modelID) {
    if (entityInfoMap.count(entityID) == 0 && flatEntityInfoMap.count(entityID) == 0) {
        std::cerr << "Can not change model of entity that is removed" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (!modelManager.modelExists(modelID)) {
        std::cerr << "Can not change model of entity to unloaded model" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (entityInfoMap.count(entityID) > 0) {
        EntityInfo &info = entityInfoMap[entityID];
        EntityData entityData = perTexturePerModelEntityList[info.textureID][info.modelID].removeData(info.dataID);

        ModelID oldModelID = info.modelID;
        info.modelID = modelID;
        info.dataID = perTexturePerModelEntityList[info.textureID][info.modelID].addData(entityData);

        if (perTexturePerModelEntityList[info.textureID][oldModelID].size() == 0) {
            perTexturePerModelEntityList[info.textureID].erase(oldModelID);
            if (perTexturePerModelEntityList[info.textureID].empty()) {
                perTexturePerModelEntityList.erase(info.textureID);
            }
        }
    } else {
        FlatEntityInfo &info = flatEntityInfoMap[entityID];
        FlatEntityData entityData = perModelFlatEntityList[info.modelID].removeData(info.dataID);

        ModelID oldModelID = info.modelID;
        info.modelID = modelID;
        info.dataID = perModelFlatEntityList[info.modelID].addData(entityData);

        if (perModelFlatEntityList[info.modelID].size() == 0) {
            perModelFlatEntityList.erase(info.modelID);
        }
    }

    if (pickerEntityInfoMap.count(entityID) != 0) {

        PickerEntityInfo& pickerEntityInfo = pickerEntityInfoMap.at(entityID);

        PickerEntityData data = perModelPickerEntityList[pickerEntityInfo.modelID].removeData(pickerEntityInfo.dataID);

        if (perModelPickerEntityList[pickerEntityInfo.modelID].size() == 0) {
            perModelPickerEntityList.erase(pickerEntityInfo.modelID);
        }

        pickerEntityInfo.modelID = modelID;
        pickerEntityInfo.dataID = perModelPickerEntityList[pickerEntityInfo.modelID].addData(data);
    }
}

void MasterRenderer::changeEntityTexture(EntityID entityID, TextureID textureID) {
    if (entityInfoMap.count(entityID) == 0) {
        std::cerr << "Can not change model of entity that is removed" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (textureInfos.count(textureID) == 0) {
        std::cerr << "Can not change texture of entity to unloaded texture" << std::endl;
        exit(EXIT_FAILURE);
    }

    EntityInfo &info = entityInfoMap[entityID];
    EntityData entityData = perTexturePerModelEntityList[info.textureID][info.modelID].removeData(info.dataID);

    TextureID oldTextureID = info.textureID;
    info.textureID = textureID;
    info.dataID = perTexturePerModelEntityList[info.textureID][info.modelID].addData(entityData);

    if (perTexturePerModelEntityList[oldTextureID][info.modelID].size() == 0) {
        perTexturePerModelEntityList[oldTextureID].erase(info.modelID);
        if (perTexturePerModelEntityList[oldTextureID].empty()) {
            perTexturePerModelEntityList.erase(oldTextureID);
        }
    }
}

EntityData &MasterRenderer::getEntity(EntityID entityID) {
    if (entityInfoMap.count(entityID) == 0) {
        std::cerr << "Can not get removed entity" << std::endl;
        exit(EXIT_FAILURE);
    }

    EntityInfo info = entityInfoMap[entityID];
    return perTexturePerModelEntityList[info.textureID][info.modelID].element(info.dataID);
}

FlatEntityData &MasterRenderer::getFlatEntity(EntityID entityID) {
    if (flatEntityInfoMap.count(entityID) == 0) {
        std::cerr << "Can not get removed entity" << std::endl;
        exit(EXIT_FAILURE);
    }

    FlatEntityInfo info = flatEntityInfoMap[entityID];
    return perModelFlatEntityList[info.modelID].element(info.dataID);
}

void MasterRenderer::setEntityModelMatrix(EntityID entityID, glm::mat4 modelMatrix) {
    if (entityInfoMap.count(entityID) > 0) {
        const EntityInfo &entityInfo = entityInfoMap[entityID];

        EntityData &entityData = perTexturePerModelEntityList[entityInfo.textureID][entityInfo.modelID].element(entityInfo.dataID);
        entityData.setModelMatrix(modelMatrix);

        if (entityInfo.selectable) {
            PickerEntityInfo &pickerInfo = pickerEntityInfoMap[entityID];

            perModelPickerEntityList[pickerInfo.modelID].element(pickerInfo.dataID).modelMatrix = modelMatrix;
        }
    } else if (flatEntityInfoMap.count(entityID) > 0) {
        const FlatEntityInfo &entityInfo = flatEntityInfoMap[entityID];

        FlatEntityData &entityData = perModelFlatEntityList[entityInfo.modelID].element(entityInfo.dataID);
        entityData.setModelMatrix(modelMatrix);

        if (entityInfo.selectable) {
            PickerEntityInfo &pickerInfo = pickerEntityInfoMap[entityID];

            perModelPickerEntityList[pickerInfo.modelID].element(pickerInfo.dataID).modelMatrix = modelMatrix;
        }
    } else {
        std::cerr << "Can not set model matrix for removed entity" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void MasterRenderer::changeLightType(LightID lightID, LightVariant lightData) {
    if (lightInfos.count(lightID) == 0) {
        std::cerr << "Can not change light that is removed" << std::endl;
        exit(EXIT_FAILURE);
    }

    LightInfo &lightInfo = lightInfos[lightID];

    switch (lightInfo.lightType) {
        case PointLight:
            pointLights.removeData(lightInfo.dataID);
            break;
        case DirectionalLight:
            directionalLights.removeData(lightInfo.dataID);
            break;
        case ConeLight:
            coneLights.removeData(lightInfo.dataID);
            break;
    }

    lightInfo.lightType = LIGHT_INDEX_TYPE[lightData.index()];

    switch (lightInfo.lightType) {
        case PointLight:
            lightInfo.dataID = pointLights.addData(std::get<PointLightData>(lightData));
            break;
        case DirectionalLight:
            lightInfo.dataID = directionalLights.addData(std::get<DirectionalLightData>(lightData));
            break;
        case ConeLight:
            lightInfo.dataID = coneLights.addData(std::get<ConeLightData>(lightData));
            break;
    }
}

LightVariantRef MasterRenderer::getLight(LightID lightID) {
    if (lightInfos.count(lightID) == 0) {
        std::cerr << "Can not get light that is removed" << std::endl;
        exit(EXIT_FAILURE);
    }

    const LightInfo &lightInfo = lightInfos[lightID];

    switch (lightInfo.lightType) {
        case PointLight:
            return &pointLights.element(lightInfo.dataID);
        case DirectionalLight:
            return &directionalLights.element(lightInfo.dataID);
        case ConeLight:
            return &coneLights.element(lightInfo.dataID);
    }

    return (PointLightData *) nullptr;
}

void MasterRenderer::updateModelData() {
    std::vector<Vertex> vertexData{};
    std::vector<uint32_t> indexData{};

    uint vertexOffset = 0;
    uint indexOffset = 0;
    for (auto &idInfo : modelInfos) {
        auto &currentModel = modelManager.getModel(idInfo.first);

        vertexData.insert(vertexData.end(), currentModel.vertices.begin(), currentModel.vertices.end());
        indexData.insert(indexData.end(), currentModel.indices.begin(), currentModel.indices.end());

        idInfo.second.vertexOffset = vertexOffset;
        idInfo.second.indexOffset = indexOffset;

        vertexOffset = vertexData.size();
        indexOffset = indexData.size();
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVBO);
    glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);

    glBufferData(GL_ARRAY_BUFFER, vertexData.size() * sizeof(Vertex), vertexData.data(), GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexData.size() * sizeof(uint32_t), indexData.data(), GL_STATIC_DRAW);
}

void MasterRenderer::updateEntityData() {
    std::vector<EntityData> entityData{};
    perTexturePerModelInstanceOffset.clear();

    uint perInstanceOffset = 0;
    for (const auto &texturePerModelEntityList : perTexturePerModelEntityList) {
        const auto &perModelEntityList = texturePerModelEntityList.second;

        for (auto &modelIDDataVec : perModelEntityList) {
            entityData.insert(entityData.end(), modelIDDataVec.second.data(), modelIDDataVec.second.data() + modelIDDataVec.second.size());

            perTexturePerModelInstanceOffset[texturePerModelEntityList.first][modelIDDataVec.first] = perInstanceOffset;

            perInstanceOffset = entityData.size();
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, perInstanceVBO);
    glBufferData(GL_ARRAY_BUFFER, entityData.size() * sizeof(EntityData), entityData.data(), GL_STREAM_DRAW);

    std::vector<FlatEntityData> flatEntityData{};
    perModelFlatInstanceOffset.clear();

    perInstanceOffset = 0;
    for (auto &modelIDDataVec : perModelFlatEntityList) {
        flatEntityData.insert(flatEntityData.end(), modelIDDataVec.second.data(), modelIDDataVec.second.data() + modelIDDataVec.second.size());

        perModelFlatInstanceOffset[modelIDDataVec.first] = perInstanceOffset;

        perInstanceOffset = flatEntityData.size();
    }

    glBindBuffer(GL_ARRAY_BUFFER, perFlatInstanceVBO);
    glBufferData(GL_ARRAY_BUFFER, flatEntityData.size() * sizeof(FlatEntityData), flatEntityData.data(), GL_STREAM_DRAW);

    std::vector<PickerEntityData> pickerEntityData{};
    perModelPickerInstanceOffset.clear();

    perInstanceOffset = 0;
    for (auto &modelID_DataVec: perModelPickerEntityList) {
        pickerEntityData.insert(pickerEntityData.end(), modelID_DataVec.second.data(), modelID_DataVec.second.data() + modelID_DataVec.second.size());

        perModelPickerInstanceOffset[modelID_DataVec.first] = perInstanceOffset;

        perInstanceOffset = pickerEntityData.size();
    }

    glBindBuffer(GL_ARRAY_BUFFER, perPickerInstanceVBO);
    glBufferData(GL_ARRAY_BUFFER, pickerEntityData.size() * sizeof(PickerEntityData), pickerEntityData.data(), GL_STREAM_DRAW);
}

#define ALIGN(x, y) {auto mod = x % y; if (mod != 0) { x += (y - mod); }}

void MasterRenderer::updateLightData() {
    int align;
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &align);
//    std::cout << "UNIFORM_BUFFER_OFFSET_ALIGNMENT: " << align << std::endl;

    size_t pointLightOffset = 0;
    ALIGN(pointLightOffset, align);
//    std::cout << "pointLightOffset: " << pointLightOffset << std::endl;
    size_t pointLightSize = sizeof(PointLightData) * pointLights.size();

    size_t directionalLightOffset = pointLightSize;
    ALIGN(directionalLightOffset, align);
//    std::cout << "directionalLightOffset: " << directionalLightOffset << std::endl;
    size_t directionalLightSize = sizeof(DirectionalLightData) * directionalLights.size();

    size_t coneLightOffset = directionalLightOffset + directionalLightSize;
    ALIGN(coneLightOffset, align);
//    std::cout << "coneLightOffset: " << coneLightOffset << std::endl;
    size_t coneLightSize = sizeof(ConeLightData) * coneLights.size();

    size_t totalSize = coneLightOffset + coneLightSize;

    glBindBuffer(GL_UNIFORM_BUFFER, lightDataUB);

    if (totalSize > 0) {
        glBufferData(GL_UNIFORM_BUFFER, totalSize, nullptr, GL_DYNAMIC_DRAW);

        if (pointLightSize > 0) {
            glBufferSubData(GL_UNIFORM_BUFFER, pointLightOffset, pointLightSize, pointLights.data());
            glBindBufferRange(GL_UNIFORM_BUFFER, 0, lightDataUB, pointLightOffset, pointLightSize);
        }

        if (directionalLightSize > 0) {
            glBufferSubData(GL_UNIFORM_BUFFER, directionalLightOffset, directionalLightSize, directionalLights.data());
            glBindBufferRange(GL_UNIFORM_BUFFER, 1, lightDataUB, directionalLightOffset, directionalLightSize);
        }

        if (coneLightSize > 0) {
            glBufferSubData(GL_UNIFORM_BUFFER, coneLightOffset, coneLightSize, coneLights.data());
            glBindBufferRange(GL_UNIFORM_BUFFER, 2, lightDataUB, coneLightOffset, coneLightSize);
        }
    }
}

void MasterRenderer::updateShader() {
    static size_t lastPointLightCount = 0;
    static size_t lastDirectionalLightCount = 0;
    static size_t lastConeLightCount = 0;

    if (pointLights.size() != lastPointLightCount
        || directionalLights.size() != lastDirectionalLightCount
        || coneLights.size() != lastConeLightCount) {

        lastPointLightCount = pointLights.size();
        lastDirectionalLightCount = directionalLights.size();
        lastConeLightCount = coneLights.size();

        shader.recompile(
                {},
                {
                        {"NUM_PL", std::to_string(pointLights.size())},
                        {"NUM_DL", std::to_string(directionalLights.size())},
                        {"NUM_CL", std::to_string(coneLights.size())},
                }
        );
    }
}

void MasterRenderer::cleanup() {
    glDeleteBuffers(1, &vertexVBO);
    glDeleteBuffers(1, &indexVBO);
    glDeleteBuffers(1, &perInstanceVBO);

    glDeleteVertexArrays(1, &VAO);

    shader.cleanup();
}

std::size_t std::hash<EntityID>::operator()(const EntityID &k) const {
    using std::hash;

    return hash<uint>()(k.id);
}

std::size_t std::hash<TextureID>::operator()(const TextureID &k) const {
    using std::hash;

    return hash<uint>()(k.id);
}

std::size_t std::hash<LightID>::operator()(const LightID &k) const {
    using std::hash;

    return hash<uint>()(k.id);
}
