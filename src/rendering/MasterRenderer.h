#ifndef MASTER_RENDERER_H
#define MASTER_RENDERER_H

#include "Model.h"
#include "shaders/BasicShader.h"
#include "../utility/IdVec.h"
#include "../Camera.h"
#include "shaders/OutlineShader.h"
#include "shaders/FlatShader.h"
#include "shaders/GizmoShader.h"
#include "shaders/PickerShader.h"

#include <unordered_map>
#include <unordered_set>
#include <variant>
#include <optional>

#include <glm/glm.hpp>

class MasterRenderer;

struct EntityID;
struct TextureID;
struct LightID;

namespace std {
    template<>
    struct hash<EntityID> {
        std::size_t operator()(const EntityID &k) const;
    };

    template<>
    struct hash<TextureID> {
        std::size_t operator()(const TextureID &k) const;
    };

    template<>
    struct hash<LightID> {
        std::size_t operator()(const LightID &k) const;
    };
}

struct EntityID {
    bool operator==(const EntityID &other) const {
        return id == other.id;
    }

    bool operator<(const EntityID &rhs) const {
        return id < rhs.id;
    }

private:
    uint id;

    friend class MasterRenderer;

    friend std::size_t std::hash<EntityID>::operator()(const EntityID &k) const;
};

struct TextureID {
    bool operator==(const TextureID &other) const {
        return id == other.id;
    }

private:
    uint id;

    friend class MasterRenderer;

    friend std::size_t std::hash<TextureID>::operator()(const TextureID &k) const;
};

struct LightID {
    bool operator==(const LightID &other) const {
        return id == other.id;
    }

private:
    uint id;

    friend class MasterRenderer;

    friend std::size_t std::hash<LightID>::operator()(const LightID &k) const;
};

struct EntityData {
private:
    glm::mat4 modelMatrix;
    glm::mat3 normalMatrix;

    friend class MasterRenderer;

public:
    union {
        float materialProperties[4] = {1.0f, 1.0f, 0.1f, 32.0f};
        struct {
            float diffuseFactor;
            float specularFactor;
            float globalFactor;
            float shininessExponent;
        };
    };
    glm::vec4 tintColour_TexScale = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

    explicit EntityData(glm::mat4 modelMatrix);
    EntityData(const glm::mat4 &modelMatrix, const glm::mat3 &normalMatrix,
            float diffuseFactor, float specularFactor, float globalFactor, float shininessExponent, const glm::vec3 &tintColour, float textureScale);
    static EntityData identity();

    const glm::mat4 &getModelMatrix() const;
private:
    void setModelMatrix(glm::mat4 modelMat);
    void calcNormalMatrix();
};

struct FlatEntityData {
private:
    glm::mat4 modelMatrix;
    glm::mat3 normalMatrix;

    friend class MasterRenderer;
public:
    glm::vec4 colour = glm::vec4{1.0f};

    explicit FlatEntityData(glm::mat4 modelMatrix);
    FlatEntityData(glm::mat4 modelMatrix, glm::vec4 colour);

    static FlatEntityData identity();

    const glm::mat4 &getModelMatrix() const;
private:
    void setModelMatrix(glm::mat4 modelMat);
    void calcNormalMatrix();
};

struct GizmoEntityData {
    ModelID modelID;

    glm::mat4 modelMatrix;
    glm::vec4 colour;

    static GizmoEntityData identity();
};

// alignas used to conform to std140 for direction binary usage with glsl
struct PointLightData {
    alignas(16) glm::vec3 position;
    alignas(16) glm::vec3 colour;
    alignas(16) glm::vec3 attenuation;
};

// alignas used to conform to std140 for direction binary usage with glsl
struct DirectionalLightData {
    alignas(16) glm::vec3 direction;
    alignas(16) glm::vec3 colour;
};

// alignas used to conform to std140 for direction binary usage with glsl
struct ConeLightData {
    alignas(16) glm::vec4 position_Softness;
    alignas(16) glm::vec4 direction_FOV;
    alignas(16) glm::vec3 colour;
    alignas(16) glm::vec3 attenuation;
};

enum LightType: int {
    PointLight = 0,
    DirectionalLight = 1,
    ConeLight = 2
};

typedef std::variant<PointLightData, DirectionalLightData, ConeLightData> LightVariant;
typedef std::variant<PointLightData *, DirectionalLightData *, ConeLightData *> LightVariantRef;
static const LightType LIGHT_INDEX_TYPE[] = {LightType::PointLight, LightType::DirectionalLight, LightType::ConeLight};

class MasterRenderer {
    bool enableBackFaceCulling = true;
    bool enableWireframes = false;

    ModelManager modelManager{};

    uint vertexVBO = 0;
    uint indexVBO = 0;
    uint perInstanceVBO = 0;
    uint perFlatInstanceVBO = 0;
    uint perPickerInstanceVBO = 0;

    uint VAO = 0;
    uint flatVAO = 0;
    uint pickerVAO = 0;

    uint lightDataUB = 0;

    uint pickerFBO = 0;
    uint pickerFBO_colour_Texture = 0;
    uint pickerFBO_depthStencil_RBO = 0;

    struct ModelInfo {
        uint vertexOffset = 0;
        uint indexOffset = 0;
    };

    std::unordered_map<TextureID, std::unordered_map<ModelID, uint>> perTexturePerModelInstanceOffset{};
    std::unordered_map<ModelID, uint> perModelFlatInstanceOffset{};

    uint nextTextureID = 0;
    std::unordered_map<ModelID, ModelInfo> modelInfos{};

    struct TextureInfo {
        uint glTextureID = 0;
    };

    std::unordered_map<TextureID, TextureInfo> textureInfos{};

    struct EntityInfo {
        ModelID modelID;
        TextureID textureID;
        ElementID dataID;
        bool selectable;
    };

    struct FlatEntityInfo {
        ModelID modelID;
        ElementID dataID;
        bool selectable;
    };

    uint nextEntityID = 1;
    std::unordered_map<EntityID, EntityInfo> entityInfoMap{};
    std::unordered_map<EntityID, FlatEntityInfo> flatEntityInfoMap{};

    std::unordered_map<TextureID, std::unordered_map<ModelID, IDVec<EntityData>>> perTexturePerModelEntityList{};
    std::unordered_map<ModelID, IDVec<FlatEntityData>> perModelFlatEntityList{};

    struct PickerEntityData {
        glm::mat4 modelMatrix;
        uint32_t entityID;
    };

    struct PickerEntityInfo {
        ModelID modelID;
        ElementID dataID;
    };

    std::unordered_map<EntityID, PickerEntityInfo> pickerEntityInfoMap{};
    std::unordered_map<ModelID, IDVec<PickerEntityData>> perModelPickerEntityList{};
    std::unordered_map<ModelID, uint> perModelPickerInstanceOffset{};

    struct LightInfo {
        LightType lightType;
        ElementID dataID;
    };

    uint nextLightID = 0;
    std::unordered_map<LightID, LightInfo> lightInfos{};

    IDVec<PointLightData> pointLights{};
    IDVec<DirectionalLightData> directionalLights{};
    IDVec<ConeLightData> coneLights{};

    glm::mat4 projectionMatrix{1.0f};
    glm::mat4 viewMatrix{1.0f};
    glm::vec3 cameraPos{0.0f};
    float cameraFOV = glm::radians(90.0f);

    glm::vec3 selectedHighlightColour{1.0f};
    float selectionThickness = 0.1f;
    std::vector<EntityID> selectedEntities{};

    std::vector<GizmoEntityData> gizmoEntities{};
    std::vector<GizmoEntityData> gizmoLineEntities{};

    Window window;

    BasicShader shader{};
    OutlineShader outlineShader{};
    FlatShader flatShader{};
    GizmoShader gizmoShader{};
    PickerShader pickerShader{};
public:
    static const float NEAR;
    static const float FAR;

    explicit MasterRenderer(const Window& window);

    void update();
    void render();

    void addImGuiSection(WindowManager& windowManager);

    std::optional<EntityID> getPickedEntity();

    void setSelectionHighlightColour(glm::vec3 colour);
    void setSelectionHighlightThickness(float thickness);
    void setSelection(std::vector<EntityID> selected);

    void setGizmoEntities(std::vector<GizmoEntityData> entities, std::vector<GizmoEntityData> lineEntities = {});

    void setProjectionMatrix(glm::mat4 mat);
    void useCamera(const Camera &camera);

    glm::mat4 getCurrentProjectionMatrix();
    glm::mat4 getCurrentViewMatrix();
    glm::vec3 getCurrentCameraPos();
    float getVertFOV();

    ModelID loadModel(const std::string &file);
    ModelID loadModel(std::vector<Vertex> vertices, std::vector<uint32_t> indices);
    ModelID loadModel(ModelData modelData, ModelMetaData modelMetaData);
    void unloadModel(ModelID modelID);

    const ModelMetaData& getModelMeta(ModelID modelId);

    TextureID loadTexture(const std::string &file);
    void unloadTexture(TextureID textureID);

    EntityID addEntity(ModelID modelID, TextureID textureID, EntityData entityData, bool selectable = true);
    EntityID addFlatEntity(ModelID modelID, FlatEntityData entityData, bool selectable = true);
    void removeEntity(EntityID entityID);

    LightID addLight(LightVariant lightData);
    void removeLight(LightID lightID);

    void changeEntityModel(EntityID entityID, ModelID modelID);
    void changeEntityTexture(EntityID entityID, TextureID textureID);
    EntityData &getEntity(EntityID entityID);
    FlatEntityData &getFlatEntity(EntityID entityID);

    void setEntityModelMatrix(EntityID entityID, glm::mat4 modelMatrix);

    void changeLightType(LightID lightID, LightVariant lightData);
    LightVariantRef getLight(LightID lightID);

    void cleanup();
private:
    void updateModelData();
    void updateEntityData();
    void updateLightData();
    void updateShader();
};

#endif //MASTER_RENDERER_H
