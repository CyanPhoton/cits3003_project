#ifndef WINDOW_MANAGER_H
#define WINDOW_MANAGER_H

#include <iostream>
#include <set>
#include <memory>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm/vec2.hpp>

class Window;

class WindowManager {
    std::set<Window> windows;

    float dt = 1.0f/60.0f;

    bool vSyncEnabled = false;
public:
    WindowManager();

    Window createWindow(std::string name, glm::ivec2 size);
    void destroyWindow(const Window& window);

    void setVSync(bool value);
    bool getVSync() const;

    void update();

    float getDeltaTime();

    void cleanup();
};

class Window {
    friend class WindowManager;

public:
    bool operator==(const Window &rhs) const;
    bool operator<(const Window &rhs) const;
    bool operator>(const Window &rhs) const;
    bool operator<=(const Window &rhs) const;
    bool operator>=(const Window &rhs) const;
    bool operator!=(const Window &rhs) const;
private:
    GLFWwindow* window = nullptr;

    struct WindowData {
        static const size_t DELTA_ARRAY_COUNT = (GLFW_MOUSE_BUTTON_LAST - GLFW_MOUSE_BUTTON_1 + 2);

        glm::dvec2 currentMotionDeltas[DELTA_ARRAY_COUNT];
        glm::dvec2 motionDeltas[DELTA_ARRAY_COUNT];

        static const size_t PRESSED_KEY_COUNT = GLFW_KEY_LAST + 1;

        bool currentPressedKeys[PRESSED_KEY_COUNT];
        bool pressedKeys[PRESSED_KEY_COUNT];

        float currentScrollDelta = 0.0f;
        float scrollDelta;
    };

    std::shared_ptr<WindowData> windowData = nullptr;
public:
    void makeContextCurrent();
    void swapBuffers();

    bool shouldClose() const;
    bool isFocused() const;
    void focus();

    glm::dvec2 getMouseMotion(int button) const;
    glm::dvec2 getMousePos() const;
    glm::vec2 getMousePosNDC() const;

    float getScrollDelta() const;

    bool isKeyPressed(int key) const;
    bool wasKeyPressed(int key) const;

    bool isMousePressed(int button) const;

    glm::ivec2 getWindowSize() const;
    uint32_t getWindowWidth() const;
    uint32_t getWindowHeight() const;

    glm::ivec2 getFramebufferSize() const;
    uint32_t getFramebufferWidth() const;
    uint32_t getFramebufferHeight() const;

    GLFWwindow* internal();
};

#endif //WINDOW_MANAGER_H
