#include "WindowManager.h"

#include <memory.h>
#include <imgui/imgui.h>

#include "../utility/OpenGL.h"

void glfwErrorCallback(int code, const char *msg) {
    std::cout << "GLFW Error (" << code << ")\n\t" << "msg: " << msg << std::endl;
}

WindowManager::WindowManager() {
    glfwSetErrorCallback(glfwErrorCallback);
    glfwInit();
}

Window WindowManager::createWindow(std::string name, glm::ivec2 size) {
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OpenGL::VERSION_MAJOR);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OpenGL::VERSION_MINOR);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif // __APPLE__
    glfwWindowHint(GLFW_SAMPLES, 8);
    auto glfwWindow = glfwCreateWindow(size.x, size.y, name.c_str(), nullptr, nullptr);

    glfwSetCursorPosCallback(glfwWindow, [](GLFWwindow *window, double xpos, double ypos) {
        static glm::dvec2 lastPos = {xpos, ypos};
        if (!ImGui::GetIO().WantCaptureMouse) {
            glm::dvec2 delta = glm::dvec2(xpos, ypos) - lastPos;

            auto windowData = reinterpret_cast<Window::WindowData *>(glfwGetWindowUserPointer(window));

            windowData->currentMotionDeltas[0] += delta;

            for (int i = 1; i < Window::WindowData::DELTA_ARRAY_COUNT; ++i) {
                if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1 + i - 1) == GLFW_PRESS) {
                    windowData->currentMotionDeltas[i] += delta;
                }
            }
        }
        lastPos = {xpos, ypos};
    });

    glfwSetKeyCallback(glfwWindow, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
        auto windowData = reinterpret_cast<Window::WindowData*>(glfwGetWindowUserPointer(window));

        if (!ImGui::GetIO().WantCaptureKeyboard) {
            if (action == GLFW_PRESS) {
                windowData->currentPressedKeys[key] = true;
            }
        }
    });

    glfwSetScrollCallback(glfwWindow, [](GLFWwindow* window, double dx, double dy) {
        auto windowData = reinterpret_cast<Window::WindowData*>(glfwGetWindowUserPointer(window));

        if (!ImGui::GetIO().WantCaptureMouse) {
            windowData->currentScrollDelta += (float) dy;
        }
    });

    Window window{};
    window.window = glfwWindow;
    window.windowData = std::make_shared<Window::WindowData>();

    glfwSetWindowUserPointer(glfwWindow, &*window.windowData);

    windows.insert(window);
    return window;
}

void WindowManager::destroyWindow(const Window &window) {
    glfwDestroyWindow(window.window);
    windows.erase(window);
}

void WindowManager::setVSync(bool value) {
    glfwSwapInterval(value ? 1 : 0);
    vSyncEnabled = value;
}

bool WindowManager::getVSync() const {
    return vSyncEnabled;
}

void WindowManager::update() {
    static double lastTime = 0.0;
    double time = glfwGetTime();
    if (lastTime != 0.0) {
        dt = (float) time - lastTime;
    }
    lastTime = time;

    for (const auto & window : windows) {
        for (int i = 0; i < Window::WindowData::DELTA_ARRAY_COUNT; ++i) {
            window.windowData->motionDeltas[i] = window.windowData->currentMotionDeltas[i];
            window.windowData->currentMotionDeltas[i] = {0.0, 0.0};
        }

        memcpy(window.windowData->pressedKeys, window.windowData->currentPressedKeys, sizeof(bool) * Window::WindowData::PRESSED_KEY_COUNT);
        memset(window.windowData->currentPressedKeys, 0, sizeof(bool) * Window::WindowData::PRESSED_KEY_COUNT);

        window.windowData->scrollDelta = window.windowData->currentScrollDelta;
        window.windowData->currentScrollDelta = 0.0f;
    }

    glfwPollEvents();
}

float WindowManager::getDeltaTime() {
    return dt;
}

void WindowManager::cleanup() {
    glfwTerminate();
}

void Window::makeContextCurrent() {
    glfwMakeContextCurrent(window);
}

void Window::swapBuffers() {
    glfwSwapBuffers(window);
}

bool Window::shouldClose() const {
    return glfwWindowShouldClose(window);
}

bool Window::isFocused() const {
    return glfwGetWindowAttrib(window, GLFW_FOCUSED) == GLFW_TRUE;
}

void Window::focus() {
    glfwFocusWindow(window);
}

glm::dvec2 Window::getMouseMotion(int button = 0) const {
    return reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window))->motionDeltas[button + 1];
}

glm::dvec2 Window::getMousePos() const {
    glm::dvec2 position{};
    glfwGetCursorPos(window, &position.x, &position.y);
    return position;
}

glm::vec2 Window::getMousePosNDC() const {
    glm::dvec2 screenPos = getMousePos();

    glm::vec2 NDC = {
            ((float) screenPos.x / (float) getWindowWidth()) * 2.0f - 1.0f,
            -(((float) screenPos.y / (float) getWindowHeight()) * 2.0f - 1.0f),
    };

    return NDC;
}


float Window::getScrollDelta() const {
    return reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window))->scrollDelta;
}

bool Window::isKeyPressed(int key) const {
    return !ImGui::GetIO().WantCaptureKeyboard && glfwGetKey(window, key) == GLFW_PRESS;
}

bool Window::wasKeyPressed(int key) const {
    return reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window))->pressedKeys[key];
}

bool Window::isMousePressed(int button) const {
    return !ImGui::GetIO().WantCaptureMouse && glfwGetMouseButton(window, button) == GLFW_PRESS;
}

glm::ivec2 Window::getWindowSize() const {
    glm::ivec2 size;
    glfwGetWindowSize(window, &size.x, &size.y);
    return size;
}

uint32_t Window::getWindowWidth() const {
    return getWindowSize().x;
}

uint32_t Window::getWindowHeight() const {
    return getWindowSize().y;
}

glm::ivec2 Window::getFramebufferSize() const {
    glm::ivec2 size;
    glfwGetFramebufferSize(window, &size.x, &size.y);
    return size;
}

uint32_t Window::getFramebufferWidth() const {
    return getFramebufferSize().x;
}

uint32_t Window::getFramebufferHeight() const {
    return getFramebufferSize().y;
}

GLFWwindow *Window::internal() {
    return window;
}

bool Window::operator==(const Window &rhs) const {
    return window == rhs.window;
}

bool Window::operator!=(const Window &rhs) const {
    return !(rhs == *this);
}

bool Window::operator<(const Window &rhs) const {
    return window < rhs.window;
}

bool Window::operator>(const Window &rhs) const {
    return rhs < *this;
}

bool Window::operator<=(const Window &rhs) const {
    return !(rhs < *this);
}

bool Window::operator>=(const Window &rhs) const {
    return !(*this < rhs);
}
