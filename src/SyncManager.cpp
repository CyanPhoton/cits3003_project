#include "SyncManager.h"

#include "rendering/imgui/ImGuiManager.h"

#include <algorithm>

SyncManager::SyncManager() {
    frameTimes.reserve(FRAME_COUNT);
    frameTimes.push_back(0.0f); // To prevent some issue
}

void SyncManager::addImGuiSection(float frameDelta) {
    if (frameTimes.size() >= FRAME_COUNT) {
        frameTimes.erase(frameTimes.begin());
    }
    frameTimes.push_back(frameDelta);

    if (ImGui::CollapsingHeader("Performance Metrics")) {
        size_t display_count = std::min(frameTimes.size(), FRAME_DISPLAY_COUNT);
        size_t display_offset = frameTimes.size() - display_count;
        std::cout << "Offset: " << display_offset << std::endl;

        ImGui::PlotLines("Frame Times (ms)", [](void* data, int index){ return ((float*) data)[index] * 1000.0f; }, frameTimes.data() + display_offset, display_count, 0, nullptr, FLT_MAX, FLT_MAX, ImVec2(0,40));

        float averageTime = 0.0f;
        float minTime = frameTimes[0];
        float maxTime = frameTimes[0];

        for (float val : frameTimes) {
            averageTime += val;
            minTime = std::min(minTime, val);
            maxTime = std::max(maxTime, val);
        }

        averageTime /= frameTimes.size();

        ImGui::Text("Average Frame time: %.3f ms", averageTime * 1000.0f);
        ImGui::Text("Average Effective FPS: %.3f", 1.0f / averageTime);
        ImGui::Text("Min Frame time: %.3f ms", minTime * 1000.0f);
        ImGui::Text("Max Frame time: %.3f ms", maxTime * 1000.0f);
    }
}
