#include "Camera.h"
#include "utility/Defs.h"

#include <cmath>

#include <glm/gtx/transform.hpp>

#include <imgui/imgui.h>

Camera::Camera(float distance, glm::vec3 focusPoint, float pitch, float yaw)
    : distance(distance), initDistance(distance), focusPoint(focusPoint), initFocusPoint(focusPoint), pitch(pitch), initPitch(pitch), yaw(yaw), initYaw(yaw) {}

void Camera::update(const Window& window, float dt, bool controlsEnabled) {
    if (controlsEnabled) {
        bool resetSeq = false;
        if (window.wasKeyPressed(GLFW_KEY_R)) {
            reset();
            resetSeq = true;
        }

        if (!resetSeq) {
            if (window.isKeyPressed(GLFW_KEY_LEFT_CONTROL) || window.isKeyPressed(GLFW_KEY_RIGHT_CONTROL)) {
//            yaw -= YAW_SPEED * (float) window.getMouseMotion(GLFW_MOUSE_BUTTON_MIDDLE).x;
                distance += ZOOM_SPEED * (float) window.getMouseMotion(GLFW_MOUSE_BUTTON_MIDDLE).y;
            } else {
                pitch += PITCH_SPEED * (float) window.getMouseMotion(GLFW_MOUSE_BUTTON_MIDDLE).y;
            }

            yaw -= YAW_SPEED * (float) window.getMouseMotion(GLFW_MOUSE_BUTTON_MIDDLE).x;

            distance -= ZOOM_SCROLL_MULTIPLIER * ZOOM_SPEED * window.getScrollDelta();
        }

        if (!resetSeq) {
            glm::vec3 forwardFace = glm::normalize(FORWARD * cosf(yaw) - RIGHT * sinf(yaw));
            glm::vec3 faceRight = glm::cross(forwardFace, UP);

            if (window.isKeyPressed(GLFW_KEY_W)) {
                focusPoint += forwardFace * MOVE_SPEED * dt;
            }
            if (window.isKeyPressed(GLFW_KEY_S)) {
                focusPoint -= forwardFace * MOVE_SPEED * dt;
            }
            if (window.isKeyPressed(GLFW_KEY_D)) {
                focusPoint += faceRight * MOVE_SPEED * dt;
            }
            if (window.isKeyPressed(GLFW_KEY_A)) {
                focusPoint -= faceRight * MOVE_SPEED * dt;
            }

            float dy = 0;
            if (window.isKeyPressed(GLFW_KEY_SPACE)) {
                dy += ELEVATION_SPEED * dt;
            }
            if (window.isKeyPressed(GLFW_KEY_LEFT_SHIFT)) {
                dy -= ELEVATION_SPEED * dt;
            }
            if (dy != 0) {
                float verticalCameraDist = distance * sin(pitch);
                float horizontalCameraDist = distance * cos(pitch);

                float changeFactor = (((verticalCameraDist + dy) / verticalCameraDist) - 1.0f);
                float distanceChange = distance * changeFactor;
                float forwardChange = horizontalCameraDist * changeFactor;

                distance += distanceChange;
                focusPoint += forwardChange * forwardFace;
            }
        }
    }

    yaw = fmodf(yaw, YAW_PERIOD);
    pitch = CLAMP(pitch, PITCH_MIN, PITCH_MAX);
    distance = CLAMP(distance, MIN_DISTANCE, MAX_DISTANCE);
    focusPoint.x = CLAMP(focusPoint.x, MIN_POS.x, MAX_POS.x);
    focusPoint.z = CLAMP(focusPoint.z, MIN_POS.y, MAX_POS.y);

    float verticalCameraDist = distance * sin(pitch);
    float horizontalCameraDist = distance * cos(pitch);

    glm::vec3 focusOffset = UP * verticalCameraDist
                            + RIGHT * sinf(yaw) * horizontalCameraDist
                            - FORWARD * cosf(yaw) * horizontalCameraDist;

    glm::vec3 cameraPos = focusPoint + focusOffset;

    lastCameraPos = cameraPos;
    lastViewMatrix = glm::lookAt(cameraPos, focusPoint, UP);
}

void Camera::reset() {
    distance = initDistance;
    focusPoint = initFocusPoint;
    pitch = initPitch;
    yaw = initYaw;
}

void Camera::addImGuiSection() {
    if (!ImGui::CollapsingHeader("Camera Options")) {
        return;
    }

    glm::vec2 collapsedFocusPoint = {focusPoint.x, focusPoint.z};
    ImGui::DragFloat2("Focus Point (x,z)", &collapsedFocusPoint.x, 0.5f, MIN_POS.x, MAX_POS.x);
    focusPoint.x = collapsedFocusPoint.x;
    focusPoint.z = collapsedFocusPoint.y;

    ImGui::DragFloat("Distance", &distance, 0.5f, MIN_DISTANCE, MAX_DISTANCE);

    ImGui::SliderFloat("Pitch", &pitch, PITCH_MIN, PITCH_MAX);
    ImGui::SliderFloat("Yaw", &yaw, 0.0f, YAW_PERIOD);

    if (ImGui::Button("Reset (R)")) {
        reset();
    }
}

glm::mat4 Camera::getViewMatrix() const {
    return lastViewMatrix;
}

glm::vec3 Camera::getPosition() const {
    return lastCameraPos;
}

float Camera::getFOV() const {
    return fov;
}

float Camera::getPitch() const {
    return pitch;
}

float Camera::getYaw() const {
    return yaw;
}
