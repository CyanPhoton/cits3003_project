#ifndef SYNC_MANAGER_H
#define SYNC_MANAGER_H

#include <vector>

class SyncManager {
    std::vector<float> frameTimes{}; // TODO: A ring buffer or similar would be much, but stl doesn't have one, make one?

    const size_t FRAME_COUNT = 200;
    const size_t FRAME_DISPLAY_COUNT = 200;

public:
    SyncManager();

    void addImGuiSection(float frameDelta);
};

#endif //SYNC_MANAGER_H
