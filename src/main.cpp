#include <thread>
using namespace std::chrono_literals;

#include <glm/gtx/transform.hpp>

#include "utility/OpenGL.h"
#include "system_interfaces/WindowManager.h"
#include "rendering/Model.h"
#include "rendering/MasterRenderer.h"
#include "rendering/imgui/ImGuiManager.h"
#include "Camera.h"
#include "scene/SceneManager.h"
#include "SyncManager.h"

#ifdef _MSC_VER
#include <windows.h>
#define MAIN WINAPI WinMain(HINSTANCE inst, HINSTANCE prev, LPSTR cmd, int show)
#else
#define MAIN main()
#endif

int MAIN {
    WindowManager windowManager;

    auto window = windowManager.createWindow("Main Window", {1200, 720});
    window.makeContextCurrent();
    windowManager.setVSync(true);

    OpenGL::loadFunctions();
    OpenGL::setupDebugCallback();

    MasterRenderer masterRenderer{window};

    ImGuiManager imguiManager{window};

    Camera camera(1.0f, {0.0f, 0.0f, 0.0f}, M_PI_4, M_PI_4);

    SyncManager syncManager{};

    SceneManager sceneManager{masterRenderer};

    while (!window.shouldClose()) {
        windowManager.update();
        imguiManager.newFrame();
        imguiManager.enableMainWindowDocking();

        camera.update(window, windowManager.getDeltaTime(), true);
        masterRenderer.useCamera(camera);

        masterRenderer.update();

//        ImGui::ShowDemoWindow();
//        ImGui::ShowStyleEditor();

        sceneManager.addImGui(masterRenderer);
        sceneManager.toolUpdate(masterRenderer, window, camera);

        if (ImGui::Begin("Options & Info", nullptr, ImGuiWindowFlags_NoFocusOnAppearing)) {
            masterRenderer.addImGuiSection(windowManager);
            sceneManager.addImGuiSection(masterRenderer);
            camera.addImGuiSection();
            syncManager.addImGuiSection(windowManager.getDeltaTime());
        }
        ImGui::End();

        masterRenderer.render();
        imguiManager.render();

        window.swapBuffers();
    }

    imguiManager.cleanup();
    masterRenderer.cleanup();

    windowManager.destroyWindow(window);

    windowManager.cleanup();

    return EXIT_SUCCESS;
}
